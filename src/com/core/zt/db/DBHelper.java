package com.core.zt.db;

import android.content.Context;

import com.core.zt.model.User;
import com.simple.util.db.operation.SimpleDbHelper;

public class DBHelper extends SimpleDbHelper {

	private static final String DBNAME = "zt.db";
	private static final int DBVERSION = 4;
	private static final Class<?>[] clazz = {User.class};

	public DBHelper(Context context) {
		super(context, DBNAME, null, DBVERSION, clazz);
	}
}