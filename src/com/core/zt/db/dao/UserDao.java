package com.core.zt.db.dao;

import android.content.Context;

import com.core.zt.db.DBHelper;
import com.core.zt.model.User;
import com.simple.util.db.operation.TemplateDAO;

import java.util.List;

public class UserDao extends TemplateDAO<User> {
	private List<User> users;

	public UserDao(Context context) {
		super(new DBHelper(context));
	}

	public User getUserByDefault() {
		users = find();
		if (users != null && users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}
	
	
}
