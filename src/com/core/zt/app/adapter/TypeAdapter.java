package com.core.zt.app.adapter;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.ui.BaseActivity;
import com.core.zt.model.Type;

import java.util.List;

;

@SuppressLint({"ViewHolder", "InflateParams"})
public class TypeAdapter extends BaseListAdapter<Type> {

    public int type = 0;

    public TypeAdapter(BaseActivity context, List<Type> list, int type) {
        super(context, list);
        this.type = type;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.item_type, null);
        final Type item = (Type) getItem(position);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        if (item.getName() != null) {
            tvName.setText(item.getName());
        } else {
            tvName.setText(item.getLabel());
        }
        final CheckBox cbCheck = (CheckBox) convertView
                .findViewById(R.id.cb_check);
        cbCheck.setChecked(item.isCheck());
        cbCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                item.setCheck(isChecked);

                if (type == 1) {
                    if (isChecked) {
                        for (int i = 0; i < mList.size(); i++) {

                            if (mList.get(i).getId().equals(item.getId())) {
                                mList.get(i).setCheck(true);
                            } else {
                                mList.get(i).setCheck(false);
                            }
                        }
                    }
                } else {
                    if (type != 2) {
                        if (position == 0) {
                            for (int i = 1; i < mList.size(); i++) {
                                mList.get(i).setCheck(false);
                            }
                        } else {
                            mList.get(0).setCheck(false);
                        }
                    }
                }
                notifyDataSetChanged();
            }
        });
        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (cbCheck.isChecked()) {
                    cbCheck.setChecked(false);
                } else {
                    cbCheck.setChecked(true);
                }
            }
        });
        return convertView;
    }

    public Type getCurrentName() {
        for (int i = 0; i < mList.size(); i++) {

            if (mList.get(i).isCheck()) {
                return mList.get(i);
            }
        }
        return null;
    }
}
