/**
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.core.zt.app.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.db.InviteMessgeDao;
import com.core.zt.model.Doctor;
import com.core.zt.model.InviteMessage;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.IMService;
import com.core.zt.util.JsonUtil;
import com.core.zt.util.UIHelper;
import com.core.zt.widget.CircleImageView;
import com.easemob.chat.EMContactManager;

import java.util.List;

/**
 * 简单的好友Adapter实现
 * 
 */
public class AddFriendsAdapter extends ArrayAdapter<Doctor> {

	private LayoutInflater layoutInflater;
	private ProgressDialog progressDialog;
	private int res;
	private Context mContext;

	public AddFriendsAdapter(Context context, int resource, List<Doctor> objects) {
		super(context, resource, objects);
		this.res = resource;
		mContext = context;
		layoutInflater = LayoutInflater.from(context);
		inviteMessgeDao = new InviteMessgeDao(mContext);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = layoutInflater.inflate(res, null);
		}

		TextView nameText = (TextView) convertView.findViewById(R.id.name);
		Button btnAdd = (Button) convertView.findViewById(R.id.indicator);
		CircleImageView avatar = (CircleImageView) convertView
				.findViewById(R.id.avatar);

		final Doctor member = getItem(position);
		String username = member.getDoctorName();

		nameText.setText(member.getDoctorName());

		AppContext.setImageFull(member.getPhoto(), avatar,
				AppContext.memberPhotoOption);
		btnAdd.setTag(member);
		btnAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				addContact((Doctor) v.getTag());


			}
		});
		return convertView;
	}

	private InviteMessgeDao inviteMessgeDao;

	/**
	 * 添加contact
	 * 

	 */
	public void addContact(final Doctor member) {
		if (AppContext.currentUser.getUserId().equals(member.getId())) {
			mContext.startActivity(new Intent(mContext, AlertDialog.class)
					.putExtra("msg", "不能添加自己"));
			return;
		}

		if (AppContext.getApplication().getContactList()
				.containsKey(member.getId() + "")) {
		UIHelper.ShowMessage(mContext, "此用户已是你的好友");
			return;
		}
		progressDialog = new ProgressDialog(mContext);
		progressDialog.setMessage("正在发送请求...");
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.show();

		new Thread(new Runnable() {
			public void run() {

				try {

					// demo写死了个reason，实际应该让用户手动填入
					EMContactManager
							.getInstance()
							.addContact(
									member.getUserName(),
									JsonUtil.convertObjectToJson(AppContext.currentUser));

					((Activity) mContext).runOnUiThread(new Runnable() {
						public void run() {
							progressDialog.dismiss();
							InviteMessage message = new InviteMessage();
							message.setFrom(member.getUserName());
							message.setStatus(InviteMessage.InviteMesageStatus.WAIT);
							message.setReason(JsonUtil
									.convertObjectToJson(member));
							message.setTime(System.currentTimeMillis());
							inviteMessgeDao.saveMessage(message);
							Toast.makeText(mContext, "发送请求成功,等待对方验证",
									Toast.LENGTH_SHORT).show();
						}
					});
				} catch (final Exception e) {
					((Activity) mContext).runOnUiThread(new Runnable() {
						public void run() {
							progressDialog.dismiss();
							Toast.makeText(mContext,
									"请求添加好友失败:" + e.getMessage(),
									Toast.LENGTH_SHORT).show();
						}
					});
				}
			}
		}).start();
	}

	@Override
	public Doctor getItem(int position) {
		return super.getItem(position);
	}

	@Override
	public int getCount() {

		return super.getCount();
	}

}
