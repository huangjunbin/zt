package com.core.zt.app.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.ui.BaseActivity;
import com.core.zt.model.Drug;

import java.util.List;

public class DrugAdapter extends BaseListAdapter<Drug> {


    public DrugAdapter(BaseActivity context, List<Drug> list) {
        super(context, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.item_type, null);
        final Drug item = (Drug) getItem(position);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        tvName.setText(item.getDrugCnName());
        final CheckBox cbCheck = (CheckBox) convertView
                .findViewById(R.id.cb_check);
        cbCheck.setVisibility(View.GONE);
        return convertView;
    }

}
