package com.core.zt.app.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.model.Notification;
import com.core.zt.util.DateUtil;

import java.util.Date;
import java.util.List;

@SuppressLint("InflateParams")
public class NotificationAdapter extends BaseAdapter {

	private Context mContext;
	List<Notification> list;

	public NotificationAdapter(Activity context, List<Notification> list) {
		mContext = context;
		this.list = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();

			convertView = ((Activity) mContext).getLayoutInflater().inflate(
					R.layout.item_notification, null);
			viewHolder.tvTitle = (TextView) convertView
					.findViewById(R.id.tv_title);
			viewHolder.tvContent = (TextView) convertView
					.findViewById(R.id.tv_content);
			viewHolder.tvTime = (TextView) convertView
					.findViewById(R.id.tv_time);
			viewHolder.ivPhoto = (ImageView) convertView
					.findViewById(R.id.iv_photo);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Notification data = (Notification) getItem(position);

		viewHolder.tvTitle.setText(data.getTitle());

		viewHolder.tvContent.setText(data.getContent());
		Date de= DateUtil.parseDate(data.getCreateDate(),"HH:mm");
		viewHolder.tvTime.setText(DateUtil.formatDate(de,"HH:mm"));

		return convertView;
	}

	class ViewHolder {
		TextView tvTitle, tvContent, tvTime;
		ImageView ivPhoto;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
}
