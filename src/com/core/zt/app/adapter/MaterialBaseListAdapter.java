package com.core.zt.app.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;

import com.core.zt.R;
import com.core.zt.app.ui.BaseActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * 
 * @ClassName: BaseListAdapter
 * @Description: 基本列表适配器封装
 * @author BaoHang baohang2011@gmail.com
 * @date
 * @param <T>
 */
public abstract class MaterialBaseListAdapter<T> extends
		RecyclerView.Adapter<ViewHolder> {
	protected List<T> mList;// 列表List
	protected LayoutInflater mInflater;// 布局管理
	protected DisplayImageOptions options;
	protected ImageLoader imageLoader;
	protected static final int NO_DEFAULT = -1;// 有图片但是没有默认图
	protected static final int NO_IMAGE = 0;// 没有图片
	protected BaseActivity baseActivity;
	protected Activity activity;

	/**
	 * 没有指定默认图的够造方法
	 * 
	 * @param context
	 * @param list
	 */
	public MaterialBaseListAdapter(BaseActivity context, List<T> list) {
		this(context, list, NO_DEFAULT);
	}

	public MaterialBaseListAdapter(Activity context, List<T> list) {
		this(context, list, NO_DEFAULT);
	}

	/**
	 * 
	 * @param context
	 * @param list
	 * @param defaultId
	 *            传0则表示适配器中没有图片需要显示,-1表示需要显示但没有默认图片
	 */
	protected MaterialBaseListAdapter(BaseActivity context, List<T> list,
			int defaultId) {
		baseActivity = context;
		init(list, defaultId);
	}

	/**
	 * 
	 * @param context
	 * @param list
	 * @param defaultId
	 *            传0则表示适配器中没有图片需要显示,-1表示需要显示但没有默认图片
	 */
	protected MaterialBaseListAdapter(Activity context, List<T> list,
			int defaultId) {
		activity = context;
		init(list, defaultId);
	}

	@SuppressWarnings("deprecation")
	private void init(List<T> list, int defaultId) {
		// TODO Auto-generated me mInflater = LayoutInflater.from(context);
		if (list == null) {
			list = new ArrayList<T>();
		}
		mList = list;
		if (defaultId == NO_IMAGE) {// 没有图片
			return;
		} else if (defaultId == NO_DEFAULT) {// 有图片但是没有默认图
			options = new DisplayImageOptions.Builder()
					.showStubImage(R.drawable.default_image)
					.showImageForEmptyUri(R.drawable.default_image)
					.showImageOnFail(R.drawable.default_image)
					.cacheInMemory(true).cacheOnDisc(true)
					.bitmapConfig(Bitmap.Config.RGB_565).build();
		} else {// 有图片有默认图
			options = new DisplayImageOptions.Builder()
					.showStubImage(defaultId).showImageForEmptyUri(defaultId)
					.showImageOnFail(defaultId).cacheInMemory(true)
					.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
					.build();
		}
		imageLoader = ImageLoader.getInstance();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
}
