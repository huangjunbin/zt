package com.core.zt.app.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.app.ui.BaseActivity;
import com.core.zt.model.Record;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

public class RecordAdapter extends BaseListAdapter<Record> {

    public RecordAdapter(BaseActivity context, List<Record> list) {
        super(context, list);

    }

    private int icons[] = {R.drawable.icon_i1, R.drawable.icon_i2, R.drawable.icon_i3, R.drawable.icon_i4, R.drawable.icon_i5,R.drawable.icon_i8};

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_record, null);
            viewHolder.tvDate = (TextView) convertView
                    .findViewById(R.id.tv_date);
            viewHolder.tvTime = (TextView) convertView
                    .findViewById(R.id.tv_time);
            viewHolder.tvName = (TextView) convertView
                    .findViewById(R.id.tv_name);
            viewHolder.ivType = (ImageView) convertView
                    .findViewById(R.id.iv_type);
            viewHolder.tvValue = (TextView) convertView
                    .findViewById(R.id.tv_value);
            viewHolder.tvUnit = (TextView) convertView
                    .findViewById(R.id.tv_unit);
            viewHolder.ivPhoto = (ImageView) convertView
                    .findViewById(R.id.iv_photo);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Record data = (Record) getItem(position);

        Date date = DateUtil.parseDate(data.getLogTime(), "yyyy-MM-dd HH:mm:ss");
        viewHolder.tvTime.setText(DateUtil.getTimeNo(date));
        viewHolder.tvDate.setText(DateUtil.getDate(date));

        if (mList.size() > 1 && position >= 1) {
            Date date1 = DateUtil.parseDate(mList.get(position - 1).getLogTime(), "yyyy-MM-dd HH:mm:ss");
            if (DateUtil.getDate(date).equals(DateUtil.getDate(date1))) {
                viewHolder.tvDate.setVisibility(View.GONE);
            } else {
                viewHolder.tvDate.setVisibility(View.VISIBLE);
            }
        } else {
            viewHolder.tvDate.setVisibility(View.VISIBLE);
        }


        if ("1".equals(data.getLogType())) {
            viewHolder.ivType.setImageResource(icons[1]);
            viewHolder.tvUnit.setText("mmol/L");
            viewHolder.tvValue.setText(data.getLogVal1());
            viewHolder.tvName.setText(RecordServer.getInstance(baseActivity).getSugarTime(data.getLogVal2()) + "血糖");
            viewHolder.ivPhoto.setVisibility(View.GONE);
            viewHolder.tvValue.setTextColor(baseActivity.getResources().getColor(R.color.main_bg));
        }
        if ("2".equals(data.getLogType())) {
            viewHolder.ivType.setImageResource(icons[2]);
            viewHolder.tvUnit.setText("mmHg");
            viewHolder.tvValue.setText((int)Float.parseFloat(data.getLogVal1()) + "/" + data.getLogVal2());
            data.setLogVal1((int) Float.parseFloat(data.getLogVal1()) + "");
            viewHolder.tvName.setText("血压");
            viewHolder.tvValue.setTextColor(baseActivity.getResources().getColor(R.color.gray));
            viewHolder.ivPhoto.setVisibility(View.GONE);
        }
        if ("3".equals(data.getLogType())) {
            float value2 = Float.parseFloat(data.getLogVal2());
            float value3 = Float.parseFloat(data.getLogVal3());
            float v = (float) (value3 / ((value2 * value2) * 0.01 * 0.01));
            DecimalFormat fnum = new DecimalFormat("##0.0");
            String dd = fnum.format(v);
            viewHolder.tvValue.setText("" + dd);
            viewHolder.tvValue.setTextColor(baseActivity.getResources().getColor(R.color.main_bg));
            viewHolder.tvName.setText("体重指数");
            viewHolder.ivType.setImageResource(icons[3]);
            viewHolder.tvUnit.setText("kg/m²");
            viewHolder.ivPhoto.setVisibility(View.GONE);
        }
        if ("4".equals(data.getLogType())) {
            if (data.getLogVal3() != null) {
                String[] s = data.getLogVal3().split(",");
                viewHolder.ivPhoto.setVisibility(View.VISIBLE);
                if (s.length > 0) {
                    AppContext.setImage(s[0], viewHolder.ivPhoto, AppContext.imageOption);
                }
            }
            viewHolder.tvValue.setText("");
            viewHolder.tvName.setText("化验单");
            viewHolder.tvValue.setTextColor(baseActivity.getResources().getColor(R.color.main_bg));
            viewHolder.ivType.setImageResource(icons[4]);
            viewHolder.tvUnit.setText("");
        }
        if ("5".equals(data.getLogType())) {
            viewHolder.ivType.setImageResource(icons[5]);
            viewHolder.tvUnit.setText("%");
            viewHolder.tvValue.setText(data.getLogVal1());
            viewHolder.tvName.setText("HbA1c");
            viewHolder.ivPhoto.setVisibility(View.GONE);
            viewHolder.tvValue.setTextColor(baseActivity.getResources().getColor(R.color.main_bg));
        }
        return convertView;
    }

    class ViewHolder {
        TextView tvDate, tvTime, tvName, tvValue, tvUnit;
        ImageView ivType, ivPhoto;
    }
}
