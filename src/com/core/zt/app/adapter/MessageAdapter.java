package com.core.zt.app.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.ui.BaseActivity;
import com.core.zt.model.Series;
import com.core.zt.net.Urls;

import java.util.List;

public class MessageAdapter extends BaseListAdapter<Series> {

	public MessageAdapter(BaseActivity context, List<Series> list) {
		super(context, list);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_message, null);
			viewHolder.textViewMessageContent = (TextView) convertView
					.findViewById(R.id.message_content);
			viewHolder.textViewMessageTime = (TextView) convertView
					.findViewById(R.id.message_time);
			viewHolder.tvTitle = (TextView) convertView
					.findViewById(R.id.message_title);
			viewHolder.ivPhoto = (ImageView) convertView
					.findViewById(R.id.iv_photo);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Series data = (Series) getItem(position);
		viewHolder.tvTitle.setText(data.getSeriesName());
		viewHolder.textViewMessageContent.setText(data.getTotal()+"篇文章");
		viewHolder.textViewMessageTime.setText("");
		imageLoader.displayImage(Urls.IMAGE_URL+data.getFilePath()+data.getFileName(),viewHolder.ivPhoto);
		return convertView;
	}

	class ViewHolder {
		TextView textViewMessageContent, tvTitle, textViewMessageTime;
		ImageView ivPhoto;
	}
}
