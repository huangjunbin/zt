package com.core.zt.app.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.ui.BaseActivity;
import com.core.zt.model.Day;
import com.core.zt.model.Notice;
import com.core.zt.model.Week;
import com.core.zt.service.NoticeServer;

import java.util.List;

public class NoticeAdapter extends BaseListAdapter<Notice> {

    public NoticeAdapter(BaseActivity context, List<Notice> list) {
        super(context, list);
        server = new NoticeServer(context);

    }

    private NoticeServer server;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_notice, null);
            viewHolder.tvWeek = (TextView) convertView
                    .findViewById(R.id.tv_week);
            viewHolder.tvTime = (TextView) convertView
                    .findViewById(R.id.tv_time);
            viewHolder.tvName = (TextView) convertView
                    .findViewById(R.id.tv_name);
            viewHolder.ivCheck = (ImageView) convertView
                    .findViewById(R.id.sth_notice);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Notice data = (Notice) getItem(position);
        viewHolder.tvName.setText(data.getName());
        if (data.getDateList() != null) {
            String time = "";
            for (Day str : data.getDateList()) {
                time += str.getDay() + "  ";
            }
            viewHolder.tvTime.setText(time);
        }
        if (data.getWeekList() != null) {
            String week = "";
            for (Week str : data.getWeekList()) {
                week += str.getWeek() + "  ";
            }
            if (data.getWeekList().size() == 7) {
                viewHolder.tvWeek.setText("每天");
            } else if (data.getWeekList().size() == 0) {
                viewHolder.tvWeek.setText("永不");
            } else {
                viewHolder.tvWeek.setText(week);
            }
        } else {
            viewHolder.tvWeek.setText("永不");
        }
        if (data.getState() == 0) {
            viewHolder.ivCheck.setImageDrawable(baseActivity.getResources().getDrawable((R.drawable.iv_n)));
        } else {
            viewHolder.ivCheck.setImageDrawable(baseActivity.getResources().getDrawable((R.drawable.iv_y)));
        }
        viewHolder.ivCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.getState() == 0) {
                    data.setState(1);
                    server.startNotice(data);
                    viewHolder.ivCheck.setImageDrawable(baseActivity.getResources().getDrawable((R.drawable.iv_y)));
                } else {
                    data.setState(0);
                    server.stopNotice(data);
                    viewHolder.ivCheck.setImageDrawable(baseActivity.getResources().getDrawable((R.drawable.iv_n)));
                }
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView tvTime, tvName, tvWeek;
        ImageView ivCheck;
    }
}
