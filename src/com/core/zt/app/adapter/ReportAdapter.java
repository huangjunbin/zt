package com.core.zt.app.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.core.zt.R;
import com.core.zt.app.ui.ReportChartActivity;
import com.core.zt.model.Report;

import java.util.List;

@SuppressLint("InflateParams")
public class ReportAdapter extends BaseAdapter {

	private Context mContext;
	List<Report> list;

	public ReportAdapter(Activity context, List<Report> list) {
		mContext = context;
		this.list = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();

			convertView = ((Activity) mContext).getLayoutInflater().inflate(
					R.layout.item_report, null);
			viewHolder.btnLeft = (Button) convertView
					.findViewById(R.id.btn_left);
			viewHolder.btnRight = (Button) convertView
					.findViewById(R.id.btn_right);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final  Report data = (Report) getItem(position);

		if(position%2==0){
			viewHolder.btnLeft.setVisibility(View.VISIBLE);
			viewHolder.btnRight.setVisibility(View.GONE);
		}else{
			viewHolder.btnLeft.setVisibility(View.GONE);
			viewHolder.btnRight.setVisibility(View.VISIBLE);
		}
		viewHolder.btnLeft.setText(data.getName());
		viewHolder.btnRight.setText(data.getName());

		viewHolder.btnLeft.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent=new Intent(mContext, ReportChartActivity.class);
				intent.putExtra("year",data.getYear());
				intent.putExtra("month",data.getMonth());
				mContext.startActivity(intent);
			}
		});
		viewHolder.btnRight.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent=new Intent(mContext, ReportChartActivity.class);
				intent.putExtra("year",data.getYear());
				intent.putExtra("month",data.getMonth());
				mContext.startActivity(intent);
			}
		});
		return convertView;
	}

	class ViewHolder {
		Button btnLeft, btnRight;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
}
