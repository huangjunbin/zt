package com.core.zt.app.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.ui.BaseActivity;
import com.core.zt.model.Lesson;
import com.core.zt.net.Urls;

import java.util.List;

import static com.core.zt.R.id.iv_photo;

public class MessageListAdapter extends BaseListAdapter<Lesson> {

	public MessageListAdapter(BaseActivity context, List<Lesson> list) {
		super(context, list);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_message_list, null);
			viewHolder.textViewMessageContent = (TextView) convertView
					.findViewById(R.id.message_content);
			viewHolder.textViewMessageTime = (TextView) convertView
					.findViewById(R.id.message_time);
			viewHolder.tvTitle = (TextView) convertView
					.findViewById(R.id.message_title);
			viewHolder.ivPhoto = (ImageView) convertView
					.findViewById(iv_photo);
			viewHolder.tv1= (TextView) convertView.findViewById(R.id.tv_1);
			viewHolder.tv2= (TextView) convertView.findViewById(R.id.tv_2);
			viewHolder.tv3= (TextView) convertView.findViewById(R.id.tv_3);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Lesson data = (Lesson) getItem(position);
		viewHolder.tvTitle.setText(data.getLessonName());
		viewHolder.textViewMessageContent.setText(data.getLessonContent()+"篇文章");
		viewHolder.textViewMessageTime.setText("");
		viewHolder.tv1.setText(data.getBrowseCnt()+"");
		viewHolder.tv2.setText(data.getUsefulCnt()+"");
		viewHolder.tv3.setText(data.getAcceptCnt() + "");

		if(data.getIsUseful()==1){
			viewHolder.tv2.setCompoundDrawablesWithIntrinsicBounds(baseActivity.getResources().getDrawable(R.drawable.ic_know_2p),null,null,null);
		}else{
			viewHolder.tv2.setCompoundDrawablesWithIntrinsicBounds(baseActivity.getResources().getDrawable(R.drawable.ic_know_2),null,null,null);
		}
		imageLoader.displayImage(Urls.IMAGE_URL+data.getFilePath()+data.getFileName(),viewHolder.ivPhoto);
		return convertView;
	}

	class ViewHolder {
		TextView textViewMessageContent, tvTitle, textViewMessageTime;
		ImageView ivPhoto;
		TextView tv1,tv2,tv3;
	}
}
