package com.core.zt.app.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;

import java.text.DecimalFormat;
import java.util.Date;

public class BMIDetailActivity extends BaseActivity {

    private HeaderBar bar;
    private Record data;
    private TextView tvTime,tvValue3,tvValue2,tvBMI;
    private Button btnEdit,btnRemove;
    private Intent intent;
    public BMIDetailActivity() {
        super(R.layout.activity_bmi_detail);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);
        tvTime= (TextView) findViewById(R.id.tv_time);
        tvValue3= (TextView) findViewById(R.id.tv_value3);
        tvValue2= (TextView) findViewById(R.id.tv_value2);
        btnEdit= (Button) findViewById(R.id.btn_save);
        btnRemove= (Button) findViewById(R.id.btn_delete);
        tvBMI= (TextView) findViewById(R.id.tv_bmi);
    }

    @Override
    public void initData() {
        data= (Record) getIntent().getSerializableExtra("data");
        bar.setTitle("体重指数");
    }

    @Override
    public void bindViews() {
        tvValue3.setText(data.getLogVal3());
        tvValue2.setText(data.getLogVal2());
        Date date= DateUtil.parseDate(data.getLogTime(), "yyyy-MM-dd HH:mm");
        tvTime.setText(DateUtil.getDateNoSecond(date));
        float value2=Float.parseFloat(data.getLogVal2());
        float value3=Float.parseFloat(data.getLogVal3());
        float v=(float)(value3 / ((value2 * value2) * 0.01 * 0.01));
        DecimalFormat fnum   =   new   DecimalFormat("##0.0");
        String   dd=fnum.format(v);
        tvBMI.setText(dd);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent();
                intent.putExtra("data", data);
                intent.setClass(BMIDetailActivity.this, NewBMIActivity.class);
                startActivity(intent);
            }
        });
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSureDialog(BMIDetailActivity.this, "确定删除?", "确定", new OnSurePress() {
                    @Override
                    public void onClick(View view) {
                        RecordServer.getInstance(BMIDetailActivity.this).removeRecord(
                                data.getId(), new CustomAsyncResponehandler() {
                                    public void onFinish() {

                                    }

                                    ;

                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {
                                            showToast("删除成功");
                                            BMIDetailActivity.this.finish();
                                        }
                                    }
                                });
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {

                    }
                });

            }
        });
    }




}
