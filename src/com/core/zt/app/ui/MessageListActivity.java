package com.core.zt.app.ui;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.core.zt.R;
import com.core.zt.app.adapter.MessageListAdapter;
import com.core.zt.model.Lesson;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.LessonServer;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.XListView;

import java.util.ArrayList;
import java.util.List;


public class MessageListActivity extends BaseActivity implements XListView.IXListViewListener {
    private HeaderBar headerBar;
    private XListView xListView;
    private MessageListAdapter adapter;
    public static List<Lesson> dataList;
    private int page = 1;
    private int rows = 5;
    private boolean isRefresh = true;
    private int type = 0;
    private String id;
    private String title;

    public MessageListActivity() {
        super(R.layout.activity_message_list);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        xListView = (XListView) findViewById(R.id.messagelist);
        xListView.setXListViewListener(this);
    }

    @Override
    public void initData() {
        title = getIntent().getStringExtra("title");
        id = getIntent().getStringExtra("id");
        type = getIntent().getIntExtra("type", 0);
        dataList = new ArrayList<Lesson>();
        adapter = new MessageListAdapter(MessageListActivity.this, dataList);
        xListView.setAdapter(adapter);
    }

    @Override
    public void bindViews() {
        headerBar.setTitle(title);
        xListView.setPullLoadEnable(true);
        xListView.setFooterDividersEnabled(false);
        xListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Lesson msg = dataList.get(position - 1);
                Intent intent = new Intent(MessageListActivity.this, LessonDetailActivity.class);
                intent.putExtra("data", msg);
                intent.putExtra("type", type);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        page = 1;
        getMessageList();

    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        getMessageList();
    }

    @Override
    protected void onResume() {
        onRefresh();
        super.onResume();
    }

    private void getMessageList() {
        LessonServer.getInstance(context).getLesson(id, page + "", rows + "", type,new CustomAsyncResponehandler() {
            public void onFinish() {
                xListView.stopRefresh();
                xListView.stopLoadMore();
            }

            ;

            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    List<Lesson> lists = (List<Lesson>) baseModel
                            .getResultObj();
                    if (lists != null && lists.size() > 0) {
                        dataList.clear();
                        dataList.addAll(0, lists);
                        adapter.notifyDataSetChanged();
                        ;
                    }
                }
            }
        });
    }
}
