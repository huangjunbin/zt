package com.core.zt.app.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;

import java.util.Date;

public class BloodSugarDetailActivity extends BaseActivity {

    private HeaderBar bar;
    private Record data;
    private TextView tvTime,tvTimeDetail,tvValue;
    private Button btnEdit,btnRemove;
    private Intent intent;
    public BloodSugarDetailActivity() {
        super(R.layout.activity_bloodsugar_detail);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);
        tvTime= (TextView) findViewById(R.id.tv_time);
        tvTimeDetail= (TextView) findViewById(R.id.tv_time_detail);
        tvValue= (TextView) findViewById(R.id.tv_value);
        btnEdit= (Button) findViewById(R.id.btn_save);
        btnRemove= (Button) findViewById(R.id.btn_delete);
    }

    @Override
    public void initData() {
        data= (Record) getIntent().getSerializableExtra("data");
        bar.setTitle("血糖");
    }

    @Override
    public void bindViews() {
        tvValue.setText(data.getLogVal1());
        tvTime.setText(RecordServer.getInstance(this).getSugarTime(data.getLogVal2()));
        Date date= DateUtil.parseDate(data.getLogTime(),"yyyy-MM-dd HH:mm");
        tvTimeDetail.setText(DateUtil.getDateNoSecond(date));
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent();
                intent.putExtra("data", data);
                intent.setClass(BloodSugarDetailActivity.this, NewBloodSugarActivity.class);
                startActivity(intent);
            }
        });
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showSureDialog(BloodSugarDetailActivity.this, "确定删除?", "确定", new OnSurePress() {
                    @Override
                    public void onClick(View view) {
                        RecordServer.getInstance(BloodSugarDetailActivity.this).removeRecord(
                                data.getId(), new CustomAsyncResponehandler() {
                                    public void onFinish() {

                                    }

                                    ;

                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {
                                            showToast("删除成功");
                                            BloodSugarDetailActivity.this.finish();
                                        }
                                    }
                                });
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {

                    }
                });

            }
        });
    }


}
