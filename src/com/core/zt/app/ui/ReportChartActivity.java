
package com.core.zt.app.ui;

import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.MyMarkerView;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ReportChartActivity extends BaseActivity implements
        OnChartGestureListener, OnChartValueSelectedListener {

    private LineChart mChart;
    private Button btn1, btn2, btn3, btn4;
    private List<Button> btnList = new ArrayList<>();
    private HeaderBar bar;
    private LinearLayout ll1,ll2,ll3,ll4;

    public ReportChartActivity() {
        super(R.layout.activity_report_chart);
    }

    private TextView tvLow, tvNormal, tvHeight;
    private LinearLayout lyMore;
    private int index = 1;
    private String year="";
    private String month="";

    @Override
    public void initViews() {
        year=getIntent().getStringExtra("year");
        month=getIntent().getStringExtra("month");
        mChart = (LineChart) findViewById(R.id.chart1);
        bar = (HeaderBar) findViewById(R.id.header);
        ll1= (LinearLayout) findViewById(R.id.ll_1);
        ll2= (LinearLayout) findViewById(R.id.ll_2);
        ll3= (LinearLayout) findViewById(R.id.ll_3);
        ll4= (LinearLayout) findViewById(R.id.ll_4);
        btn1 = (Button) findViewById(R.id.btn_1);
        btn2 = (Button) findViewById(R.id.btn_2);
        btn3 = (Button) findViewById(R.id.btn_3);
        btn4 = (Button) findViewById(R.id.btn_4);
        lyMore = (LinearLayout) findViewById(R.id.ll_more);
        btnList.add(btn1);
        btnList.add(btn2);
        btnList.add(btn3);
        btnList.add(btn4);
        tvLow = (TextView) findViewById(R.id.tvLow);
        tvNormal = (TextView) findViewById(R.id.tvNormal);
        tvHeight = (TextView) findViewById(R.id.tvHight);
        for (int i = 0; i < btnList.size(); i++) {
            btnList.get(i).setTag(i + 1);
            btnList.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    change((int) view.getTag());
                }
            });
        }
        change(1);
    }

    @Override
    public void initData() {

        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);
        mChart.setBackgroundColor(Color.WHITE);
        // no description text
        mChart.setDescription("");
        mChart.setNoDataTextDescription("You need to provide data for the chart");

        // enable value highlighting
        mChart.setHighlightEnabled(true);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setScaleXEnabled(true);
        mChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        // mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);

        // set the marker to the chart
        mChart.setMarkerView(mv);

        // enable/disable highlight indicators (the lines that indicate the
        // highlighted Entry)
        mChart.setHighlightIndicatorEnabled(false);

        // x-axis limit line
//        LimitLine llXAxis = new LimitLine(10f, "Index 10");
//        llXAxis.setLineWidth(4f);
//        llXAxis.enableDashedLine(10f, 10f, 0f);
//        llXAxis.setLabelPosition(LimitLabelPosition.POS_RIGHT);
//        llXAxis.setTextSize(10f);
//
//        XAxis xAxis = mChart.getXAxis();
//        xAxis.addLimitLine(llXAxis);


        if (index == 1) {
            LimitLine ll2 = new LimitLine(20f, "正常HbA1c最大值");
            ll2.setLineWidth(1f);
            ll2.enableDashedLine(0f, 0f, 0f);
            ll2.setLabelPosition(LimitLabelPosition.POS_RIGHT);
            ll2.setTextSize(10f);
            ll2.setLineColor(getResColor(R.color.green));

            LimitLine ll3 = new LimitLine(3f, "正常HbA1c最低值");
            ll3.setLineWidth(1f);
            ll3.enableDashedLine(0f, 0f, 0f);
            ll3.setLabelPosition(LimitLabelPosition.POS_RIGHT);
            ll3.setTextSize(10f);
            ll3.setLineColor(getResColor(R.color.blue));
            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
            leftAxis.addLimitLine(ll2);
            leftAxis.addLimitLine(ll3);
            leftAxis.setAxisMaxValue(40f);
            leftAxis.setAxisMinValue(0f);
            leftAxis.setStartAtZero(false);
            leftAxis.enableGridDashedLine(10f, 10f, 0f);

            // limit lines are drawn behind data (and not on top)
            leftAxis.setDrawLimitLinesBehindData(true);

            mChart.getAxisRight().setEnabled(false);
        }
        if(index==3){
            LimitLine ll2 = new LimitLine(28f, "正常体重最大值");
            ll2.setLineWidth(1f);
            ll2.enableDashedLine(0f, 0f, 0f);
            ll2.setLabelPosition(LimitLabelPosition.POS_RIGHT);
            ll2.setTextSize(10f);
            ll2.setLineColor(getResColor(R.color.green));

            LimitLine ll3 = new LimitLine(18.5f, "正常体重最低值");
            ll3.setLineWidth(1f);
            ll3.enableDashedLine(0f, 0f, 0f);
            ll3.setLabelPosition(LimitLabelPosition.POS_RIGHT);
            ll3.setTextSize(10f);
            ll3.setLineColor(getResColor(R.color.blue));
            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
            leftAxis.addLimitLine(ll2);
            leftAxis.addLimitLine(ll3);
            leftAxis.setAxisMaxValue(40f);
            leftAxis.setAxisMinValue(0f);
            leftAxis.setStartAtZero(false);
            leftAxis.enableGridDashedLine(10f, 10f, 0f);

            // limit lines are drawn behind data (and not on top)
            leftAxis.setDrawLimitLinesBehindData(true);

            mChart.getAxisRight().setEnabled(false);
        }
        if(index==2){
            LimitLine ll1 = new LimitLine(140f, "高血压");
            ll1.setLineWidth(1f);
            ll1.enableDashedLine(0f, 0f, 0f);
            ll1.setLabelPosition(LimitLabelPosition.POS_RIGHT);
            ll1.setTextSize(10f);
            ll1.setLineColor(getResColor(R.color.orange));
            LimitLine ll2 = new LimitLine(90f, "正常血压");
            ll2.setLineWidth(1f);
            ll2.enableDashedLine(0f, 0f, 0f);
            ll2.setLabelPosition(LimitLabelPosition.POS_RIGHT);
            ll2.setTextSize(10f);
            ll2.setLineColor(getResColor(R.color.green));

            LimitLine ll3 = new LimitLine(60f, "低血压");
            ll3.setLineWidth(1f);
            ll3.enableDashedLine(0f, 0f, 0f);
            ll3.setLabelPosition(LimitLabelPosition.POS_RIGHT);
            ll3.setTextSize(10f);
            ll3.setLineColor(getResColor(R.color.blue));
            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
            leftAxis.addLimitLine(ll1);
            leftAxis.addLimitLine(ll2);
            leftAxis.addLimitLine(ll3);
            leftAxis.setAxisMaxValue(400f);
            leftAxis.setAxisMinValue(0f);
            leftAxis.setStartAtZero(false);
            leftAxis.enableGridDashedLine(10f, 10f, 0f);

            // limit lines are drawn behind data (and not on top)
            leftAxis.setDrawLimitLinesBehindData(true);

            mChart.getAxisRight().setEnabled(false);
        }
        if(index==1){
            LimitLine ll1 = new LimitLine(10f, "正常最大值");
            ll1.setLineWidth(1f);
            ll1.enableDashedLine(0f, 0f, 0f);
            ll1.setLabelPosition(LimitLabelPosition.POS_RIGHT);
            ll1.setTextSize(10f);
            ll1.setLineColor(Color.GREEN);
            LimitLine ll2 = new LimitLine(4.4f, "正常最小值");
            ll2.setLineWidth(1f);
            ll2.enableDashedLine(0f, 0f, 0f);
            ll2.setLabelPosition(LimitLabelPosition.POS_RIGHT);
            ll2.setTextSize(10f);
            ll2.setLineColor(Color.BLUE);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
            leftAxis.addLimitLine(ll1);
            leftAxis.addLimitLine(ll2);
            leftAxis.setAxisMaxValue(20f);
            leftAxis.setAxisMinValue(0f);
            leftAxis.setStartAtZero(false);
            leftAxis.enableGridDashedLine(10f, 10f, 0f);

            // limit lines are drawn behind data (and not on top)
            leftAxis.setDrawLimitLinesBehindData(true);

            mChart.getAxisRight().setEnabled(false);
        }
    }

    @Override
    public void bindViews() {

        bar.setTitle("月度报告");
        // add data
//        mChart.setVisibleXRange(20);
//        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mChart.centerViewTo(20, 50, AxisDependency.LEFT);
        mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
//        mChart.invalidate();
        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();
        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(LegendForm.LINE);
        // // dont forget to refresh the drawing
        // mChart.invalidate();

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }


    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("", "low: " + mChart.getLowestVisibleXIndex() + ", high: " + mChart.getHighestVisibleXIndex());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    public void change(int index) {
        this.index = index;
        for (int i = 0; i < btnList.size(); i++) {
            if (i + 1 == index) {
                btnList.get(i).setTextColor(getResColor(R.color.white));
                btnList.get(i).setBackgroundResource(R.drawable.btn_cheeck);
            } else {
                btnList.get(i).setTextColor(getResColor(R.color.main_bg));
                btnList.get(i).setBackgroundResource(R.drawable.btn_no_check);
            }
        }
        if(index==1){
            getSugar();
        }
        if(index==2){
            getPress();
        }
        if(index==3){
            getBMI();
        }
        if(index==4){
            getHbaRecord();
        }
        initData();
    }


    private void setData(ArrayList<String> xVals, ArrayList<Entry> yVals) {

        mChart.clear();
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "血糖");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        set1.enableDashedLine(10f, 5f, 0f);
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);

        set1.setValueTextSize(9f);
        set1.setFillAlpha(65);
        set1.setFillColor(Color.BLACK);
//        set1.setDrawFilled(true);
        // set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(),
        // Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);

        // set data
        mChart.setData(data);
        ll1.setVisibility(View.VISIBLE);
        ll2.setVisibility(View.GONE);
        ll3.setVisibility(View.GONE);
        ll4.setVisibility(View.GONE);
    }


    private void setDataPress(ArrayList<String> xVals, ArrayList<Entry> yVals, ArrayList<String> xVals2, ArrayList<Entry> yVals2) {

        mChart.clear();
        LineDataSet set1 = new LineDataSet(yVals, "高压");
        set1.enableDashedLine(10f, 5f, 0f);
        set1.setColor(getResColor(R.color.orange));
        set1.setCircleColor(getResColor(R.color.orange));
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setFillAlpha(65);
        set1.setFillColor(Color.BLACK);

        LineDataSet set2 = new LineDataSet(yVals2, "低压");
        set2.enableDashedLine(10f, 5f, 0f);
        set2.setColor(getResColor(R.color.blue));
        set2.setCircleColor(getResColor(R.color.blue));
        set2.setLineWidth(1f);
        set2.setCircleSize(3f);
        set2.setDrawCircleHole(false);
        set2.setValueTextSize(9f);
        set2.setFillAlpha(65);
        set2.setFillColor(Color.BLACK);
        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets
        LineData data = new LineData(xVals, dataSets);
        mChart.setData(data);
        ArrayList<LineDataSet> dataSets2 = new ArrayList<LineDataSet>();
        dataSets.add(set2); // add the datasets
        LineData data2 = new LineData(xVals2, dataSets);
        mChart.setData(data2);
        ll1.setVisibility(View.GONE);
        ll2.setVisibility(View.VISIBLE);
        ll3.setVisibility(View.GONE);
        ll4.setVisibility(View.GONE);

    }


    private void setDataBMI(ArrayList<String> xVals, ArrayList<Entry> yVals) {

        mChart.clear();
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "BMI");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        set1.enableDashedLine(10f, 5f, 0f);
        set1.setColor(getResColor(R.color.orange));
        set1.setCircleColor(getResColor(R.color.orange));
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);

        set1.setValueTextSize(9f);
        set1.setFillAlpha(65);
        set1.setFillColor(Color.BLACK);
//        set1.setDrawFilled(true);
        // set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(),
        // Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets
        LineData data = new LineData(xVals, dataSets);
        mChart.setData(data);

        ll1.setVisibility(View.GONE);
        ll2.setVisibility(View.GONE);
        ll3.setVisibility(View.VISIBLE);
        ll4.setVisibility(View.GONE);

    }


    private void setDataHba(ArrayList<String> xVals, ArrayList<Entry> yVals) {

        mChart.clear();
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "HbA1c");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        set1.enableDashedLine(10f, 5f, 0f);
        set1.setColor(getResColor(R.color.orange));
        set1.setCircleColor(getResColor(R.color.orange));
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);

        set1.setValueTextSize(9f);
        set1.setFillAlpha(65);
        set1.setFillColor(Color.BLACK);
//        set1.setDrawFilled(true);
        // set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(),
        // Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets
        LineData data = new LineData(xVals, dataSets);
        mChart.setData(data);
        ll1.setVisibility(View.GONE);
        ll2.setVisibility(View.GONE);
        ll3.setVisibility(View.GONE);
        ll4.setVisibility(View.VISIBLE);
    }


    private HashMap<String, List<Record>> map = new HashMap<>();
    private void getSugar() {
        RecordServer.getInstance(ReportChartActivity.this).getMonthKpisRecord(AppContext.currentUser.getUserId(),
                1 + "", year,month, new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Record> lists = (List<Record>) baseModel
                                    .getResultObj();
                            tvLow.setText(baseModel.getLow() + "\n偏低");
                            tvNormal.setText(baseModel.getNormal() + "\n正常");
                            tvHeight.setText(baseModel.getHigh() + "\n偏高");
                            map.clear();
                            ;
                            if (lists != null && lists.size() > 0) {
                                ArrayList<String> xVals = new ArrayList<String>();
                                ArrayList<Entry> yVals = new ArrayList<Entry>();
                                lyMore.removeAllViews();
                                for (int i = 0; i < lists.size(); i++) {
                                    Date date= DateUtil.parseDate(lists.get(i).getLogTime(), "yyyy-MM-dd HH:mm:ss");
                                    String key = DateUtil.getDate(date);
                                    Record item = lists.get(i);
                                    xVals.add(item.getDate());
                                    yVals.add(new Entry(Float.parseFloat(item.getLogVal1()), i));
                                    if (map.containsKey(key)) {
                                        List<Record> vls = map.get(key);
                                        vls.add(lists.get(i));
                                    } else {
                                        List<Record> vls = new ArrayList<Record>();
                                        vls.add(lists.get(i));
                                        map.put(key, vls);
                                    }
                                }
                                Iterator iter = map.entrySet().iterator();
                                while (iter.hasNext()) {
                                    Map.Entry entry = (Map.Entry) iter.next();
                                    List<Record> vls = (List<Record>) entry.getValue();
                                    View v = getLayoutInflater().inflate(R.layout.layout_bloodsugar_chart_value, null);
                                    TextView tv1 = (TextView) v.findViewById(R.id.tv_1);
                                    TextView tv2 = (TextView) v.findViewById(R.id.tv_2);
                                    TextView tv3 = (TextView) v.findViewById(R.id.tv_3);
                                    TextView tv4 = (TextView) v.findViewById(R.id.tv_4);
                                    TextView tv5 = (TextView) v.findViewById(R.id.tv_5);
                                    TextView tv6 = (TextView) v.findViewById(R.id.tv_6);
                                    TextView tv7 = (TextView) v.findViewById(R.id.tv_7);
                                    TextView tv8 = (TextView) v.findViewById(R.id.tv_8);
                                    TextView tv9 = (TextView) v.findViewById(R.id.tv_9);
                                    for (int i = 0; i < vls.size(); i++) {

                                        Record item = vls.get(i);
                                        System.out.println("item:"+item);

                                        tv1.setText(item.getMD());

                                        if ("0".equals(item.getLogVal2())) {
                                            tv2.setText(item.getLogVal1());
                                        } else {
                                            //tv2.setText("");
                                        }

                                        if ("1".equals(item.getLogVal2())) {
                                            tv3.setText(item.getLogVal1());
                                        } else {
                                            //  tv3.setText("");
                                        }

                                        if ("3".equals(item.getLogVal2())) {
                                            tv4.setText(item.getLogVal1());
                                        } else {
                                            // tv4.setText("");
                                        }

                                        tv5.setVisibility(View.VISIBLE);
                                        if ("4".equals(item.getLogVal2())) {
                                            tv5.setText(item.getLogVal1());
                                        } else {
                                            //tv5.setText("");
                                        }

                                        tv6.setVisibility(View.VISIBLE);
                                        if ("6".equals(item.getLogVal2())) {
                                            tv6.setText(item.getLogVal1());
                                        } else {
                                            //  tv6.setText("");
                                        }

                                        tv7.setVisibility(View.VISIBLE);
                                        if ("7".equals(item.getLogVal2())) {
                                            tv7.setText(item.getLogVal1());
                                        } else {
                                            // tv7.setText("");
                                        }

                                        tv8.setVisibility(View.VISIBLE);
                                        if ("5".equals(item.getLogVal2())) {
                                            tv8.setText(item.getLogVal1());
                                        } else {
                                            //  tv8.setText("");
                                        }

                                        tv9.setVisibility(View.VISIBLE);
                                        if ("2".equals(item.getLogVal2())) {
                                            tv9.setText(item.getLogVal1());
                                        } else {
                                            //  tv9.setText("");
                                        }

                                    }
                                    lyMore.addView(v);
                                }

                                setData(xVals, yVals);
                            }
                        }
                    }
                });
    }


    private void getPress() {
        RecordServer.getInstance(ReportChartActivity.this).getMonthKpisRecord(AppContext.currentUser.getUserId(),
                2 + "",year,month, new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Record> lists = (List<Record>) baseModel
                                    .getResultObj();
                            tvLow.setText(baseModel.getLow() + "\n偏低");
                            tvNormal.setText(baseModel.getNormal() + "\n正常");
                            tvHeight.setText(baseModel.getHigh() + "\n偏高");
                            if (lists != null && lists.size() >= 0) {
                                ArrayList<String> xVals = new ArrayList<String>();
                                ArrayList<Entry> yVals = new ArrayList<Entry>();
                                ArrayList<String> xVals2 = new ArrayList<String>();
                                ArrayList<Entry> yVals2 = new ArrayList<Entry>();
                                lyMore.removeAllViews();
                                ;
                                for (int i = 0; i < lists.size(); i++) {
                                    Record item = lists.get(i);
                                    xVals.add(item.getDate());
                                    xVals2.add(item.getDate());
                                    yVals.add(new Entry(Float.parseFloat(item.getLogVal1()), i));
                                    yVals2.add(new Entry(Float.parseFloat(item.getLogVal2()), i));
                                    View v = getLayoutInflater().inflate(R.layout.layout_bloodsugar_chart_value, null);
                                    TextView tv1 = (TextView) v.findViewById(R.id.tv_1);
                                    tv1.setText(item.getDate());
                                    TextView tv2 = (TextView) v.findViewById(R.id.tv_2);
                                    tv2.setText(item.getTime());
                                    TextView tv3 = (TextView) v.findViewById(R.id.tv_3);
                                    tv3.setText(item.getLogVal1());
                                    TextView tv4 = (TextView) v.findViewById(R.id.tv_4);
                                    tv4.setText(item.getLogVal2());
                                    TextView tv5 = (TextView) v.findViewById(R.id.tv_5);
                                    tv5.setVisibility(View.GONE);
                                    TextView tv6 = (TextView) v.findViewById(R.id.tv_6);
                                    tv6.setVisibility(View.GONE);
                                    TextView tv7 = (TextView) v.findViewById(R.id.tv_7);
                                    tv7.setVisibility(View.GONE);
                                    TextView tv8 = (TextView) v.findViewById(R.id.tv_8);
                                    tv8.setVisibility(View.GONE);
                                    TextView tv9 = (TextView) v.findViewById(R.id.tv_9);
                                    tv9.setVisibility(View.GONE);
                                    lyMore.addView(v);
                                }
                                setDataPress(xVals, yVals, xVals2, yVals2);
                            }
                        }
                    }
                });
    }

    private void getBMI() {
        RecordServer.getInstance(ReportChartActivity.this).getMonthKpisRecord(AppContext.currentUser.getUserId(),
                3 + "", year,month, new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Record> lists = (List<Record>) baseModel
                                    .getResultObj();
                            tvLow.setText(baseModel.getState() + "\n当前状态");
                            tvNormal.setText(baseModel.getRange() + "\n正常体重范围（kg）");
                            tvHeight.setText(("".equals(baseModel.getOffset()) ? "0" : baseModel.getOffset()) + "\n偏差（kg）");
                            if (lists != null && lists.size() >=0) {
                                ArrayList<String> xVals = new ArrayList<String>();
                                ArrayList<Entry> yVals = new ArrayList<Entry>();
                                lyMore.removeAllViews();
                                ;
                                for (int i = 0; i < lists.size(); i++) {
                                    Record item = lists.get(i);
                                    xVals.add(item.getDate());
                                    View v = getLayoutInflater().inflate(R.layout.layout_bloodsugar_chart_value, null);
                                    TextView tv1 = (TextView) v.findViewById(R.id.tv_1);
                                    tv1.setText(item.getDate());
                                    TextView tv2 = (TextView) v.findViewById(R.id.tv_2);
                                    tv2.setText(item.getTime());
                                    TextView tv3 = (TextView) v.findViewById(R.id.tv_3);
                                    tv3.setText(item.getLogVal2());
                                    TextView tv4 = (TextView) v.findViewById(R.id.tv_4);
                                    tv4.setText(item.getLogVal3());
                                    TextView tv5 = (TextView) v.findViewById(R.id.tv_5);
                                    float value2 = Float.parseFloat(item.getLogVal2());
                                    float value3 = Float.parseFloat(item.getLogVal3());
                                    tv5.setText("" + (int) (value3 / ((value2 * value2) * 0.01 * 0.01)));
                                    yVals.add(new Entry((float) (value3 / ((value2 * value2) * 0.01 * 0.01)), i));
                                    TextView tv6 = (TextView) v.findViewById(R.id.tv_6);
                                    tv6.setVisibility(View.GONE);
                                    TextView tv7 = (TextView) v.findViewById(R.id.tv_7);
                                    tv7.setVisibility(View.GONE);
                                    TextView tv8 = (TextView) v.findViewById(R.id.tv_8);
                                    tv8.setVisibility(View.GONE);
                                    TextView tv9 = (TextView) v.findViewById(R.id.tv_9);
                                    tv9.setVisibility(View.GONE);
                                    lyMore.addView(v);
                                }
                                setDataBMI(xVals, yVals);
                            }
                        }
                    }
                });
    }


    private void getHbaRecord() {
        RecordServer.getInstance(ReportChartActivity.this).getMonthKpisRecord(AppContext.currentUser.getUserId(),
                5 + "", year,month, new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Record> lists = (List<Record>) baseModel
                                    .getResultObj();
                            tvLow.setText(baseModel.getLow() + "\n偏低");
                            tvNormal.setText(baseModel.getNormal() + "\n正常");
                            tvHeight.setText(baseModel.getHigh() + "\n偏高");
                            if (lists != null && lists.size() >= 0) {
                                ArrayList<String> xVals = new ArrayList<String>();
                                ArrayList<Entry> yVals = new ArrayList<Entry>();
                                lyMore.removeAllViews();
                                ;
                                for (int i = 0; i < lists.size(); i++) {
                                    Record item = lists.get(i);
                                    xVals.add(item.getDate());
                                    View v = getLayoutInflater().inflate(R.layout.layout_bloodsugar_chart_value, null);
                                    TextView tv1 = (TextView) v.findViewById(R.id.tv_1);
                                    tv1.setText(item.getDate());
                                    TextView tv2 = (TextView) v.findViewById(R.id.tv_2);
                                    tv2.setText(item.getTime());
                                    TextView tv3 = (TextView) v.findViewById(R.id.tv_3);
                                    tv3.setText(item.getLogVal1());
                                    TextView tv4 = (TextView) v.findViewById(R.id.tv_4);
                                    tv4.setVisibility(View.GONE);
                                    TextView tv5 = (TextView) v.findViewById(R.id.tv_5);
                                    tv5.setVisibility(View.GONE);
                                    yVals.add(new Entry(Float.parseFloat(item.getLogVal1()), i));
                                    TextView tv6 = (TextView) v.findViewById(R.id.tv_6);
                                    tv6.setVisibility(View.GONE);
                                    TextView tv7 = (TextView) v.findViewById(R.id.tv_7);
                                    tv7.setVisibility(View.GONE);
                                    TextView tv8 = (TextView) v.findViewById(R.id.tv_8);
                                    tv8.setVisibility(View.GONE);
                                    TextView tv9 = (TextView) v.findViewById(R.id.tv_9);
                                    tv9.setVisibility(View.GONE);
                                    lyMore.addView(v);
                                }
                                setDataHba(xVals, yVals);
                            }
                        }
                    }
                });
    }
}
