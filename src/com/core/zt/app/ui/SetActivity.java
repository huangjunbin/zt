package com.core.zt.app.ui;

import android.view.View;
import android.view.View.OnClickListener;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.app.AppManager;
import com.core.zt.service.NoticeServer;
import com.core.zt.service.UserServer;
import com.core.zt.util.UpdateManager;
import com.core.zt.widget.HeaderBar;


public class SetActivity extends BaseActivity implements OnClickListener {
    private HeaderBar headerBar;
    private NoticeServer server;
    public SetActivity() {
        super(R.layout.activity_set);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        headerBar.setTitle(getResString(R.string.set_title));
    }

    @Override
    public void initData() {
        server=new NoticeServer(this);
    }

    @Override
    public void bindViews() {
        findViewById(R.id.set_aboutus_bar).setOnClickListener(this);
        findViewById(R.id.set_feedback_bar).setOnClickListener(this);
        findViewById(R.id.set_updatepwd).setOnClickListener(this);
        findViewById(R.id.set_update).setOnClickListener(this);
        findViewById(R.id.set_logout).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.set_update:
                UpdateManager.getUpdateManager(this).checkAppUpdate(true);
                break;
            case R.id.set_aboutus_bar:
                JumpToActivity(TextActivity.class, false);
                break;
            case R.id.set_feedback_bar:
                JumpToActivity(FeedBackActivity.class, false);
                break;
            case R.id.set_updatepwd:
                JumpToActivity(UpdatePwdActivity.class, false);
                break;
            case R.id.set_logout:
                server.removeNotices();
                AppContext.currentUser = null;
                UserServer.getInstance(this).clearCacheUser();
                AppManager.getAppManager().finishAllActivity();
                JumpToActivity(LoginActivity.class, false);
                break;
        }
    }
}
