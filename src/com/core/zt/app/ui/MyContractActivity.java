package com.core.zt.app.ui;


import com.core.zt.R;
import com.core.zt.app.fragment.ContactlistFragment;

/**
 * @className：MyContractActivity.java
 * @author: li mingtao
 * @Function: 我的联络人
 * @createDate: 2014-8-18下午3:22:02
 * @update:
 */
public class MyContractActivity extends BaseActivity {
	private ContactlistFragment contactListFragment;

	public MyContractActivity() {
		super(R.layout.activity_mycontract);
	}

	@Override
	public void initViews() {

	}

	@Override
	public void initData() {
		contactListFragment = new ContactlistFragment();
	}

	@Override
	public void bindViews() {
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment_container, contactListFragment)
				.show(contactListFragment).commit();
	}

	public void onResume() {
		super.onResume();
	}

	public void onPause() {
		super.onPause();
	}
}
