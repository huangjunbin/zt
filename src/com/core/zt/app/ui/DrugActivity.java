package com.core.zt.app.ui;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.core.zt.R;
import com.core.zt.app.adapter.DrugAdapter;
import com.core.zt.model.Drug;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.XListView;

import java.util.List;


public class DrugActivity extends BaseActivity implements XListView.IXListViewListener {
    private HeaderBar headerBar;
    private XListView xListView;
    private DrugAdapter adapter;
    public static List<Drug> dataList;


    public DrugActivity() {
        super(R.layout.activity_drug);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        xListView = (XListView) findViewById(R.id.messagelist);
        xListView.setXListViewListener(this);
    }

    @Override
    public void initData() {
        dataList = (List<Drug>) getIntent().getSerializableExtra("data");
        adapter = new DrugAdapter(DrugActivity.this, dataList);
        xListView.setAdapter(adapter);
    }

    @Override
    public void bindViews() {
        headerBar.setTitle("选择药品");
        xListView.setPullLoadEnable(false);
        xListView.setPullRefreshEnable(false);
        xListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Drug msg = dataList.get(position - 1);
                Intent data = new Intent();
                data.putExtra("data", msg);
                setResult(NewNoticeActivity.DRUGCODE, data);
                DrugActivity.this.finish();
            }
        });
    }

    @Override
    public void onRefresh() {


    }

    @Override
    public void onLoadMore() {

    }

    @Override
    protected void onResume() {
        onRefresh();
        super.onResume();
    }

}
