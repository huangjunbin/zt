package com.core.zt.app.ui;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.core.zt.R;
import com.core.zt.model.Day;
import com.core.zt.model.Drug;
import com.core.zt.model.Notice;
import com.core.zt.model.Type;
import com.core.zt.model.Week;
import com.core.zt.service.NoticeServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NewNoticeActivity extends BaseActivity {

    private HeaderBar bar;
    private RelativeLayout rlTime;
    private LinearLayout llTime;
    private TextView tvCount, tvWeek;
    private int count = 0;
    private RelativeLayout rlWeek;
    public final static int CODE = 1000;
    private int type = 1;
    private int tag = 0;
    private LinearLayout llDrug, llDrugAdd, llDrugMore;
    private Button btnDelete;
    private EditText etRemark;
    public final static int DRUGCODE = 1001;
    private List<Day> dayList = new ArrayList<>();
    private List<Week> weekList = new ArrayList<>();
    private List<Drug> drugList = new ArrayList<>();
    private NoticeServer noticeServer;
    private Notice notice;
    private CheckBox c1,c2,c3;
    Drug d = new Drug();
    Drug d1 = new Drug();
    Drug d2 = new Drug();
    private Button btnSave;
    public NewNoticeActivity() {
        super(R.layout.activity_new_notice);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);
        rlTime = (RelativeLayout) findViewById(R.id.rl_time);
        llTime = (LinearLayout) findViewById(R.id.ll_time);
        tvCount = (TextView) findViewById(R.id.tv_count);
        rlWeek = (RelativeLayout) findViewById(R.id.rl_week);
        tvWeek = (TextView) findViewById(R.id.tv_week);
        llDrug = (LinearLayout) findViewById(R.id.ll_drug);
        llDrugAdd = (LinearLayout) findViewById(R.id.ll_drud_add);
        llDrugMore = (LinearLayout) findViewById(R.id.ll_drug_more);
        btnDelete = (Button) findViewById(R.id.btn_delete);
        etRemark = (EditText) findViewById(R.id.editText);
        c1= (CheckBox) findViewById(R.id.c1);
        c2= (CheckBox) findViewById(R.id.c2);
        c3= (CheckBox) findViewById(R.id.c3);
        btnSave= (Button) findViewById(R.id.btn_save);
    }

    @Override
    public void initData() {
        d.setId("1");
        d.setDrugCnName(c1.getText().toString());
        d1.setId("2");
        d1.setDrugCnName(c2.getText().toString());
        d2.setId("3");
        d2.setDrugCnName(c3.getText().toString());
        noticeServer = new NoticeServer(this);
        llDrugMore.removeAllViews();
        type = getIntent().getIntExtra("type", 1);
        tag = getIntent().getIntExtra("tag", 0);
        notice = (Notice) getIntent().getSerializableExtra("data");
        if (tag == 0) {
            bar.setTitle("新增提醒");
            btnDelete.setVisibility(View.GONE);
        } else {
            weekList = notice.getWeekList();
            dayList = notice.getDateList();
            drugList = notice.getDrugList();
            bar.setTitle("编辑提醒");
            btnDelete.setVisibility(View.VISIBLE);
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notice != null) {
                    if (type == 2) {
                        if(!c1.isChecked()&&!c2.isChecked()&&!c3.isChecked()){
                            showToast("请选择药品");
                            return;
                        }
                    }
                    if (count <= 0) {
                        showToast("请选择提醒时间");
                        return;
                    }
                    notice.setWeekList(weekList);
                    notice.setDateList(dayList);
                    notice.setRemark(etRemark.getText().toString());
                    noticeServer.updateNotice(notice);
                    NewNoticeActivity.this.finish();
                    return;
                }
                Notice notice = new Notice();
                notice.setState(1);
                notice.setType(type);
                notice.setDrugList(drugList);
                if (type == 1) {
                    notice.setName("血糖提醒");
                }
                if (type == 2) {
                    notice.setName("用药提醒");
                    if(!c1.isChecked()&&!c2.isChecked()&&!c3.isChecked()){
                        showToast("请选择药品");
                        return;
                    }
                }
                if (type == 3) {
                    notice.setName("其他提醒");
                }
                notice.setRemark(etRemark.getText().toString());
                notice.setWeekList(weekList);
                notice.setDateList(dayList);
                if(dayList==null){
                    showToast("请选择时间");
                    return;
                }

                if (count <= 0) {
                    showToast("请选择提醒时间");
                    return;
                }
                noticeServer.saveNotice(notice);
                NewNoticeActivity.this.finish();
            }
        });
    }

    @Override
    public void bindViews() {
        if (notice != null) {
            etRemark.setText(notice.getRemark());
            count = notice.getDateList().size();
            for (int i = 0; i < notice.getDateList().size(); i++) {
                final View child = getLayoutInflater().inflate(R.layout.layout_notice_time, null);
                ImageView ivDelete = (ImageView) child.findViewById(R.id.iv_delete);
                ImageView ivEdit = (ImageView) child.findViewById(R.id.iv_edit);
                final TextView tvTime = (TextView) child.findViewById(R.id.tv_time);
                tvTime.setText(DateUtil.getZero(notice.getDateList().get(i).hour) + ":" +DateUtil.getZero(notice.getDateList().get(i).minutes));
                final Day day = dayList.get(i);
                ivDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        llTime.removeView(child);
                        count--;
                        tvCount.setText(count + "");
                        dayList.remove(tvTime.getTag());
                    }
                });
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                NewNoticeActivity.this);
                        View v = getLayoutInflater().inflate(
                                R.layout.date_time_dialog, null);
                        final DatePicker datePicker = (DatePicker) v
                                .findViewById(R.id.date_picker);
                        final TimePicker timePicker = (android.widget.TimePicker) v
                                .findViewById(R.id.time_picker);
                        timePicker.setVisibility(View.VISIBLE);
                        builder.setView(v);
                        datePicker.setVisibility(View.GONE);
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(System.currentTimeMillis());
                        datePicker.init(cal.get(Calendar.YEAR),
                                cal.get(Calendar.MONTH),
                                cal.get(Calendar.DAY_OF_MONTH), null);

                        timePicker.setIs24HourView(true);
                        timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                        timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
                        builder.setTitle("选取提醒时间");
                        builder.setPositiveButton("确  定",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        StringBuffer sb = new StringBuffer();
                                    /*sb.append(String.format("%d-%02d-%02d",
                                            datePicker.getYear(),
                                            datePicker.getMonth() + 1,
                                            datePicker.getDayOfMonth()));
                                    sb.append("  ");*/
                                        sb.append(DateUtil.getZero(timePicker.getCurrentHour()))
                                                .append(":")
                                                .append(DateUtil.getZero(timePicker.getCurrentMinute()));
                                        tvTime.setText(sb);
                                        day.setHour(timePicker.getCurrentHour());
                                        day.setMinutes(timePicker.getCurrentMinute());
                                        tvTime.setTag(day);
                                        dialog.cancel();
                                    }
                                });
                        Dialog dialog = builder.create();
                        dialog.show();
                    }
                });
                llTime.addView(child);
                tvCount.setText(count + "");
            }
            String weekStr = "";
            if (weekList != null) {
                for (Week week : weekList) {
                    String str = week.getWeek() + " ";
                    weekStr += str;
                }
                if (weekList.size() == 7) {
                    tvWeek.setText("每天");
                } else {
                    tvWeek.setText(weekStr);
                }
            }

//            for (int i = 0; i < notice.getDrugList().size(); i++) {
//                final Drug item = notice.getDrugList().get(i);
//                final View child = getLayoutInflater().inflate(R.layout.layout_notice_time, null);
//                ImageView ivDelete = (ImageView) child.findViewById(R.id.iv_delete);
//                ImageView ivEdit = (ImageView) child.findViewById(R.id.iv_edit);
//                final TextView tvTime = (TextView) child.findViewById(R.id.tv_time);
//                ivDelete.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        llDrugMore.removeView(child);
//                        drugList.remove(item);
//                    }
//                });
//                tvTime.setText(item.getDrupCategoryCnName());
//                ivEdit.setVisibility(View.INVISIBLE);
//                ivEdit.setOnClickListener(new View.OnClickListener() {
//                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//                    @Override
//                    public void onClick(View view) {
//                        llDrug.callOnClick();
//                    }
//                });
//                llDrugMore.addView(child);
//            }

            for (int i = 0; i < notice.getDrugList().size(); i++) {
               if("1".equals(notice.getDrugList().get(i).getId())){
                   c1.setChecked(true);
               }
                if("2".equals(notice.getDrugList().get(i).getId())){
                    c2.setChecked(true);
                }
                if("3".equals(notice.getDrugList().get(i).getId())){
                    c3.setChecked(true);
                }
            }

        }
        c1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    drugList.add(d);
                } else {
                    for (Drug drug:drugList){
                        if(d.getId().equals(drug.getId())){
                            drugList.remove(drug);
                            return;
                        }
                    }
                }
            }
        });
        c2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    drugList.add(d1);
                } else {
                    for (Drug drug:drugList){
                        if(d1.getId().equals(drug.getId())){
                            drugList.remove(drug);
                            return;
                        }
                    }
                }
            }
        });
        c3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    drugList.add(d2);
                } else {
                    for (Drug drug:drugList){
                        if(d2.getId().equals(drug.getId())){
                            drugList.remove(drug);
                            return;
                        }
                    }
                }
            }
        });
        if (type == 2) {
            llDrug.setVisibility(View.VISIBLE);
//            llDrug.setOnClickListener(new View.OnClickListener() {
//                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//                @Override
//                public void onClick(View view) {
//
//                    NoticeServer.getInstance(context).getDrugType(
//                            new CustomAsyncResponehandler() {
//                                public void onFinish() {
//
//                                }
//
//                                ;
//
//                                @SuppressWarnings("unchecked")
//                                @Override
//                                public void onSuccess(ResponeModel baseModel) {
//                                    super.onSuccess(baseModel);
//                                    if (baseModel != null && baseModel.isStatus()) {
//                                        List<DrugType> dataList = (List<DrugType>) baseModel
//                                                .getResultObj();
//                                        if (dataList != null && dataList.size() > 0) {
//                                            Intent intent = new Intent(NewNoticeActivity.this, DrugTypeActivity.class);
//                                            intent.putExtra("data", (Serializable) dataList);
//                                            startActivityForResult(intent, DRUGCODE, null);
//                                        }
//                                    }
//                                }
//                            });
//
//                }
//            });
        }
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noticeServer.removeNotice(notice);
                NewNoticeActivity.this.finish();
                ;
            }
        });
        rlWeek.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                List<Type> dataList = new ArrayList<Type>();
                Type t1 = new Type();
                t1.setId("7");
                t1.setName("周日");
                t1.setCheck(true);
                dataList.add(t1);
                Type t2 = new Type();
                t2.setId("1");
                t2.setName("周一");
                t2.setCheck(true);
                dataList.add(t2);
                Type t3 = new Type();
                t3.setId("2");
                t3.setName("周二");
                t3.setCheck(true);
                dataList.add(t3);
                Type t4 = new Type();
                t4.setId("3");
                t4.setName("周三");
                t4.setCheck(true);
                dataList.add(t4);
                Type t5 = new Type();
                t5.setId("4");
                t5.setName("周四");
                t5.setCheck(true);
                dataList.add(t5);
                Type t6 = new Type();
                t6.setId("5");
                t6.setName("周五");
                t6.setCheck(true);
                dataList.add(t6);
                Type t7 = new Type();
                t7.setId("6");
                t7.setName("周六");
                t7.setCheck(true);
                dataList.add(t7);
                Intent intent = new Intent(NewNoticeActivity.this, TypeActivity.class);
                intent.putExtra("data", (Serializable) dataList);
                intent.putExtra("title", "选择重复周期");
                intent.putExtra("type", 2);
                startActivityForResult(intent, CODE, null);
            }
        });
        rlTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View child = getLayoutInflater().inflate(R.layout.layout_notice_time, null);
                ImageView ivDelete = (ImageView) child.findViewById(R.id.iv_delete);
                ImageView ivEdit = (ImageView) child.findViewById(R.id.iv_edit);
                final TextView tvTime = (TextView) child.findViewById(R.id.tv_time);
                ivDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        llTime.removeView(child);
                        count--;
                        tvCount.setText(count + "");
                        dayList.remove(tvTime.getTag());
                    }
                });
                final Day day = new Day(0, 0);
                dayList.add(day);
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                NewNoticeActivity.this);
                        View v = getLayoutInflater().inflate(
                                R.layout.date_time_dialog, null);
                        final DatePicker datePicker = (DatePicker) v
                                .findViewById(R.id.date_picker);
                        final TimePicker timePicker = (android.widget.TimePicker) v
                                .findViewById(R.id.time_picker);
                        timePicker.setVisibility(View.VISIBLE);
                        builder.setView(v);
                        datePicker.setVisibility(View.GONE);
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(System.currentTimeMillis());
                        datePicker.init(cal.get(Calendar.YEAR),
                                cal.get(Calendar.MONTH),
                                cal.get(Calendar.DAY_OF_MONTH), null);

                        timePicker.setIs24HourView(true);
                        timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                        timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
                        builder.setTitle("选取提醒时间");
                        builder.setPositiveButton("确  定",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        StringBuffer sb = new StringBuffer();
                                    /*sb.append(String.format("%d-%02d-%02d",
                                            datePicker.getYear(),
                                            datePicker.getMonth() + 1,
                                            datePicker.getDayOfMonth()));
                                    sb.append("  ");*/
                                        sb.append(DateUtil.getZero(timePicker.getCurrentHour()))
                                                .append(":")
                                                .append(DateUtil.getZero(timePicker.getCurrentMinute()));
                                        tvTime.setText(sb);
                                        day.setHour(timePicker.getCurrentHour());
                                        day.setMinutes(timePicker.getCurrentMinute());
                                        tvTime.setTag(day);
                                        dialog.cancel();
                                    }
                                });
                        Dialog dialog = builder.create();
                        dialog.show();
                    }
                });
                llTime.addView(child);
                count++;
                tvCount.setText(count + "");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == CODE) {
            if (data != null) {
                weekList.clear();
                List<Type> dataList = (List<Type>) data.getSerializableExtra("data");
                String str = "";
                int i = 0;
                for (Type item : dataList) {
                    if (item.isCheck()) {
                        i++;
                        str += item.getName() + " ";
                        weekList.add(new Week(Integer.parseInt(item.getId())));
                    }
                }
                if (i == 7) {
                    tvWeek.setText("每天");
                }else if (i == 0) {
                    tvWeek.setText("永不");
                }  else {
                    tvWeek.setText(str);
                }
            } else {
                tvWeek.setText("");
            }
        }
        if (requestCode == DRUGCODE) {
            if (data != null) {
                final Drug drug = (Drug) data.getSerializableExtra("data");
                drugList.add(drug);
                final View child = getLayoutInflater().inflate(R.layout.layout_notice_time, null);
                ImageView ivDelete = (ImageView) child.findViewById(R.id.iv_delete);
                ImageView ivEdit = (ImageView) child.findViewById(R.id.iv_edit);
                final TextView tvTime = (TextView) child.findViewById(R.id.tv_time);
                ivDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        llDrugMore.removeView(child);
                        drugList.remove(drug);
                    }
                });
                tvTime.setText(drug.getDrupCategoryCnName());
                ivEdit.setVisibility(View.INVISIBLE);
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onClick(View view) {
                        llDrug.callOnClick();
                    }
                });
                llDrugMore.addView(child);


            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
