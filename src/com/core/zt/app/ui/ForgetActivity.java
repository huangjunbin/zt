package com.core.zt.app.ui;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.core.zt.R;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.UserServer;
import com.core.zt.util.StringUtils;
import com.core.zt.util.UIHelper;
import com.core.zt.widget.HeaderBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;


public class ForgetActivity extends BaseActivity implements OnClickListener {
	private static final int HAND_TIME = 1;
	// ---------
	private HeaderBar headerBar;
	private EditText editTextTel, editTextCheckCode, editTextNewPassword;
	private Button btnForget;
	private Button buttonCheckCode;
	private int timeCount=60;
	private boolean isAgree = false;
	String verifyCode="";
	String newPwd="";
	private Timer timer;
	TimerTask task = new TimerTask() {
		public void run() {
			Message message = new Message();
			message.what = 1;
			handler.sendMessage(message);
		}
	};
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
				case HAND_TIME:
					timeCount--;
					buttonCheckCode.setEnabled(false);
					buttonCheckCode.setText(timeCount + "S");
					if (timeCount <= 0) {
						buttonCheckCode.setEnabled(true);
						timer.cancel();
						timer = null;
						timeCount = 0;
						buttonCheckCode.setEnabled(true);
						buttonCheckCode.setText("获取验证码");
					}
					break;
			}
		}
	};




	public ForgetActivity() {
		super(R.layout.activity_forgetpassword);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		btnForget = (Button) findViewById(R.id.forget);
		buttonCheckCode = (Button) findViewById(R.id.forget_getCheckcode);
		editTextTel = (EditText) findViewById(R.id.forget_tel);
		editTextCheckCode = (EditText) findViewById(R.id.forget_checkcode);
		editTextNewPassword = (EditText) findViewById(R.id.forget_newpassword);

	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		headerBar.setTitle(getResString(R.string.forget_title));
		btnForget.setOnClickListener(this);
		findViewById(R.id.forget).setOnClickListener(this);
		buttonCheckCode.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.forget_getCheckcode:
			if (StringUtils.isEmpty(editTextTel.getText().toString())) {
				UIHelper.ShowMessage(context,
						getResString(R.string.forget_telempt));
				return;
			}

			if (!StringUtils.isPhoneNumberValid2(editTextTel.getText()
					.toString())) {
				UIHelper.ShowMessage(context,
						getResString(R.string.forget_telnoavail));
				return;
			}

			UserServer.getInstance(ForgetActivity.this).backPwd(editTextTel.getText().toString(), new CustomAsyncResponehandler() {
				public void onFinish() {

				};
				@SuppressWarnings("unchecked")
				@Override
				public void onSuccess(ResponeModel baseModel) {
					super.onSuccess(baseModel);
					if (baseModel != null && baseModel.isStatus()) {
						try {
							JSONObject object = new JSONObject(baseModel.getResult());
							verifyCode = object.getString("verifyCode");
							newPwd= object.getString("newPwd");
						} catch (JSONException e) {
							e.printStackTrace();
						}
						timer = new Timer(true);
						timer.schedule(task, 1000, 1000); //延时1000ms后执行，1000ms执行一次
					}
				}
			});
			break;
		case R.id.forget:
			if (checkForget()) {
				Intent intent=new Intent(ForgetActivity.this,UpdatePwdActivity.class);
				intent.putExtra("newPwd",newPwd);
				intent.putExtra("mobile",editTextTel.getText().toString());
				startActivity(intent);
				ForgetActivity.this.finish();
			}
			break;
		}
	}

	private boolean checkForget() {
		if (!editTextCheckCode.getText().toString().equals(verifyCode)) {
			UIHelper.ShowMessage(context, "验证码不正确");
			return false;
		}

		return true;
	}

}
