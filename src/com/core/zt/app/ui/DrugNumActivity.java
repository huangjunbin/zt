package com.core.zt.app.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.core.zt.R;
import com.core.zt.model.DrugChildItem;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class DrugNumActivity extends BaseActivity {

    private HeaderBar bar;
    private DrugChildItem data;
    private TextView tvTime, tvNum;
    private Button btnEdit, btnRemove;
    private Intent intent;
    private String unit;
    private  int num=1;

    public DrugNumActivity() {
        super(R.layout.activity_drug_num);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvNum = (TextView) findViewById(R.id.tv_num);
        btnEdit = (Button) findViewById(R.id.btn_save);
        btnRemove = (Button) findViewById(R.id.btn_delete);
    }

    @Override
    public void initData() {
        data = (DrugChildItem) getIntent().getSerializableExtra("data");
        unit=getIntent().getStringExtra("unit");
        bar.setTitle("剂量和时间");
    }

    @Override
    public void bindViews() {

        if (data != null) {
            tvNum.setText(data.getDose() + unit);
            Date date = DateUtil.parseDate(data.getMedicationTime(), "HH:mm");
            tvTime.setText(DateUtil.getTimeNo(date));

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    data.setDose(num);
                    data.setMedicationTime(tvTime.getText().toString());
                    if(tvTime.getText().toString().length()<1){
                        showToast("请选择时间");
                        return;
                    }
                    Intent intent = new Intent();
                    intent.putExtra("child", (Serializable) data);
                    intent.putExtra("count", getIntent().getIntExtra("count", -1));
                    intent.setClass(DrugNumActivity.this, NewDrugActivity.class);
                    setResult(NewDrugActivity.DRUGNUMCODE, intent);
                    DrugNumActivity.this.finish();
                }
            });
            btnRemove.setVisibility(View.VISIBLE);
            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.putExtra("count", getIntent().getIntExtra("count", -1));
                    intent.setClass(DrugNumActivity.this, NewDrugActivity.class);
                    setResult(NewDrugActivity.DRUGNUMCODE, intent);
                    DrugNumActivity.this.finish();
                }
            });
        } else {
            tvNum.setText(1 + unit);
            data=new DrugChildItem();
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    data.setDose(num);
                    data.setMedicationTime(tvTime.getText().toString());
                    if(tvTime.getText().toString().length()<1){
                        showToast("请选择时间");
                        return;
                    }
                    Intent intent = new Intent();
                    intent.putExtra("child", (Serializable) data);
                    intent.putExtra("count", getIntent().getIntExtra("count", -1));
                    intent.setClass(DrugNumActivity.this, NewDrugActivity.class);
                    setResult(NewDrugActivity.DRUGNUMCODE, intent);
                    DrugNumActivity.this.finish();
                }
            });
            btnRemove.setVisibility(View.GONE);
        }

        findViewById(R.id.rl_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        DrugNumActivity.this);
                View v = getLayoutInflater().inflate(
                        R.layout.date_time_dialog, null);
                final DatePicker datePicker = (DatePicker) v
                        .findViewById(R.id.date_picker);
                final TimePicker timePicker = (TimePicker) v
                        .findViewById(R.id.time_picker);
                timePicker.setVisibility(View.VISIBLE);
                builder.setView(v);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                datePicker.init(cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH), null);
                datePicker.setVisibility(View.GONE);
                timePicker.setIs24HourView(true);
                timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
                builder.setTitle("选取时间");
                builder.setPositiveButton("确  定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                StringBuffer sb = new StringBuffer();
                                sb.append(DateUtil.getZero(timePicker.getCurrentHour()))
                                        .append(":")
                                        .append(DateUtil.getZero(timePicker.getCurrentMinute()));
                                tvTime.setText(sb);
                                dialog.cancel();
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });
        findViewById(R.id.rl_num).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 AlertDialog.Builder builder = new AlertDialog.Builder(
                         DrugNumActivity.this);
                 View v = getLayoutInflater().inflate(
                         R.layout.date_num_dialog, null);
                 final NumberPicker numberPicker = (NumberPicker) v
                         .findViewById(R.id.num_picker);

                 numberPicker.setMinValue(1);
                 numberPicker.setMaxValue(100);
                 //设置np2的当前值
                 numberPicker.setValue(1);
                 numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

                                                            @Override
                                                            public void onValueChange(NumberPicker picker, int oldVal,
                                                                                      int newVal) {
                                                                tvNum.setText(newVal + unit);
                                                                num=newVal;
                                                            }
                                                        }


                 );
                 builder.setView(v);
                 builder.setTitle("选取数量");
                 builder.setPositiveButton("确  定",
                         new DialogInterface.OnClickListener()

                         {

                             @Override
                             public void onClick(DialogInterface dialog,
                                                 int which) {

                                 dialog.cancel();
                             }
                         }

                 );
                 Dialog dialog = builder.create();
                 dialog.show();
             }
         });
    }


}
