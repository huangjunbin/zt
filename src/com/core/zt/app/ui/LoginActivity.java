package com.core.zt.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.UserServer;
import com.core.zt.widget.HeaderBar;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;


public class LoginActivity extends BaseActivity {
    private HeaderBar headerBar;
    private Button btnLogin, btnRegister;
    private EditText etAccount, etPassWord;
    private TextView textForgetPwd;
    private String mobileNumber, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public LoginActivity() {
        super(R.layout.activity_login);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        headerBar.setTitle(getResString(R.string.login_title));
        headerBar.back.setVisibility(View.GONE);

        btnLogin = (Button) findViewById(R.id.login);
        btnRegister = (Button) findViewById(R.id.go_register);
        etAccount = (EditText) findViewById(R.id.user_account);
        etPassWord = (EditText) findViewById(R.id.user_pwd);
        textForgetPwd = (TextView) findViewById(R.id.forget_pwd);
    }

    @Override
    public void initData() {

    }

    @Override
    public void bindViews() {


        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });

        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileNumber = etAccount.getText().toString();
                password = etPassWord.getText().toString();
                if ("".equals(mobileNumber) && "".equals(password)) {
                    showToast(getResString(R.string.login_no_nameandpwd));
                    return;
                }
                if (cantEmpty(etAccount, R.string.login_inputname))
                    return;
                if (cantEmpty(etPassWord, R.string.login_inputpwd))
                    return;

                UserServer.getInstance(context).login(
                        etAccount.getText().toString(),
                        etPassWord.getText().toString(), new CustomAsyncResponehandler() {
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    loginHX(AppContext.currentUser.getUserName(), AppContext.currentUser.getPassword(), new EMCallBack() {
                                        @Override
                                        public void onSuccess() {
                                            JumpToActivity(MainActivity.class, false);
                                        }

                                        @Override
                                        public void onError(int i, String s) {
                                            JumpToActivity(MainActivity.class, false);
                                        }

                                        @Override
                                        public void onProgress(int i, String s) {

                                        }
                                    });

                                }
                            }
                        });

            }
        });

        textForgetPwd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                JumpToActivity(ForgetActivity.class,false);
            }
        });
    }


    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void loginHX(final String mobileNumber, final String password, final EMCallBack callback) {
        AppContext.getApplication().setPassword(password);
        Log.d("HX", mobileNumber + "_____" + password);
        // 调用sdk登陆方法登陆聊天服务器
        new Thread(new Runnable() {
            public void run() {
                EMChatManager.getInstance().login(mobileNumber, password,
                        callback);
            }
        }).start();
    }
}
