package com.core.zt.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.core.zt.R;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.UserServer;
import com.core.zt.util.StringUtils;
import com.core.zt.util.UIHelper;
import com.core.zt.widget.HeaderBar;


public class FeedBackActivity extends BaseActivity {
	private HeaderBar headerBar;
	private EditText editTextContent, editTextTel;

	public FeedBackActivity() {
		super(R.layout.activity_feedback);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.header);
		headerBar.setTitle(getResString(R.string.feedback_title));
		headerBar.top_right_btn.setText("提交");

		editTextContent = (EditText) findViewById(R.id.feedback_content);
		editTextTel = (EditText) findViewById(R.id.feedback_tel);
	}

	@Override
	public void initData() {

	}
	
	@Override
	public void bindViews() {
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (checkInput()) {

				}
			}
		});
	}

	private boolean checkInput() {
		if (StringUtils.isEmpty(editTextContent.getText().toString())) {
			UIHelper.ShowMessage(context, "请先输入描述");
			return false;
		}
		if (StringUtils.isEmpty(editTextTel.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入标题");
			return false;
		}

		UserServer.getInstance(FeedBackActivity.this).addFeedback(editTextTel.getText().toString(), editTextContent.getText().toString(), new CustomAsyncResponehandler() {
			public void onFinish() {

			}

			;

			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(ResponeModel baseModel) {
				super.onSuccess(baseModel);
				if (baseModel != null && baseModel.isStatus()) {
					showToast("提交成功");
					FeedBackActivity.this.finish();
				}
			}
		});
		return true;
	}
}
