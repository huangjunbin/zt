package com.core.zt.app.ui;


import com.core.zt.R;
import com.core.zt.app.fragment.ChatAllHistoryFragment;

/**
 * @className：SearchFriendActivity.java
 * @author: li mingtao
 * @Function: 搜寻朋友
 * @createDate: 2014-8-18下午3:24:13
 * @update:
 */
public class MessageHistoryActivity extends BaseActivity {
	private ChatAllHistoryFragment chatAllHistoryFragment;

	public MessageHistoryActivity() {
		super(R.layout.activity_messagehistory);
	}

	@Override
	public void initViews() {

	}

	@Override
	public void initData() {
		chatAllHistoryFragment = new ChatAllHistoryFragment();
	}

	@Override
	public void bindViews() {
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment_container, chatAllHistoryFragment)
				.show(chatAllHistoryFragment).commit();
	}

	public void onResume() {
		super.onResume();
	}

	public void onPause() {
		super.onPause();
	}
}
