package com.core.zt.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.core.zt.R;
import com.core.zt.model.City;
import com.core.zt.model.District;
import com.core.zt.model.Province;
import com.core.zt.widget.wheel.OnWheelChangedListener;
import com.core.zt.widget.wheel.WheelView;
import com.core.zt.widget.wheel.adapter.ArrayWheelAdapter;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class AddressActivity extends Activity implements
		OnClickListener, OnWheelChangedListener {



	private WheelView mViewProvince;
	private WheelView mViewCity;
	private WheelView mViewDistrict;
	private Button mBtnConfirm;
	/**
	 * 所有省
	 */
	protected String[] mProvinceDatas;
	/**
	 * key - 省 value - 市
	 */
	protected Map<String, String[]> mCitisDatasMap = new HashMap<String, String[]>();
	/**
	 * key - 市 values - 区
	 */
	protected Map<String, String[]> mDistrictDatasMap = new HashMap<String, String[]>();

	/**
	 * key - 区 values - 邮编
	 */
	protected Map<String, String> mZipcodeDatasMap = new HashMap<String, String>();

	/**
	 * 当前省的名称
	 */
	protected String mCurrentProviceName;
	/**
	 * 当前市的名称
	 */
	protected String mCurrentCityName;
	/**
	 * 当前区的名称
	 */
	protected String mCurrentDistrictName = "";

	/**
	 * 当前区的邮政编码
	 */
	protected String mCurrentZipCode = "";


	/**
	 * 解析省市区的XML数据
	 */

	protected void initProvinceDatas() {
		List<Province> provinceList = null;
		AssetManager asset = getAssets();
		try {
			InputStream input = asset.open("province_data.xml");
			// 创建一个解析xml的工厂对象
			SAXParserFactory spf = SAXParserFactory.newInstance();
			// 解析xml
			SAXParser parser = spf.newSAXParser();
			XmlParserHandler handler = new XmlParserHandler();
			parser.parse(input, handler);
			input.close();
			// 获取解析出来的数据
			provinceList = handler.getDataList();
			// */ 初始化默认选中的省、市、区
			if (provinceList != null && !provinceList.isEmpty()) {
				mCurrentProviceName = provinceList.get(0).getName();
				List<City> cityList = provinceList.get(0).getCityList();
				if (cityList != null && !cityList.isEmpty()) {
					mCurrentCityName = cityList.get(0).getName();
					List<District> districtList = cityList.get(0)
							.getDistrictList();
					mCurrentDistrictName = districtList.get(0).getName();
					mCurrentZipCode = districtList.get(0).getZipcode();
				}
			}
			// */
			mProvinceDatas = new String[provinceList.size()];
			for (int i = 0; i < provinceList.size(); i++) {
				// 遍历所有省的数据
				mProvinceDatas[i] = provinceList.get(i).getName();
				List<City> cityList = provinceList.get(i).getCityList();
				String[] cityNames = new String[cityList.size()];
				for (int j = 0; j < cityList.size(); j++) {
					// 遍历省下面的所有市的数据
					cityNames[j] = cityList.get(j).getName();
					List<District> districtList = cityList.get(j)
							.getDistrictList();
					String[] distrinctNameArray = new String[districtList
							.size()];
					District[] distrinctArray = new District[districtList
							.size()];
					for (int k = 0; k < districtList.size(); k++) {
						// 遍历市下面所有区/县的数据
						District District = new District(districtList.get(k)
								.getName(), districtList.get(k).getZipcode());
						// 区/县对于的邮编，保存到mZipcodeDatasMap
						mZipcodeDatasMap.put(districtList.get(k).getName(),
								districtList.get(k).getZipcode());
						distrinctArray[k] = District;
						distrinctNameArray[k] = District.getName();
					}
					// 市-区/县的数据，保存到mDistrictDatasMap
					mDistrictDatasMap.put(cityNames[j], distrinctNameArray);
				}
				// 省-市的数据，保存到mCitisDatasMap
				mCitisDatasMap.put(provinceList.get(i).getName(), cityNames);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {

		}
	}

	String address = null;
	private static final int MESSAGE_PHONEUPDATE_FAILED = 2;
	private static final int MESSAGE_PHONEUPDATE_SUCCEED = 1;
	private AlertDialog mDialog;
	private View view;
	private int pProvice = -1, pCity = 0, pDistrict = 0;




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_address);

		setUpViews();
		setUpListener();
		setUpData();
		findViewById(R.id.root).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AddressActivity.this.finish();
			}
		});
	}

	private void setUpViews() {
		mViewProvince = (WheelView) findViewById(R.id.id_province);
		mViewCity = (WheelView) findViewById(R.id.id_city);
		mViewDistrict = (WheelView) findViewById(R.id.id_district);
		mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
	}

	private void setUpListener() {
		mViewProvince.addChangingListener(this);
		mViewCity.addChangingListener(this);
		mViewDistrict.addChangingListener(this);
		mBtnConfirm.setOnClickListener(this);
	}

	private void setUpData() {
		initProvinceDatas();
		mViewProvince.setViewAdapter(new ArrayWheelAdapter<String>(
				AddressActivity.this, mProvinceDatas));
		mViewProvince.setVisibleItems(7);
		mViewCity.setVisibleItems(7);
		mViewDistrict.setVisibleItems(7);
		updateCities();
		updateAreas();
		address = getIntent().getStringExtra("address");
		if (address != null && !"".equals(address) && !"未填写".equals(address)) {
			String str[] = address.split(",");
			if(str.length>2) {
				mCurrentProviceName = str[0].trim();
				mCurrentCityName = str[1].trim();
				mCurrentDistrictName = str[2].trim();
				for (int i = 0; i < mProvinceDatas.length; i++) {
					if (mCurrentProviceName.equals(mProvinceDatas[i])) {
						pProvice = i;
					}
				}
				mViewProvince.setCurrentItem(pProvice);
				if (mCitisDatasMap.get(mCurrentProviceName) != null) {
					for (int j = 0; j < mCitisDatasMap.get(mCurrentProviceName).length; j++) {

						if (mCurrentCityName.equals(mCitisDatasMap
								.get(mCurrentProviceName)[j])) {
							pCity = j;
						}

					}
					mViewCity.setCurrentItem(pCity);
				}

				if (mDistrictDatasMap.get(mCurrentCityName) != null) {
					for (int z = 0; z < mDistrictDatasMap.get(mCurrentCityName).length; z++) {

						if (mCurrentDistrictName.equals(mDistrictDatasMap
								.get(mCurrentCityName)[z])) {
							pDistrict = z;
						}

					}
					mViewDistrict.setCurrentItem(pDistrict);
				}
			}
		}
	}

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub
		if (wheel == mViewProvince) {
			updateCities();
		} else if (wheel == mViewCity) {
			updateAreas();
		} else if (wheel == mViewDistrict) {

			mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[newValue];
			mCurrentZipCode = mZipcodeDatasMap.get(mCurrentDistrictName);
		}
	}

	private void updateAreas() {
		int pCurrent = mViewCity.getCurrentItem();
		mCurrentCityName = mCitisDatasMap.get(mCurrentProviceName)[pCurrent];
		String[] areas = mDistrictDatasMap.get(mCurrentCityName);

		if (areas == null) {
			areas = new String[] { "" };
		}
		mViewDistrict
				.setViewAdapter(new ArrayWheelAdapter<String>(this, areas));
		mViewDistrict.setCurrentItem(0);
		mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[0];
		mCurrentZipCode = mZipcodeDatasMap.get(mCurrentDistrictName);
	}

	private void updateCities() {
		int pCurrent = mViewProvince.getCurrentItem();
		mCurrentProviceName = mProvinceDatas[pCurrent];
		String[] cities = mCitisDatasMap.get(mCurrentProviceName);
		if (cities == null) {
			cities = new String[] { "" };
		}
		mViewCity.setViewAdapter(new ArrayWheelAdapter<String>(this, cities));
		mViewCity.setCurrentItem(0);
		updateAreas();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_confirm:
			showSelectedResult();
			break;
		default:
			break;
		}
	}

	private void showSelectedResult() {
		address = mCurrentProviceName + " , " + mCurrentCityName + " , "
				+ mCurrentDistrictName;
		Intent intent = new Intent();
		intent.putExtra("address", address);
		setResult(RESULT_OK, intent);
		finish();
	}

	public class XmlParserHandler extends DefaultHandler {

		/**
		 * 存储所有的解析对象
		 */
		private List<Province> provinceList = new ArrayList<Province>();

		public XmlParserHandler() {

		}

		public List<Province> getDataList() {
			return provinceList;
		}

		@Override
		public void startDocument() throws SAXException {
			// 当读到第一个开始标签的时候，会触发这个方法
		}

		Province Province = new Province();
		City City = new City();
		District District = new District();

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			// 当遇到开始标记的时候，调用这个方法
			if (qName.equals("province")) {
				Province = new Province();
				Province.setName(attributes.getValue(0));
				Province.setCityList(new ArrayList<City>());
			} else if (qName.equals("city")) {
				City = new City();
				City.setName(attributes.getValue(0));
				City.setDistrictList(new ArrayList<District>());
			} else if (qName.equals("district")) {
				District = new District();
				District.setName(attributes.getValue(0));
				District.setZipcode(attributes.getValue(1));
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			// 遇到结束标记的时候，会调用这个方法
			if (qName.equals("district")) {
				City.getDistrictList().add(District);
			} else if (qName.equals("city")) {
				Province.getCityList().add(City);
			} else if (qName.equals("province")) {
				provinceList.add(Province);
			}
		}

		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {
		}

	}
	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}


}
