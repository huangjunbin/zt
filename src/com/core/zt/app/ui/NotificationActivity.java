package com.core.zt.app.ui;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.core.zt.R;
import com.core.zt.app.adapter.NotificationAdapter;
import com.core.zt.model.Notification;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.LessonServer;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.XListView;

import java.util.ArrayList;
import java.util.List;


public class NotificationActivity extends BaseActivity implements XListView.IXListViewListener {
    private HeaderBar headerBar;
    private XListView xListView;
    private NotificationAdapter adapter;
    public static List<Notification> dataList;
    private int page = 1;
    private int rows = 5;
    private boolean isRefresh = true;
    private int type = 0;
    private String id;

    public NotificationActivity() {
        super(R.layout.activity_notification);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        xListView = (XListView) findViewById(R.id.messagelist);
        xListView.setXListViewListener(this);
    }

    @Override
    public void initData() {
        id = getIntent().getStringExtra("id");
        type = getIntent().getIntExtra("type", 0);
        dataList = new ArrayList<Notification>();
        adapter = new NotificationAdapter(NotificationActivity.this, dataList);
        xListView.setAdapter(adapter);
    }

    @Override
    public void bindViews() {
        headerBar.setTitle("我的消息");
        xListView.setPullLoadEnable(true);
        xListView.setFooterDividersEnabled(false);
        xListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

            }
        });
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        page = 1;
        getNotification();

    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        getNotification();
    }

    @Override
    protected void onResume() {
        onRefresh();
        super.onResume();
    }

    private void getNotification() {
        LessonServer.getInstance(context).getNotification(page + "", rows + "", new CustomAsyncResponehandler() {
            public void onFinish() {
                xListView.stopRefresh();
                xListView.stopLoadMore();
            }

            ;

            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    List<Notification> lists = (List<Notification>) baseModel
                            .getResultObj();
                    if (lists != null && lists.size() > 0) {
                        dataList.clear();
                        dataList.addAll(0, lists);
                        adapter.notifyDataSetChanged();
                        ;
                    }
                }
            }
        });
    }
}
