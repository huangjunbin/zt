package com.core.zt.app.ui;

import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.widget.HeaderBar;

import org.w3c.dom.Text;


public class TextActivity extends BaseActivity {
	private HeaderBar headerBar;
	private Text text;

	public TextActivity() {
		super(R.layout.activity_text);
	}

	@Override
	public void firstLoad() {
		super.firstLoad();
		text = (Text) getIntent().getSerializableExtra("data");
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.header);
		headerBar.setTitle("关于我们");
		TextView textView = (TextView) findViewById(R.id.textcontent);
		textView.setText("关于知糖");
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {

	}

}
