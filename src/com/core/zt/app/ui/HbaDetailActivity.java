package com.core.zt.app.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;

import java.util.Date;

public class HbaDetailActivity extends BaseActivity {

    private HeaderBar bar;
    private Record data;
    private TextView tvTimeDetail,tvValue;
    private Button btnEdit,btnRemove;
    private Intent intent;
    public HbaDetailActivity() {
        super(R.layout.activity_hba_detail);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);
        tvTimeDetail= (TextView) findViewById(R.id.tv_time_detail);
        tvValue= (TextView) findViewById(R.id.tv_value);
        btnEdit= (Button) findViewById(R.id.btn_save);
        btnRemove= (Button) findViewById(R.id.btn_delete);
    }

    @Override
    public void initData() {
        data= (Record) getIntent().getSerializableExtra("data");
        bar.setTitle("HbA1c");
    }

    @Override
    public void bindViews() {
        tvValue.setText(data.getLogVal1()+"%");
        Date date= DateUtil.parseDate(data.getLogTime(),"yyyy-MM-dd HH:mm");
        tvTimeDetail.setText(DateUtil.getDateNoSecond(date));
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent();
                intent.putExtra("data", data);
                intent.setClass(HbaDetailActivity.this, NewHbarActivity.class);
                startActivity(intent);
            }
        });
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSureDialog(HbaDetailActivity.this, "确定删除?", "确定", new OnSurePress() {
                    @Override
                    public void onClick(View view) {
                        RecordServer.getInstance(HbaDetailActivity.this).removeRecord(
                                data.getId(), new CustomAsyncResponehandler() {
                                    public void onFinish() {

                                    }

                                    ;

                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {
                                            showToast("删除成功");
                                            HbaDetailActivity.this.finish();
                                        }
                                    }
                                });
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {

                    }
                });

            }
        });
    }


}
