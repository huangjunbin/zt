package com.core.zt.app.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.core.zt.R;
import com.core.zt.app.AppManager;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.TuneWheel;

import java.util.Calendar;
import java.util.Date;

public class NewHbarActivity extends BaseActivity {

    private HeaderBar bar;
    private RelativeLayout rlWeek;
    private TextView tvTime;
    private Record record;
    private Button btnSave;
    private EditText etValue;
    private Record data;
    private TuneWheel tw;

    public NewHbarActivity() {
        super(R.layout.activity_new_hba);

    }

    @Override
    public void initViews() {
        tw= (TuneWheel) findViewById(R.id.tw_value);
        data = (Record) getIntent().getSerializableExtra("data");
        etValue = (EditText) findViewById(R.id.et_Value);
        bar = (HeaderBar) findViewById(R.id.header);
        btnSave = (Button) findViewById(R.id.btn_save);
        rlWeek = (RelativeLayout) findViewById(R.id.rl_week);
        tvTime = (TextView) findViewById(R.id.tv_time);
    }

    @Override
    public void initData() {
    }

    @Override
    public void bindViews() {


        tw.setValueChangeListener(new TuneWheel.OnValueChangeListener() {
            @Override
            public void onValueChange(float value) {
                etValue.setText(value/10+"");
            }
        });
        record = new Record();
        if (data != null) {
            Date date= DateUtil.parseDate(data.getLogTime(), "yyyy-MM-dd HH:mm");
            tvTime.setText(DateUtil.getDateNoSecond(date));
            etValue.setText(data.getLogVal1());
            record.setId(data.getId());
            bar.setTitle("编辑HbA1c记录");
            tw.initViewParam((int)(Float.parseFloat(data.getLogVal1())*10),200,TuneWheel.MOD_TYPE_ONE);
        } else {
            tw.initViewParam(60,200,TuneWheel.MOD_TYPE_ONE);
            bar.setTitle("新增HbA1c记录");
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            StringBuffer sb = new StringBuffer();
            sb.append(String.format("%d-%02d-%02d",
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH) + 1,
                    cal.get(Calendar.DAY_OF_MONTH)));
            sb.append("  ");
            sb.append(cal.get(Calendar.HOUR_OF_DAY))
                    .append(":")
                    .append(cal.get(Calendar.MINUTE));
            tvTime.setText(sb);
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(etValue.getText().toString())) {
                    showToast("请输入数值");
                }
                if ("".equals(tvTime.getText().toString())) {
                    showToast("请选择时间");
                }

                record.setLogTime(tvTime.getText().toString());
                record.setLogVal1(etValue.getText().toString());
                record.setLogType("5");
                RecordServer.getInstance(NewHbarActivity.this).saveRecord(
                        record, new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            ;

                            @SuppressWarnings("unchecked")
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    if (data != null) {
                                        showToast("编辑成功");
                                        AppManager.getAppManager().finishActivity(NewHbarActivity.class);
                                        NewHbarActivity.this.finish();
//                                        Intent intent=new Intent();
//                                        intent.putExtra("data", record);
//                                        intent.setClass(NewHbarActivity.this, BloodSugarDetailActivity.class);
//                                        startActivity(intent);
                                    }else{
                                        showToast("添加成功");
                                    }
                                    NewHbarActivity.this.finish();
                                }
                            }
                        });
            }
        });
        rlWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        NewHbarActivity.this);
                View v = getLayoutInflater().inflate(
                        R.layout.date_time_dialog, null);
                final DatePicker datePicker = (DatePicker) v
                        .findViewById(R.id.date_picker);
                final TimePicker timePicker = (TimePicker) v
                        .findViewById(R.id.time_picker);
                timePicker.setVisibility(View.VISIBLE);
                datePicker.setVisibility(View.VISIBLE);
                builder.setView(v);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                datePicker.init(cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH), null);

                timePicker.setIs24HourView(true);
                timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
                builder.setTitle("选取起始时间");
                builder.setPositiveButton("确  定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                StringBuffer sb = new StringBuffer();
                                sb.append(String.format("%d-%02d-%02d",
                                        datePicker.getYear(),
                                        datePicker.getMonth() + 1,
                                        datePicker.getDayOfMonth()));
                                sb.append("  ");
                                sb.append(timePicker.getCurrentHour())
                                        .append(":")
                                        .append(timePicker.getCurrentMinute());
                                tvTime.setText(sb);
                                dialog.cancel();
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });
    }




}
