package com.core.zt.app.ui;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.db.dao.UserDao;
import com.core.zt.model.ResponeModel;
import com.core.zt.model.User;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.UserServer;
import com.core.zt.util.StringUtils;
import com.core.zt.util.UIHelper;
import com.core.zt.widget.HeaderBar;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;


public class RegisterActivity extends BaseActivity implements OnClickListener {

    private static final int HAND_TIME = 1;
    private HeaderBar headerBar;
    private Button buttonCheckCode;
    private EditText editTextName, editTextTel, editTextCheckcode,
            editTextPasworrd, editTextSurePassword;
    private Timer timer;
    private int timeCount=60;
    private boolean isAgree = false;
    String verifyCode;

    TimerTask task = new TimerTask() {
        public void run() {
            Message message = new Message();
            message.what = 1;
            handler.sendMessage(message);
        }
    };
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case HAND_TIME:
                    timeCount--;
                    buttonCheckCode.setEnabled(false);
                    buttonCheckCode.setText(timeCount + "S");
                    if (timeCount <= 0) {
                        buttonCheckCode.setEnabled(true);
                        timer.cancel();
                        timer = null;
                        timeCount = 0;
                        buttonCheckCode.setEnabled(true);
                        buttonCheckCode.setText("获取验证码");
                    }
                    break;
            }
        }
    };

    public RegisterActivity() {
        super(R.layout.activity_register);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        buttonCheckCode = (Button) findViewById(R.id.register_checkcode);
        editTextName = (EditText) findViewById(R.id.register_name);
        editTextTel = (EditText) findViewById(R.id.register_tel);
        editTextPasworrd = (EditText) findViewById(R.id.register_password);
        editTextCheckcode = (EditText) findViewById(R.id.register_incheckcode);
        editTextSurePassword = (EditText) findViewById(R.id.register_surepassword);
    }

    @Override
    public void initData() {

    }

    @Override
    public void bindViews() {

        headerBar.setTitle(getResString(R.string.register_title));
        buttonCheckCode.setOnClickListener(this);
        findViewById(R.id.register).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_checkcode:

                if (StringUtils.isEmpty(editTextTel.getText().toString())) {
                    UIHelper.ShowMessage(context,
                            getResString(R.string.register_telempt));
                    return;
                }

                if (!StringUtils.isPhoneNumberValid2(editTextTel.getText()
                        .toString())) {
                    UIHelper.ShowMessage(context,
                            getResString(R.string.register_telnoavail));
                    return;
                }
                UserServer.getInstance(RegisterActivity.this).getVerifyCode(editTextTel.getText().toString(), new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            showToast("获取成功");
                            try {
                                JSONObject object = new JSONObject(baseModel.getResult());
                                verifyCode = object.getString("verifyCode");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            timer = new Timer(true);
                            timer.schedule(task, 1000, 1000); //延时1000ms后执行，1000ms执行一次

                        }
                    }
                });

                break;
            case R.id.register:
                if (checkRegister()) {
                    UserServer.getInstance(RegisterActivity.this).userRegister(editTextTel.getText().toString(), editTextPasworrd.getText().toString(), editTextName.getText().toString(), new CustomAsyncResponehandler() {
                        public void onFinish() {

                        }

                        ;

                        @SuppressWarnings("unchecked")
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel != null && baseModel.isStatus()) {
                                showToast("注册成功");
                                AppContext.currentUser = (User) baseModel.getResultObj();
                                UserDao userDao = new UserDao(RegisterActivity.this);
                                if (userDao != null) {
                                    userDao.delete();
                                    userDao.insert(AppContext.currentUser);
                                }

                                loginHX(AppContext.currentUser.getUserName(), AppContext.currentUser.getPassword(), new EMCallBack() {
                                    @Override
                                    public void onSuccess() {
                                        RegisterActivity.this.finish();
                                        JumpToActivity(MainActivity.class, false);
                                    }

                                    @Override
                                    public void onError(int i, String s) {
                                        RegisterActivity.this.finish();
                                        JumpToActivity(MainActivity.class, false);
                                    }

                                    @Override
                                    public void onProgress(int i, String s) {

                                    }
                                });

                            }
                        }
                    });

                }
                break;
        }
    }
    public void loginHX(final String mobileNumber, final String password, final EMCallBack callback) {
        AppContext.getApplication().setPassword(password);
        Log.d("HX", mobileNumber + "_____" + password);
        // 调用sdk登陆方法登陆聊天服务器
        new Thread(new Runnable() {
            public void run() {
                EMChatManager.getInstance().login(mobileNumber, password,
                        callback);
            }
        }).start();
    }

    private boolean checkRegister() {
        if (StringUtils.isEmpty(editTextName.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_nameempt));
            return false;
        }

        if (StringUtils.isEmpty(editTextTel.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_telempt));
            return false;
        }
        if (StringUtils.isEmpty(editTextCheckcode.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_checkcodeempt));
            return false;
        }

        if (StringUtils.isEmpty(editTextPasworrd.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_passwordempt));
            return false;
        }

        if (StringUtils.isEmpty(editTextSurePassword.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_surepasswordempt));
            return false;
        }

        // ---------------
        if (!StringUtils.isPhoneNumberValid2(editTextTel.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_telnoavail));
            return false;
        }

        if (!editTextPasworrd.getText().toString()
                .equals(editTextSurePassword.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_passworddiffer));
            return false;
        }

        if (editTextPasworrd.getText().toString().length() < 6) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_passwordlen));
            return false;
        }

        if (!editTextCheckcode.getText().toString().equals(verifyCode)) {
            UIHelper.ShowMessage(context, "验证码不正确");
            return false;
        }

        return true;
    }
}
