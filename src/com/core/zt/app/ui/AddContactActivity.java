package com.core.zt.app.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.core.zt.R;
import com.core.zt.app.adapter.AddFriendsAdapter;
import com.core.zt.model.Doctor;
import com.core.zt.model.ResponeModel;
import com.core.zt.model.User;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.IMService;

import java.util.ArrayList;
import java.util.List;

public class AddContactActivity extends BaseActivity {
	private EditText editText;
	private LinearLayout searchedUserLayout;

	private Button searchBtn;

	private AddFriendsAdapter adapter;
	private ListView listView;
	private List<Doctor> memberList;

	public AddContactActivity() {
		super(R.layout.activity_add_contact);
	}

	@Override
	public void initViews() {
		listView = (ListView) findViewById(R.id.list);
		editText = (EditText) findViewById(R.id.edit_note);
		searchedUserLayout = (LinearLayout) findViewById(R.id.ll_user);
		searchBtn = (Button) findViewById(R.id.search);
	}

	@Override
	public void initData() {
		memberList = new ArrayList<Doctor>();
		adapter = new AddFriendsAdapter(this, R.layout.row_addfriends,
				memberList);
		listView.setAdapter(adapter);
	}

	@Override
	public void bindViews() {

	}

	/**
	 * 查找contact
	 * 
	 * @param v
	 */
	public void searchContact(View v) {
		final String name = editText.getText().toString();
		String saveText = searchBtn.getText().toString();

		if (getString(R.string.button_search).equals(saveText)) {

			if (TextUtils.isEmpty(name)) {

				showToast("请输入用户名");
				return;
			}
			searchFriends();
			// TODO 从服务器获取此contact,如果不存在提示不存在此用户

			// 服务器存在此用户，显示此用户和添加按钮
			searchedUserLayout.setVisibility(View.VISIBLE);

		}
	}

	public void searchFriends() {
		IMService.getInstance(this).searchFriends(
				editText.getText().toString(), new CustomAsyncResponehandler() {

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						List<Doctor> list = (List<Doctor>) baseModel
								.getResultObj();
						memberList.clear();
						memberList.addAll(0, list);
						adapter.notifyDataSetChanged();
						super.onSuccess(baseModel);
					}
				});
	}

	public void back(View v) {
		finish();
	}

	public void onResume() {
		super.onResume();
	}

	public void onPause() {
		super.onPause();
	}
}
