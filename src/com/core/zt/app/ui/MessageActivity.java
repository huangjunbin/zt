package com.core.zt.app.ui;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.core.zt.R;
import com.core.zt.app.adapter.MessageAdapter;
import com.core.zt.model.ResponeModel;
import com.core.zt.model.Series;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.LessonServer;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.XListView;

import java.util.ArrayList;
import java.util.List;


public class MessageActivity extends BaseActivity implements XListView.IXListViewListener {
    private HeaderBar headerBar;
    private XListView xListView;
    private MessageAdapter adapter;
    public static List<Series> dataList;
    private int page = 1;
    private int rows = 5;
    private boolean isRefresh = true;
    private int position = 0;

    private String title;

    public MessageActivity() {
        super(R.layout.activity_message);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);

        xListView = (XListView) findViewById(R.id.messagelist);


        xListView.setXListViewListener(this);
        xListView.setFooterDividersEnabled(false);
    }

    @Override
    public void initData() {
        position = getIntent().getIntExtra("position", 0);
        dataList = new ArrayList<Series>();
        adapter = new MessageAdapter(MessageActivity.this, dataList);
        xListView.setAdapter(adapter);
        onRefresh();
        title = getIntent().getStringExtra("title");
    }

    @Override
    public void bindViews() {
        headerBar.setTitle(title);
        xListView.setPullLoadEnable(false);
        xListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Series msg = dataList.get(position - 1);
                if (msg.getTotal() > 0) {
                    Intent intent = new Intent(MessageActivity.this, MessageListActivity.class);
                    intent.putExtra("id", msg.getId());
                    intent.putExtra("title", msg.getSeriesName());
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        page = 1;
        getMessageList();
    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        getMessageList();
    }

    private void getMessageList() {
        LessonServer.getInstance(context).getSeries(new CustomAsyncResponehandler() {
            public void onFinish() {
                xListView.stopRefresh();
                xListView.stopLoadMore();
            }

            ;

            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    List<Series> lists = (List<Series>) baseModel
                            .getResultObj();
                    if (lists != null && lists.size() > 0) {
                        dataList.clear();
                        dataList.addAll(0, lists);
                        adapter.notifyDataSetChanged();
                        ;
                    }
                }
            }
        });
    }
}
