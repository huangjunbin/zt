package com.core.zt.app.ui;

import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.Lesson;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.LessonServer;
import com.core.zt.util.DateUtil;
import com.core.zt.util.UMShare;
import com.core.zt.widget.HeaderBar;

import org.w3c.dom.Text;

import java.util.Date;


public class LessonDetailActivity extends BaseActivity {
    private HeaderBar headerBar;
    private Text text;
    private Lesson lesson;
    private int type = 0;

    public LessonDetailActivity() {
        super(R.layout.activity_lesson_detail);
    }

    private TextView tvTitle, tvAuthor, tvContent, tv1, tv2, tv3, tvDate;
    private ImageView ivPhoto;
    private int sp = 15;

    @Override
    public void firstLoad() {
        super.firstLoad();
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.header);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvAuthor = (TextView) findViewById(R.id.tv_author);
        tvContent = (TextView) findViewById(R.id.tv_content);
        tv1 = (TextView) findViewById(R.id.tv_1);
        tv2 = (TextView) findViewById(R.id.tv_2);
        tv3 = (TextView) findViewById(R.id.tv_3);
        tvDate = (TextView) findViewById(R.id.tv_date);
        ivPhoto = (ImageView) findViewById(R.id.iv_photo);
        headerBar.setTitle("课程详情");
        headerBar.top_right_img.setVisibility(View.VISIBLE);
        headerBar.top_right_img.setImageResource(R.drawable.ic_know_add);
        headerBar.top_right_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sp++;
                tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, sp);
            }
        });
        headerBar.top_right_img2.setVisibility(View.VISIBLE);
        headerBar.top_right_img2.setImageResource(R.drawable.ic_know_remove);
        headerBar.top_right_img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sp--;
                tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, sp);
            }
        });
        lesson = (Lesson) getIntent().getSerializableExtra("data");
        type = getIntent().getIntExtra("type", 0);
        headerBar.top_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public void initData() {
        if(lesson.getIsUseful()==1){
            tv3.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_know_2p),null,null,null);
        }else{
            tv3.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_know_2),null,null,null);
        }
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UMShare.ShareText(LessonDetailActivity.this,lesson.getLessonContent()).openShare(LessonDetailActivity.this,
                        false);

//                LessonServer.getInstance(LessonDetailActivity.this).editLesson(lesson.getId(), 1, true,new CustomAsyncResponehandler() {
//                    public void onFinish() {
//
//                    }
//
//                    ;
//
//                    @SuppressWarnings("unchecked")
//                    @Override
//                    public void onSuccess(ResponeModel baseModel) {
//                        super.onSuccess(baseModel);
//                        if (baseModel != null && baseModel.isStatus()) {
//                            lesson.setUsefulCnt(lesson.getUsefulCnt() + 1);
//                            tv2.setText(lesson.getUsefulCnt() + "");
//                        }
//                    }
//                });
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LessonServer.getInstance(LessonDetailActivity.this).editLesson(lesson.getId(), 2,true, new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            lesson.setUsefulCnt(lesson.getUsefulCnt() + 1);
                            tv3.setText(lesson.getUsefulCnt() + "");
                            tv3.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_know_2p), null, null, null);
                        }
                    }
                });
            }
        });
        LessonServer.getInstance(this).editLesson(lesson.getId(), 0,false, new CustomAsyncResponehandler() {
            public void onFinish() {

            }

            ;

            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    lesson.setBrowseCnt(lesson.getBrowseCnt() + 1);
                    tv1.setText(lesson.getBrowseCnt() + "");
                }
            }
        });
    }

    @Override
    public void bindViews() {
        tvTitle.setText(lesson.getLessonName());
        if (type == 0) {
            tvAuthor.setText("");
        }else{
            tvAuthor.setText("作者:"+lesson.getAuthor());
        }
        Date date= DateUtil.parseDate(lesson.getIssueDate(),"MM-dd HH:mm");
        tvDate.setText(lesson.getCreateUserName() + "    " + DateUtil.getDate2(date));
        tvContent.setText(lesson.getLessonContent());
        AppContext.setImage(lesson.getFilePath() + lesson.getFileName(), ivPhoto, AppContext.imageOption);
        tv1.setText(lesson.getBrowseCnt() + "");
        tv2.setText(lesson.getAcceptCnt() + "");
        tv3.setText(lesson.getUsefulCnt() + "");
    }

}
