
package com.core.zt.app.ui;

import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.util.DisplayUtils;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.MyMarkerView;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BloodSugarChartActivity extends BaseActivity implements
        OnChartGestureListener, OnChartValueSelectedListener {

    private LineChart mChart;
    private Button btn1, btn2, btn3, btn4, btn5, btn6;
    private List<Button> btnList = new ArrayList<>();
    private HeaderBar bar;

    public BloodSugarChartActivity() {
        super(R.layout.activity_bloodsugar_chart);
    }

    private TextView tvLow, tvNormal, tvHeight;
    private LinearLayout lyMore;
    private ImageView ivType;
    private TextView tvType;
    private int mindex=0;
    private int index=1;
    @Override
    public void initViews() {
        mChart = (LineChart) findViewById(R.id.chart1);
        bar = (HeaderBar) findViewById(R.id.header);
        tvLow = (TextView) findViewById(R.id.tvLow);
        tvNormal = (TextView) findViewById(R.id.tvNormal);
        tvHeight = (TextView) findViewById(R.id.tvHight);
        btn1 = (Button) findViewById(R.id.btn_1);
        btn2 = (Button) findViewById(R.id.btn_2);
        btn3 = (Button) findViewById(R.id.btn_3);
        btn4 = (Button) findViewById(R.id.btn_4);
        btn5 = (Button) findViewById(R.id.btn_5);
        btn6 = (Button) findViewById(R.id.btn_6);
        ivType= (ImageView) findViewById(R.id.iv_type);
        tvType= (TextView) findViewById(R.id.tv_type);
        lyMore = (LinearLayout) findViewById(R.id.ll_more);
        btnList.add(btn1);
        btnList.add(btn2);
        btnList.add(btn3);
        btnList.add(btn4);
        btnList.add(btn5);
        btnList.add(btn6);
        for (int i = 0; i < btnList.size(); i++) {
            btnList.get(i).setTag(i + 1);
            btnList.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    index=(int) view.getTag();
                    change((int) view.getTag());
                }
            });
        }
        change(1);
        tvType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDropWindow(view);
            }
        });
    }
    private PopupWindow popupWindow;

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mindex=Integer.parseInt(view.getTag().toString());
            popupWindow.dismiss();
            TextView tv= (TextView) view;
            tvType.setText(tv.getText().toString());
            getRecord(1, index,mindex);
        }
    };
    private void showDropWindow(View position) {
        ivType.setImageDrawable(getResources().getDrawable(R.drawable.icon_sugar_up));
        bar.top_title.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.icon_up), null);
        View popBar = getLayoutInflater().inflate(R.layout.pop_sugar,
                null);
        TextView btnR1, btnR2, btnR3, btnR4, btnR5,btnR6,btnR7,btnR8;
        btnR1 = (TextView) popBar.findViewById(R.id.tv_1);
        btnR1.setTag(0);
        btnR1.setOnClickListener(clickListener);
        btnR2 = (TextView) popBar.findViewById(R.id.tv_2);
        btnR2.setTag(1);
        btnR2.setOnClickListener(clickListener);
        btnR3 = (TextView) popBar.findViewById(R.id.tv_3);
        btnR3.setTag(3);
        btnR3.setOnClickListener(clickListener);
        btnR4 = (TextView) popBar.findViewById(R.id.tv_4);
        btnR4.setTag(4);
        btnR4.setOnClickListener(clickListener);
        btnR5 = (TextView) popBar.findViewById(R.id.tv_5);
        btnR5.setTag(6);
        btnR5.setOnClickListener(clickListener);
        btnR6 = (TextView) popBar.findViewById(R.id.tv_6);
        btnR6.setTag(7);
        btnR6.setOnClickListener(clickListener);
        btnR7 = (TextView) popBar.findViewById(R.id.tv_7);
        btnR7.setTag(5);
        btnR7.setOnClickListener(clickListener);
        btnR8 = (TextView) popBar.findViewById(R.id.tv_8);
        btnR8.setTag(2);
        btnR8.setOnClickListener(clickListener);

        popupWindow = new PopupWindow();
        // 设置弹框的宽度为布局文件的宽
        popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
        // popupWindow.setHeight(DisplayUtils.dip2px(context, 300));
        // 设置一个透明的背景，不然无法实现点击弹框外，弹框消失
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // popupWindow.setBackgroundDrawable(getResources().getDrawable(
        // R.drawable.pupback));
        // 设置点击弹框外部，弹框消失
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setContentView(popBar);
        // 设置弹框出现的位置，在v的正下方横轴偏移textview的宽度，为了对齐~纵轴不偏移
        popupWindow.showAsDropDown(position, 0,
                DisplayUtils.dip2px(this, 11));
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                ivType.setImageDrawable(getResources().getDrawable(R.drawable.icon_sugar_down));
            }

        });
    }
    @Override
    public void initData() {

        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);
        mChart.setBackgroundColor(Color.WHITE);
        // no description text
        mChart.setDescription("");
        mChart.setNoDataTextDescription("You need to provide data for the chart");

        // enable value highlighting
        mChart.setHighlightEnabled(true);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setScaleXEnabled(true);
        mChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        // mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);

        // set the marker to the chart
        mChart.setMarkerView(mv);

        // enable/disable highlight indicators (the lines that indicate the
        // highlighted Entry)
        mChart.setHighlightIndicatorEnabled(false);

        // x-axis limit line
//        LimitLine llXAxis = new LimitLine(10f, "Index 10");
//        llXAxis.setLineWidth(4f);
//        llXAxis.enableDashedLine(10f, 10f, 0f);
//        llXAxis.setLabelPosition(LimitLabelPosition.POS_RIGHT);
//        llXAxis.setTextSize(10f);
//
//        XAxis xAxis = mChart.getXAxis();
//        xAxis.addLimitLine(llXAxis);

        LimitLine ll1 = new LimitLine(10f, "正常最大值");
        ll1.setLineWidth(1f);
        ll1.enableDashedLine(0f, 0f, 0f);
        ll1.setLabelPosition(LimitLabelPosition.POS_RIGHT);
        ll1.setTextSize(10f);
        ll1.setLineColor(Color.GREEN);
        LimitLine ll2 = new LimitLine(4.4f, "正常最小值");
        ll2.setLineWidth(1f);
        ll2.enableDashedLine(0f, 0f, 0f);
        ll2.setLabelPosition(LimitLabelPosition.POS_RIGHT);
        ll2.setTextSize(10f);
        ll2.setLineColor(Color.BLUE);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.addLimitLine(ll1);
        leftAxis.addLimitLine(ll2);
        leftAxis.setAxisMaxValue(20f);
        leftAxis.setAxisMinValue(0f);
        leftAxis.setStartAtZero(false);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        mChart.getAxisRight().setEnabled(false);
    }

    @Override
    public void bindViews() {

        bar.setTitle("血糖");
        // add data

//        mChart.setVisibleXRange(20);
//        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mChart.centerViewTo(20, 50, AxisDependency.LEFT);
        mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
//        mChart.invalidate();
        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();
        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(LegendForm.LINE);
        // // dont forget to refresh the drawing
        // mChart.invalidate();

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    private void setData(ArrayList<String> xVals, ArrayList<Entry> yVals) {

        mChart.clear();
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "血糖");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        set1.enableDashedLine(10f, 5f, 0f);
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);

        set1.setValueTextSize(9f);
        set1.setFillAlpha(65);
        set1.setFillColor(Color.BLACK);
//        set1.setDrawFilled(true);
        // set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(),
        // Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);

        // set data
        mChart.setData(data);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("", "low: " + mChart.getLowestVisibleXIndex() + ", high: " + mChart.getHighestVisibleXIndex());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    public void change(int index) {
        getRecord(1, index,mindex);
        for (int i = 0; i < btnList.size(); i++) {

            if (i + 1 == index) {
                btnList.get(i).setTextColor(getResColor(R.color.white));
                btnList.get(i).setBackgroundResource(R.drawable.btn_cheeck);
            } else {
                btnList.get(i).setTextColor(getResColor(R.color.main_bg));
                btnList.get(i).setBackgroundResource(R.drawable.btn_no_check);
            }
        }
    }


    private HashMap<String, List<Record>> map = new HashMap<>();

    private void getRecord(int logType, int day,int index) {
        RecordServer.getInstance(BloodSugarChartActivity.this).getChatRecord2(AppContext.currentUser.getUserId(),
                logType + "", day + "",index+"", new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Record> lists = (List<Record>) baseModel
                                    .getResultObj();
                            tvLow.setText(baseModel.getLow() + "\n偏低");
                            tvNormal.setText(baseModel.getNormal() + "\n正常");
                            tvHeight.setText(baseModel.getHigh() + "\n偏高");
                            map.clear();
                            ;
                            if (lists != null && lists.size() > 0) {
                                ArrayList<String> xVals = new ArrayList<String>();
                                ArrayList<Entry> yVals = new ArrayList<Entry>();
                                lyMore.removeAllViews();
                                for (int i = 0; i < lists.size(); i++) {
                                    Date date=DateUtil.parseDate(lists.get(i).getLogTime(),"yyyy-MM-dd HH:mm:ss");
                                    String key = DateUtil.getDate(date);
                                    Record item = lists.get(i);
                                    xVals.add(item.getDate());
                                    yVals.add(new Entry(Float.parseFloat(item.getLogVal1()), i));
                                    if (map.containsKey(key)) {
                                        List<Record> vls = map.get(key);
                                        vls.add(lists.get(i));
                                    } else {
                                        List<Record> vls = new ArrayList<Record>();
                                        vls.add(lists.get(i));
                                        map.put(key, vls);
                                    }
                                }
                                Iterator iter = map.entrySet().iterator();
                                while (iter.hasNext()) {
                                    Map.Entry entry = (Map.Entry) iter.next();
                                    List<Record> vls = (List<Record>) entry.getValue();
                                    View v = getLayoutInflater().inflate(R.layout.layout_bloodsugar_chart_value, null);
                                    TextView tv1 = (TextView) v.findViewById(R.id.tv_1);
                                    TextView tv2 = (TextView) v.findViewById(R.id.tv_2);
                                    TextView tv3 = (TextView) v.findViewById(R.id.tv_3);
                                    TextView tv4 = (TextView) v.findViewById(R.id.tv_4);
                                    TextView tv5 = (TextView) v.findViewById(R.id.tv_5);
                                    TextView tv6 = (TextView) v.findViewById(R.id.tv_6);
                                    TextView tv7 = (TextView) v.findViewById(R.id.tv_7);
                                    TextView tv8 = (TextView) v.findViewById(R.id.tv_8);
                                    TextView tv9 = (TextView) v.findViewById(R.id.tv_9);
                                    for (int i = 0; i < vls.size(); i++) {

                                        Record item = vls.get(i);
                                        System.out.println("item:"+item);

                                        tv1.setText(item.getMD());

                                        if ("0".equals(item.getLogVal2())) {
                                            tv2.setText(item.getLogVal1());
                                        } else {
                                            //tv2.setText("");
                                        }

                                        if ("1".equals(item.getLogVal2())) {
                                            tv3.setText(item.getLogVal1());
                                        } else {
                                          //  tv3.setText("");
                                        }

                                        if ("3".equals(item.getLogVal2())) {
                                            tv4.setText(item.getLogVal1());
                                        } else {
                                           // tv4.setText("");
                                        }

                                        tv5.setVisibility(View.VISIBLE);
                                        if ("4".equals(item.getLogVal2())) {
                                            tv5.setText(item.getLogVal1());
                                        } else {
                                            //tv5.setText("");
                                        }

                                        tv6.setVisibility(View.VISIBLE);
                                        if ("6".equals(item.getLogVal2())) {
                                            tv6.setText(item.getLogVal1());
                                        } else {
                                          //  tv6.setText("");
                                        }

                                        tv7.setVisibility(View.VISIBLE);
                                        if ("7".equals(item.getLogVal2())) {
                                            tv7.setText(item.getLogVal1());
                                        } else {
                                           // tv7.setText("");
                                        }

                                        tv8.setVisibility(View.VISIBLE);
                                        if ("5".equals(item.getLogVal2())) {
                                            tv8.setText(item.getLogVal1());
                                        } else {
                                          //  tv8.setText("");
                                        }

                                        tv9.setVisibility(View.VISIBLE);
                                        if ("2".equals(item.getLogVal2())) {
                                            tv9.setText(item.getLogVal1());
                                        } else {
                                          //  tv9.setText("");
                                        }

                                    }
                                    lyMore.addView(v);
                                }

                                setData(xVals, yVals);
                            }else{
                                lyMore.removeAllViews();
                                setData(new ArrayList<String>(),new ArrayList<Entry>());
                            }
                        }
                    }
                });
    }
}
