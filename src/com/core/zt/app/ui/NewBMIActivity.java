package com.core.zt.app.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.core.zt.R;
import com.core.zt.app.AppManager;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.TuneWheel;

import java.util.Calendar;
import java.util.Date;

public class NewBMIActivity extends BaseActivity {

    private HeaderBar bar;
    private RelativeLayout rlWeek;
    private TextView tvTime;
    private Button btnSave;
    private Record record;
    private EditText etH, etL;
    private Record data;
    private TuneWheel tw;
    private TuneWheel tw1;
    public NewBMIActivity() {
        super(R.layout.activity_new_bmi);

    }

    @Override
    public void initViews() {
        tw= (TuneWheel) findViewById(R.id.tw_value);
        tw1= (TuneWheel) findViewById(R.id.tw_value1);
        data = (Record) getIntent().getSerializableExtra("data");
        bar = (HeaderBar) findViewById(R.id.header);
        rlWeek = (RelativeLayout) findViewById(R.id.rl_week);
        tvTime = (TextView) findViewById(R.id.tv_time);
        etH = (EditText) findViewById(R.id.et_h);
        etL = (EditText) findViewById(R.id.et_l);
        btnSave = (Button) findViewById(R.id.btn_save);
    }

    @Override
    public void initData() {
        record = new Record();
        tw.setValueChangeListener(new TuneWheel.OnValueChangeListener() {
            @Override
            public void onValueChange(float value) {
                etH.setText(value/10+"");
            }
        });
        tw1.setValueChangeListener(new TuneWheel.OnValueChangeListener() {
            @Override
            public void onValueChange(float value) {
                etL.setText(value/10+"");
            }
        });
        if (data != null) {
            record.setId(data.getId());
            Date date= DateUtil.parseDate(data.getLogTime(), "yyyy-MM-dd HH:mm");
            tvTime.setText(DateUtil.getDateNoSecond(date));
            etH.setText(data.getLogVal3());
            etL.setText(data.getLogVal2());
            bar.setTitle("编辑体重指数记录");
            tw.initViewParam((int) (Float.parseFloat(data.getLogVal3()) * 10),2500, TuneWheel.MOD_TYPE_ONE);
            tw1.initViewParam((int)(Float.parseFloat(data.getLogVal2())*10),2000,TuneWheel.MOD_TYPE_ONE);
        }else{
            bar.setTitle("新增体重指数记录");
            tw1.initViewParam(1650, 2500, TuneWheel.MOD_TYPE_ONE);
            tw.initViewParam(600,2000,TuneWheel.MOD_TYPE_ONE);
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            StringBuffer sb = new StringBuffer();
            sb.append(String.format("%d-%02d-%02d",
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH) + 1,
                    cal.get(Calendar.DAY_OF_MONTH)));
            sb.append("  ");
            sb.append(DateUtil.getZero(cal.get(Calendar.HOUR_OF_DAY)))
                    .append(":")
                    .append(DateUtil.getZero(cal.get(Calendar.MINUTE)));
            tvTime.setText(sb);
        }
    }

    @Override
    public void bindViews() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(etH.getText().toString())) {
                    showToast("请输入身高");
                }
                if ("".equals(etL.getText().toString())) {
                    showToast("请输入体重");
                }
                if ("".equals(tvTime.getText().toString())) {
                    showToast("请选择时间");
                }

                record.setLogTime(tvTime.getText().toString());
                record.setLogVal2(etL.getText().toString());
                record.setLogVal3(etH.getText().toString());
                record.setLogType("3");
                RecordServer.getInstance(NewBMIActivity.this).saveRecord(
                        record, new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            ;

                            @SuppressWarnings("unchecked")
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {

                                    if (data != null) {
                                        showToast("编辑成功");
                                        AppManager.getAppManager().finishActivity(BMIDetailActivity.class);
                                        NewBMIActivity.this.finish();
//                                        Intent intent=new Intent();
//                                        intent.putExtra("data", record);
//                                        intent.setClass(NewBMIActivity.this, BMIDetailActivity.class);
//                                        startActivity(intent);
                                    }else{
                                        showToast("添加成功");
                                    }
                                    NewBMIActivity.this.finish();
                                }
                            }
                        });
            }
        });
        rlWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        NewBMIActivity.this);
                View v = getLayoutInflater().inflate(
                        R.layout.date_time_dialog, null);
                final DatePicker datePicker = (DatePicker) v
                        .findViewById(R.id.date_picker);
                final TimePicker timePicker = (android.widget.TimePicker) v
                        .findViewById(R.id.time_picker);
                timePicker.setVisibility(View.VISIBLE);
                datePicker.setVisibility(View.VISIBLE);
                builder.setView(v);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                datePicker.init(cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH), null);

                timePicker.setIs24HourView(true);
                timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
                builder.setTitle("选取起始时间");
                builder.setPositiveButton("确  定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                StringBuffer sb = new StringBuffer();
                                sb.append(String.format("%d-%02d-%02d",
                                        datePicker.getYear(),
                                        datePicker.getMonth() + 1,
                                        datePicker.getDayOfMonth()));
                                sb.append("  ");
                                sb.append(DateUtil.getZero(timePicker.getCurrentHour()))
                                        .append(":")
                                        .append(DateUtil.getZero(timePicker.getCurrentMinute()));
                                tvTime.setText(sb);
                                dialog.cancel();
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });
    }


}
