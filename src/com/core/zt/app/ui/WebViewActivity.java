package com.core.zt.app.ui;

import android.graphics.Bitmap;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.core.zt.R;
import com.core.zt.widget.HeaderBar;


public class WebViewActivity extends BaseActivity {

    private HeaderBar bar;

    public WebViewActivity() {
        super(R.layout.activity_webview);
    }

    /**
     * 控件
     * *
     */
    public static WebView webview; // 主网页加载控件

    /**
     * 数据
     */
    public boolean isRefresh = false; // 判断webview当前是否属于刷新

    private String url = "";

    private String str = "";
    private String title = "";
    private String fkId = "";
    /**
     * article 为新闻 job 为工作 company 为公司
     */
    private String type = "article";
    private String isFavorite = "0";


    private void InitControl() {

        webview = (WebView) findViewById(R.id.webView);
        bar = (HeaderBar) findViewById(R.id.title);

    }


    private void InitData() {
        initWebView();

    }


    private void BindControl() {

    }

    private void initWebView() {
        WebSettings settings = webview.getSettings();
        // 设置WebView属性，能够执行Javascript脚本
        settings.setJavaScriptEnabled(true);
        /*
         * // 设置WebView不支持缩放 settings.setBuiltInZoomControls(false); //
		 * 设置WebView允许缓存 settings.setAppCacheEnabled(true); String cache_dir =
		 * this.getApplicationContext() .getDir("cache",
		 * Context.MODE_PRIVATE).getPath();
		 * settings.setAppCachePath(cache_dir);// 设置应用缓存的路径 // 设置缓存的模式
		 * 如果内容已经存在cache 则使用cache，即使是过去的历史记录。如果cache中不存在，从网络中获取
		 * settings.setCacheMode(WebSettings.LOAD_DEFAULT);
		 * settings.setAppCacheMaxSize(1024 * 1024 * 8);// 设置应用缓存的最大尺寸
		 * settings.setAllowFileAccess(true);// 可以读取文件缓存(manifest生效)
		 * settings.setRenderPriority(RenderPriority.HIGH);
		 * settings.setBlockNetworkLoads(true);
		 * settings.setJavaScriptCanOpenWindowsAutomatically(true);
		 */
        settings.setBuiltInZoomControls(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setSavePassword(true);
        settings.setSaveFormData(true);
        settings.setJavaScriptEnabled(true);
        settings.setGeolocationEnabled(true);
        settings.setGeolocationDatabasePath("/data/data/com.core.zt/databases/");
        settings.setDomStorageEnabled(true);
        webview.requestFocus();
        // set.setUserAgentString(SmartConfig.getWebUserAgent(mContext));

        /**
         * 使用户跳转到系统网络连接界面
         * */

        webview.setWebViewClient(new WebViewClientDemo());
        if (url != null && url.length() > 0) {
            webview.loadUrl(url);
        }
        if (str != null && str.length() > 0) {
            try {

                str = str.replaceAll("&amp;", "");
                str = str.replaceAll("quot;", "\"");
                str = str.replaceAll("lt;", "<");
                str = str.replaceAll("gt;", ">");
                webview.loadDataWithBaseURL(null, str, "text/html", "utf-8",
                        null);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        bar.setTitle(title);
    }

    /**
     * WebViewClientDemo类 主要帮助WebView处理各种通知、请求事件
     *
     * @author 黃俊彬
     * @version 1.0
     * @created 2013-8-7
     */
    private class WebViewClientDemo extends WebViewClient {
        @Override
        // 在WebView中而不是默认浏览器中显示页面
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            isRefresh = false;
            view.getSettings().setBlockNetworkImage(false);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            view.stopLoading();
            view.clearView();

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            isRefresh = true;
            view.getSettings().setBlockNetworkImage(true);
        }

    }

    public void initViews() {
        InitControl();
    }

    public void initData() {
        url = getIntent().getStringExtra("url");

        str=getIntent().getStringExtra("str");
        title = getIntent().getStringExtra("title");
        InitData();
    }

    public void bindViews() {
        BindControl();
    }


}
