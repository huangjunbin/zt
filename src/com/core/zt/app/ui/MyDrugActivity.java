package com.core.zt.app.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.DrugChildItem;
import com.core.zt.model.DrugItem;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.DrugServer;
import com.core.zt.widget.HeaderBar;

import java.util.List;

public class MyDrugActivity extends BaseActivity {

    private HeaderBar bar;

    private LinearLayout llDrugAdd, llDrugMore;
    private Button btnDelete;

    public MyDrugActivity() {
        super(R.layout.activity_my_drug);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);

        llDrugAdd = (LinearLayout) findViewById(R.id.ll_drud_add);
        llDrugMore = (LinearLayout) findViewById(R.id.ll_more);
        btnDelete = (Button) findViewById(R.id.btn_delete);

    }

    @Override
    public void initData() {

        llDrugMore.removeAllViews();

    }

    @Override
    public void bindViews() {
        bar.setTitle("用药方案");
        llDrugAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyDrugActivity.this, NewDrugActivity.class));
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSureDialog(MyDrugActivity.this, "确定删除当前用药方案?", "确定", new OnSurePress() {
                    @Override
                    public void onClick(View view) {
                        DrugServer.getInstance(MyDrugActivity.this).deleteMedicationPlan(
                                AppContext.currentUser.getUserId(), new CustomAsyncResponehandler() {
                                    public void onFinish() {

                                    }

                                    ;

                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {
                                            showToast("删除成功");
                                            llDrugMore.removeAllViews();
                                        }
                                    }
                                });
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {

                    }
                });

            }
        });
    }

    @Override
    protected void onResume() {
        DrugServer.getInstance(context).getMedicationPlan(AppContext.currentUser.getUserId(), new CustomAsyncResponehandler() {
            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    List<DrugItem> lists = (List<DrugItem>) baseModel
                            .getResultObj();
                    if (lists != null && lists.size() > 0) {
                        btnDelete.setVisibility(View.VISIBLE);
                        llDrugMore.removeAllViews();
                        for (int i = 0; i < lists.size(); i++) {
                            final DrugItem item = lists.get(i);
                            for (int j = 0; j < lists.get(i).getChild().size(); j++) {
                                DrugChildItem childItem = lists.get(i).getChild().get(j);
                                final View child = getLayoutInflater().inflate(R.layout.layout_my_drug, null);
                                TextView tv1 = (TextView) child.findViewById(R.id.tv_1);
                                TextView tv2 = (TextView) child.findViewById(R.id.tv_2);
                                TextView tv3 = (TextView) child.findViewById(R.id.tv_3);
                                if (j == 0) {
                                    tv1.setText(item.getDrugChemicalCategoryCnName() + "/" + item.getDrud_cn_name());
                                    tv2.setText(childItem.getDose() + item.getUnit());
                                    tv3.setText(childItem.getMedicationTime());
                                    child.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(MyDrugActivity.this, NewDrugActivity.class);
                                            intent.putExtra("data", item);
                                            intent.putExtra("tag", 1);
                                            startActivity(intent);
                                        }
                                    });
                                } else {
                                    tv2.setText(childItem.getDose() + item.getUnit());
                                    tv3.setText(childItem.getMedicationTime());
                                }
                                llDrugMore.addView(child);
                            }
                        }
                    } else {
                        llDrugMore.removeAllViews();
                        btnDelete.setVisibility(View.GONE);
                    }
                }
            }
        });
        super.onPostResume();
    }
}
