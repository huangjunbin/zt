package com.core.zt.app.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.core.zt.R;
import com.core.zt.app.AppManager;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.TuneWheel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewBloodSugarActivity extends BaseActivity {

    private HeaderBar bar;
    private Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;
    private List<Button> btnList = new ArrayList<>();
    private RelativeLayout rlWeek;
    private TextView tvTime;
    private Record record;
    private Button btnSave;
    private EditText etValue;
    private int index = 0;
    private Record data;
    private TuneWheel tw;

    public NewBloodSugarActivity() {
        super(R.layout.activity_new_bloodsugar);

    }

    @Override
    public void initViews() {
        tw = (TuneWheel) findViewById(R.id.tw_value);
        data = (Record) getIntent().getSerializableExtra("data");
        etValue = (EditText) findViewById(R.id.et_Value);
        bar = (HeaderBar) findViewById(R.id.header);
        btn1 = (Button) findViewById(R.id.btn_1);
        btn2 = (Button) findViewById(R.id.btn_2);
        btn3 = (Button) findViewById(R.id.btn_3);
        btn4 = (Button) findViewById(R.id.btn_4);
        btn5 = (Button) findViewById(R.id.btn_5);
        btn6 = (Button) findViewById(R.id.btn_6);
        btn7 = (Button) findViewById(R.id.btn_7);
        btn8 = (Button) findViewById(R.id.btn_8);
        btn9 = (Button) findViewById(R.id.btn_9);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnList.add(btn1);
        btnList.add(btn2);
        btnList.add(btn3);
        btnList.add(btn4);
        btnList.add(btn5);
        btnList.add(btn6);
        btnList.add(btn7);
        btnList.add(btn8);
        btnList.add(btn9);
        for (int i = 0; i < btnList.size(); i++) {
            btnList.get(i).setTag(i);
            btnList.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    change((int) view.getTag());
                }
            });
        }
        if (data != null) {
            change(Integer.parseInt(data.getLogVal2()));
        } else {
            change(0);
        }
        rlWeek = (RelativeLayout) findViewById(R.id.rl_week);
        tvTime = (TextView) findViewById(R.id.tv_time);
    }

    @Override
    public void initData() {
    }

    @Override
    public void bindViews() {


        tw.setValueChangeListener(new TuneWheel.OnValueChangeListener() {
            @Override
            public void onValueChange(float value) {
                etValue.setText(value / 10 + "");
            }
        });
        record = new Record();
        if (data != null) {
            Date date = DateUtil.parseDate(data.getLogTime(), "yyyy-MM-dd HH:mm");
            tvTime.setText(DateUtil.getDateNoSecond(date));
            etValue.setText(data.getLogVal1());
            record.setId(data.getId());
            bar.setTitle("编辑血糖记录");
            tw.initViewParam((int) (Float.parseFloat(data.getLogVal1()) * 10), 330, TuneWheel.MOD_TYPE_ONE);
        } else {
            tw.initViewParam(60, 300, TuneWheel.MOD_TYPE_ONE);
            bar.setTitle("新增血糖记录");
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            StringBuffer sb = new StringBuffer();
            sb.append(String.format("%d-%02d-%02d",
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH) + 1,
                    cal.get(Calendar.DAY_OF_MONTH)));
            sb.append("  ");
            sb.append(DateUtil.getZero(cal.get(Calendar.HOUR_OF_DAY)))
                    .append(":")
                    .append(DateUtil.getZero(cal.get(Calendar.MINUTE)));
            tvTime.setText(sb);
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(etValue.getText().toString())) {
                    showToast("请输入数值");
                }
                if ("".equals(tvTime.getText().toString())) {
                    showToast("请选择时间");
                }

                record.setLogTime(tvTime.getText().toString());
                record.setLogVal1(etValue.getText().toString());
                record.setLogVal2(index + "");
                record.setLogType("1");
                RecordServer.getInstance(NewBloodSugarActivity.this).saveRecord(
                        record, new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            ;

                            @SuppressWarnings("unchecked")
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    if (data != null) {
                                        showToast("编辑成功");
                                        AppManager.getAppManager().finishActivity(BloodSugarDetailActivity.class);
                                        NewBloodSugarActivity.this.finish();
//                                        Intent intent=new Intent();
//                                        intent.putExtra("data", record);
//                                        intent.setClass(NewBloodSugarActivity.this, BloodSugarDetailActivity.class);
//                                        startActivity(intent);
                                    } else {
                                        showToast("添加成功");
                                    }
                                    NewBloodSugarActivity.this.finish();
                                }
                            }
                        });
            }
        });
        rlWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        NewBloodSugarActivity.this);
                View v = getLayoutInflater().inflate(
                        R.layout.date_time_dialog, null);
                final DatePicker datePicker = (DatePicker) v
                        .findViewById(R.id.date_picker);
                final TimePicker timePicker = (android.widget.TimePicker) v
                        .findViewById(R.id.time_picker);
                timePicker.setVisibility(View.VISIBLE);
                datePicker.setVisibility(View.VISIBLE);
                builder.setView(v);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                datePicker.init(cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH), null);

                timePicker.setIs24HourView(true);
                timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
                builder.setTitle("选取起始时间");
                builder.setPositiveButton("确  定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                StringBuffer sb = new StringBuffer();
                                sb.append(String.format("%d-%02d-%02d",
                                        datePicker.getYear(),
                                        datePicker.getMonth() + 1,
                                        datePicker.getDayOfMonth()));
                                sb.append("  ");
                                sb.append(DateUtil.getZero(timePicker.getCurrentHour()))
                                        .append(":")
                                        .append(DateUtil.getZero(timePicker.getCurrentMinute()));
                                tvTime.setText(sb);
                                dialog.cancel();
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });
    }


    public void change(int index) {
        this.index = index;
        for (int i = 0; i < btnList.size(); i++) {
            if (i == 8) {
                if (i == index) {
                    btnList.get(i).setTextColor(getResColor(R.color.white));
                    btnList.get(i).setBackgroundResource(R.drawable.btn_bg_red_check);
                } else {
                    btnList.get(i).setTextColor(getResColor(R.color.sugar_red));
                    btnList.get(i).setBackgroundResource(R.drawable.btn_bg_red_no);
                }
            } else {
                if (i == index) {
                    btnList.get(i).setTextColor(getResColor(R.color.white));
                    btnList.get(i).setBackgroundResource(R.drawable.btn_cheeck);
                } else {
                    btnList.get(i).setTextColor(getResColor(R.color.main_bg));
                    btnList.get(i).setBackgroundResource(R.drawable.btn_no_check);
                }
            }
        }
    }

}
