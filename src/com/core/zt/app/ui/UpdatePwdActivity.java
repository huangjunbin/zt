package com.core.zt.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.UserServer;
import com.core.zt.util.StringUtils;
import com.core.zt.widget.HeaderBar;


public class UpdatePwdActivity extends BaseActivity {

    private HeaderBar headerBar;
    private EditText etOldPwd, etpwd, etRePwd;
    private Button btnUpdate;
    private String newPwd = "";
    private String mobile = "";
    public UpdatePwdActivity() {
        super(R.layout.activity_updatepwd);

    }

    @Override
    public void initViews() {

        headerBar = (HeaderBar) findViewById(R.id.header);
        etOldPwd = (EditText) findViewById(R.id.et_old_pwd);
        etpwd = (EditText) findViewById(R.id.et_pwd);
        etRePwd = (EditText) findViewById(R.id.et_repwd);
        btnUpdate = (Button) findViewById(R.id.btn_forget);
    }

    @Override
    public void initData() {

    }

    @Override
    public void bindViews() {
        newPwd = getIntent().getStringExtra("newPwd");
        mobile = getIntent().getStringExtra("mobile");
        if (newPwd == null || "".equals(newPwd)) {
            headerBar.setTitle("修改密码");
        } else {
            headerBar.setTitle("新密码");
            etOldPwd.setText(newPwd);
            findViewById(R.id.ll_pwd).setVisibility(View.GONE);
            findViewById(R.id.ll_line).setVisibility(View.GONE);
        }

        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (StringUtils.isEmpty(etOldPwd.getText().toString())) {
                    showToast("旧密码不能为空");
                    return;
                }
                if (StringUtils.isEmpty(etpwd.getText().toString())) {
                    showToast("密码不能为空");
                    return;
                }
                if (!etRePwd.getText().toString()
                        .equals((etpwd.getText().toString()))) {
                    showToast("两次密码不一致");
                    return;
                }
                UserServer.getInstance(UpdatePwdActivity.this).updatePwd(AppContext.currentUser==null?mobile:AppContext.currentUser.getUserId(), etOldPwd.getText().toString(), etpwd.getText().toString(), new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            showToast("操作成功");
                            UpdatePwdActivity.this.finish();
                        }
                    }
                });
            }
        });

    }

}
