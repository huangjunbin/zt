
package com.core.zt.app.ui;

import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.MyMarkerView;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;

import java.util.ArrayList;
import java.util.List;

public class HbaChartActivity extends BaseActivity implements
        OnChartGestureListener, OnChartValueSelectedListener {

    private LineChart mChart;
    private Button btn1, btn2, btn3, btn4, btn5, btn6;
    private List<Button> btnList = new ArrayList<>();
    private HeaderBar bar;
    public HbaChartActivity() {
        super(R.layout.activity_hba_chart);
    }
    private TextView tvLow,tvNormal,tvHeight;
    private LinearLayout lyMore;
    @Override
    public void initViews() {
        mChart = (LineChart) findViewById(R.id.chart1);
        bar= (HeaderBar) findViewById(R.id.header);
        btn1 = (Button) findViewById(R.id.btn_1);
        btn2 = (Button) findViewById(R.id.btn_2);
        btn3 = (Button) findViewById(R.id.btn_3);
        btn4 = (Button) findViewById(R.id.btn_4);
        btn5 = (Button) findViewById(R.id.btn_5);
        btn6 = (Button) findViewById(R.id.btn_6);
        lyMore= (LinearLayout) findViewById(R.id.ll_more);
        btnList.add(btn1);
        btnList.add(btn2);
        btnList.add(btn3);
        btnList.add(btn4);
        btnList.add(btn5);
        btnList.add(btn6);
        tvLow= (TextView) findViewById(R.id.tvLow);
        tvNormal= (TextView) findViewById(R.id.tvNormal);
        tvHeight= (TextView) findViewById(R.id.tvHight);
        for (int i = 0; i < btnList.size(); i++) {
            btnList.get(i).setTag(i+1);
            btnList.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    change((int) view.getTag());
                }
            });
        }
        change(1);
    }

    @Override
    public void initData() {

        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);
        mChart.setBackgroundColor(Color.WHITE);
        // no description text
        mChart.setDescription("");
        mChart.setNoDataTextDescription("You need to provide data for the chart");

        // enable value highlighting
        mChart.setHighlightEnabled(true);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setScaleXEnabled(true);
        mChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        // mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);

        // set the marker to the chart
        mChart.setMarkerView(mv);

        // enable/disable highlight indicators (the lines that indicate the
        // highlighted Entry)
        mChart.setHighlightIndicatorEnabled(false);

        // x-axis limit line
//        LimitLine llXAxis = new LimitLine(10f, "Index 10");
//        llXAxis.setLineWidth(4f);
//        llXAxis.enableDashedLine(10f, 10f, 0f);
//        llXAxis.setLabelPosition(LimitLabelPosition.POS_RIGHT);
//        llXAxis.setTextSize(10f);
//
//        XAxis xAxis = mChart.getXAxis();
//        xAxis.addLimitLine(llXAxis);


        LimitLine ll2 = new LimitLine(20f, "正常HbA1c最大值");
        ll2.setLineWidth(1f);
        ll2.enableDashedLine(0f, 0f, 0f);
        ll2.setLabelPosition(LimitLabelPosition.POS_RIGHT);
        ll2.setTextSize(10f);
        ll2.setLineColor(getResColor(R.color.green));

        LimitLine ll3 = new LimitLine(3f, "正常HbA1c最低值");
        ll3.setLineWidth(1f);
        ll3.enableDashedLine(0f, 0f, 0f);
        ll3.setLabelPosition(LimitLabelPosition.POS_RIGHT);
        ll3.setTextSize(10f);
        ll3.setLineColor(getResColor(R.color.blue));
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.addLimitLine(ll2);
        leftAxis.addLimitLine(ll3);
        leftAxis.setAxisMaxValue(40f);
        leftAxis.setAxisMinValue(0f);
        leftAxis.setStartAtZero(false);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        mChart.getAxisRight().setEnabled(false);
    }

    @Override
    public void bindViews() {

        bar.setTitle("HbA1c");
        // add data
//        mChart.setVisibleXRange(20);
//        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mChart.centerViewTo(20, 50, AxisDependency.LEFT);
        mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
//        mChart.invalidate();
        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();
        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(LegendForm.LINE);
        // // dont forget to refresh the drawing
        // mChart.invalidate();

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    private void setData(  ArrayList<String> xVals,  ArrayList<Entry> yVals) {

        mChart.clear();
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "HbA1c");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        set1.enableDashedLine(10f, 5f, 0f);
        set1.setColor(getResColor(R.color.orange));
        set1.setCircleColor(getResColor(R.color.orange));
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);

        set1.setValueTextSize(9f);
        set1.setFillAlpha(65);
        set1.setFillColor(Color.BLACK);
//        set1.setDrawFilled(true);
        // set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(),
        // Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets
        LineData data = new LineData(xVals, dataSets);
        mChart.setData(data);

    }
    
    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

	@Override
	public void onChartTranslate(MotionEvent me, float dX, float dY) {
		Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
	}

	@Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("", "low: " + mChart.getLowestVisibleXIndex() + ", high: " + mChart.getHighestVisibleXIndex());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    public void change(int index) {
        getRecord(5,index);
        for (int i = 0; i < btnList.size(); i++) {
            if (i+1 == index) {
                btnList.get(i).setTextColor(getResColor(R.color.white));
                btnList.get(i).setBackgroundResource(R.drawable.btn_cheeck);
            } else {
                btnList.get(i).setTextColor(getResColor(R.color.main_bg));
                btnList.get(i).setBackgroundResource(R.drawable.btn_no_check);
            }
        }
    }

    private void getRecord(int logType,int day) {
        RecordServer.getInstance(HbaChartActivity.this).getChatRecord(AppContext.currentUser.getUserId(),
                logType + "",day+"", new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }
                    ;
                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Record> lists = (List<Record>) baseModel
                                    .getResultObj();
                            tvLow.setText(baseModel.getLow()+"\n偏低");
                            tvNormal.setText(baseModel.getNormal()+"\n正常");
                            tvHeight.setText(baseModel.getHigh()+"\n偏高");
                            if (lists != null && lists.size() > 0) {
                                ArrayList<String> xVals=new ArrayList<String>();
                                ArrayList<Entry> yVals=new ArrayList<Entry>();
                                lyMore.removeAllViews();;
                                for (int i=0;i<lists.size();i++){
                                    Record item=lists.get(i);
                                    xVals.add(item.getDate());
                                    View v=getLayoutInflater().inflate(R.layout.layout_bloodsugar_chart_value,null);
                                    TextView tv1= (TextView) v.findViewById(R.id.tv_1);
                                    tv1.setText(item.getDate());
                                    TextView tv2= (TextView) v.findViewById(R.id.tv_2);
                                    tv2.setText(item.getTime());
                                    TextView tv3= (TextView) v.findViewById(R.id.tv_3);
                                    tv3.setText(item.getLogVal1());
                                    TextView tv4= (TextView) v.findViewById(R.id.tv_4);
                                    tv4.setVisibility(View.GONE);
                                    TextView tv5= (TextView) v.findViewById(R.id.tv_5);
                                    tv5.setVisibility(View.GONE);
                                    yVals.add(new Entry(Float.parseFloat(item.getLogVal1()), i));
                                    TextView tv6= (TextView) v.findViewById(R.id.tv_6);
                                    tv6.setVisibility(View.GONE);
                                    TextView tv7= (TextView) v.findViewById(R.id.tv_7);
                                    tv7.setVisibility(View.GONE);
                                    TextView tv8= (TextView) v.findViewById(R.id.tv_8);
                                    tv8.setVisibility(View.GONE);
                                    TextView tv9= (TextView) v.findViewById(R.id.tv_9);
                                    tv9.setVisibility(View.GONE);
                                    lyMore.addView(v);
                                }
                                setData(xVals,yVals);
                            }
                        }
                    }
                });
    }
}
