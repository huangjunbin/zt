package com.core.zt.app.ui;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.app.adapter.ReportAdapter;
import com.core.zt.model.Report;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.LessonServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.XListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class ReportActivity extends BaseActivity implements XListView.IXListViewListener {
    private HeaderBar headerBar;
    private XListView xListView;
    private ReportAdapter adapter;
    public static List<Report> dataList;
    private int page = 1;
    private int rows = 5;
    private boolean isRefresh = true;
    private int type = 0;
    private String id;

    public ReportActivity() {
        super(R.layout.activity_report);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        xListView = (XListView) findViewById(R.id.messagelist);
        xListView.setXListViewListener(this);
    }

    @Override
    public void initData() {
        dataList = new ArrayList<Report>();
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // 获取当前年份
        int mMonth = c.get(Calendar.MONTH) + 1;// 获取当前月份
        Date date = DateUtil.parseDate(AppContext.currentUser.getCreateDate(), "yyyy-MM-dd");
        int bYear = DateUtil.getYear(date);
        int bMonth = DateUtil.getMonth(date);


        for (int i = mYear; i >= bYear; i--) {
            if (mYear > bYear) {
                if (mYear - bYear > 0) {
                    for (int j = 12; j >= 1; j--) {
                        Report r = new Report();
                        r.setName(i + "年" + j + "月\n月度报告");
                        r.setYear(i + "");
                        r.setMonth(j + "");
                        dataList.add(r);
                    }
                } else {
                    for (int j = mMonth; j >= 1; j--) {
                        Report r = new Report();
                        r.setName(i + "年" + j + "月\n月度报告");
                        r.setYear(i + "");
                        r.setMonth(j + "");
                        dataList.add(r);
                    }
                }
            } else {
                for (int j = mMonth; j >= bMonth + 1; j--) {
                    Report r = new Report();
                    r.setName(i + "年" + j + "月\n月度报告");
                    r.setYear(i + "");
                    r.setMonth(j + "");
                    dataList.add(r);
                }
            }

        }

        View top = getLayoutInflater().inflate(R.layout.layout_report_top, null);
        xListView.addHeaderView(top);
        adapter = new ReportAdapter(ReportActivity.this, dataList);
        xListView.setAdapter(adapter);

    }

    @Override
    public void bindViews() {
        headerBar.setTitle("月度报告");
        xListView.setPullLoadEnable(true);
        xListView.setFooterDividersEnabled(false);
        xListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

            }
        });
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        page = 1;
        getMessageList();

    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        getMessageList();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getMessageList() {
        LessonServer.getInstance(context).getLesson(id, page + "", rows + "", type, new CustomAsyncResponehandler() {
            public void onFinish() {
                xListView.stopRefresh();
                xListView.stopLoadMore();
            }

            ;

            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    List<Report> lists = (List<Report>) baseModel
                            .getResultObj();
                    if (lists != null && lists.size() > 0) {
                        dataList.clear();
                        dataList.addAll(0, lists);
                        adapter.notifyDataSetChanged();
                        ;
                    }
                }
            }
        });
    }
}
