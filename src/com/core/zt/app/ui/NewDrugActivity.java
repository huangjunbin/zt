package com.core.zt.app.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.Drug;
import com.core.zt.model.DrugChildItem;
import com.core.zt.model.DrugItem;
import com.core.zt.model.DrugType;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.DrugServer;
import com.core.zt.service.NoticeServer;
import com.core.zt.widget.HeaderBar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NewDrugActivity extends BaseActivity {

    private HeaderBar bar;
    private RelativeLayout rlTime;
    private LinearLayout llTime;
    private TextView tvCount;
    private int count = 0;
    public final static int CODE = 1000;
    private int type = 1;
    private int tag = 0;
    private Button btnDelete;
    public final static int DRUGCODE = 1001;
    public final static int DRUGNUMCODE = 1002;
    private NoticeServer noticeServer;
    private DrugItem item;
    private Button btnSave;
    private TextView tvChange, tvDrugName;
    private Drug drug;
    private List<DrugChildItem> lists;

    public NewDrugActivity() {
        super(R.layout.activity_new_drug);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);
        rlTime = (RelativeLayout) findViewById(R.id.rl_time);
        llTime = (LinearLayout) findViewById(R.id.ll_time);
        tvCount = (TextView) findViewById(R.id.tv_count);
        btnDelete = (Button) findViewById(R.id.btn_delete);
        btnSave = (Button) findViewById(R.id.btn_save);
        tvChange = (TextView) findViewById(R.id.tv_change_drug);
        tvDrugName = (TextView) findViewById(R.id.tv_drug_name);
    }

    @Override
    public void initData() {
        noticeServer = new NoticeServer(this);
        type = getIntent().getIntExtra("type", 1);
        tag = getIntent().getIntExtra("tag", 0);
        item = (DrugItem) getIntent().getSerializableExtra("data");
        if (tag == 0) {
            bar.setTitle("添加用药");
            btnDelete.setVisibility(View.GONE);
        } else {

            bar.setTitle("编辑用药");
            btnDelete.setVisibility(View.VISIBLE);
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvDrugName.getText().toString().length() < 1) {
                    showToast("请选择药品");
                    return;
                }
                if (count < 1) {
                    showToast("剂量和用药时间不能为空");
                    return;
                }

                String doses = "";
                String medicationTimes = "";
                List<DrugChildItem> lists=new ArrayList<DrugChildItem>();
                for (int i = 0; i < llTime.getChildCount(); i++) {
                    DrugChildItem item = (DrugChildItem) llTime.getChildAt(i).getTag();
                    doses += item.getDose() + ";";
                    medicationTimes += item.getMedicationTime() + ";";
                    lists.add(item);
                }
                if(DrugChildItem.isContant(lists)){
                    showToast("有重复的用药时间");
                    return;
                }
                DrugServer.getInstance(context).updateMedicationPlan(item==null?"":item.getPlanId(), AppContext.currentUser.getUserId(), drug.getId(), doses, medicationTimes,
                        new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            ;

                            @SuppressWarnings("unchecked")
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    NewDrugActivity.this.finish();
                                }
                            }
                        });
                NewDrugActivity.this.finish();
            }
        });
        tvChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoticeServer.getInstance(context).getDrugType(
                        new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            ;

                            @SuppressWarnings("unchecked")
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    List<DrugType> dataList = (List<DrugType>) baseModel
                                            .getResultObj();
                                    if (dataList != null && dataList.size() > 0) {
                                        Intent intent = new Intent(NewDrugActivity.this, DrugTypeActivity.class);
                                        intent.putExtra("data", (Serializable) dataList);
                                        startActivityForResult(intent, DRUGCODE, null);
                                    }
                                }
                            }
                        });


            }
        });
    }

    @Override
    public void bindViews() {
        if (item != null) {
            drug=new Drug();
            drug.setId(item.getDrudId() + "");
            drug.setUnit(item.getUnit());
            tvDrugName.setText(item.getDrugChemicalCategoryCnName() + "/" + item.getDrud_cn_name());

            for (int i = 0; i < item.getChild().size(); i++) {

                final DrugChildItem childItem = item.getChild().get(i);
                final View child = getLayoutInflater().inflate(R.layout.layout_drug_num, null);
                child.setTag(childItem);
                childItem.setIndex(i);
                child.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DrugChildItem di= (DrugChildItem) view.getTag();
                        Intent intent = new Intent(NewDrugActivity.this, DrugNumActivity.class);
                        intent.putExtra("count", di.getIndex());
                        intent.putExtra("data", di);
                        intent.putExtra("unit", item.getUnit());
                        startActivityForResult(intent, DRUGNUMCODE, null);
                    }
                });
                TextView tvNum = (TextView) child.findViewById(R.id.tv_num);
                TextView tvTime = (TextView) child.findViewById(R.id.tv_time);
                tvNum.setText(childItem.getDose() + item.getUnit());
                tvTime.setText(childItem.getMedicationTime());
                llTime.addView(child);
                count++;
                tvCount.setText(count + "");
            }

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showSureDialog(NewDrugActivity.this, "确定删除?", "确定", new OnSurePress() {
                        @Override
                        public void onClick(View view) {
                            DrugServer.getInstance(NewDrugActivity.this).deleteMedicationPlanItem(
                                    item.getPlanId(), new CustomAsyncResponehandler() {
                                        public void onFinish() {

                                        }

                                        ;

                                        @SuppressWarnings("unchecked")
                                        @Override
                                        public void onSuccess(ResponeModel baseModel) {
                                            super.onSuccess(baseModel);
                                            if (baseModel != null && baseModel.isStatus()) {
                                                showToast("删除成功");
                                                NewDrugActivity.this.finish();
                                            }
                                        }
                                    });
                        }
                    }, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {

                        }
                    });


                }
            });


        }
        if("".equals(tvDrugName.getText().toString())){
            tvChange.setText("添加药品");
        }else{
            tvChange.setText("更换药品");
        }
        rlTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvDrugName.getText().toString().length() < 1) {
                    showToast("请选择药品");
                    return;
                }
                if (count >= 6) {
                    showToast("不超过6次");
                    return;
                }

                final View child = getLayoutInflater().inflate(R.layout.layout_drug_num, null);
                TextView tvNum = (TextView) child.findViewById(R.id.tv_num);
                TextView tvTime = (TextView) child.findViewById(R.id.tv_time);
                tvNum.setText("1" + drug.getUnit());
                tvTime.setText("08:00");
                final DrugChildItem item=new DrugChildItem();
                item.setDose(1);
                item.setMedicationTime("08:00");
                child.setTag(item);
                item.setIndex(count);
                child.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DrugChildItem di= (DrugChildItem) view.getTag();
                        Intent intent = new Intent(NewDrugActivity.this, DrugNumActivity.class);
                        intent.putExtra("count", di.getIndex());
                        intent.putExtra("data", di);
                        intent.putExtra("unit", drug.getUnit());
                        startActivityForResult(intent, DRUGNUMCODE, null);
                    }
                });
                llTime.addView(child);
                count++;
                tvCount.setText(count + "");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == DRUGNUMCODE) {
            if (data != null) {
                final DrugChildItem item = (DrugChildItem) data.getSerializableExtra("child");
                int c = data.getIntExtra("count", -1);
                if (item != null) {
                    if (c > -1) {
                        View view = llTime.getChildAt(c);
                        view.setTag(item);
                        TextView tvNum = (TextView) view.findViewById(R.id.tv_num);
                        TextView tvTime = (TextView) view.findViewById(R.id.tv_time);
                        tvNum.setText(item.getDose() + drug.getUnit());
                        tvTime.setText(item.getMedicationTime());
                    }
                }else{
                    if (c > -1) {
                        View view = llTime.getChildAt(c);
                        llTime.removeView(view);
                        count--;
                        tvCount.setText(count + "");
                    }
                }
            }
        }
        if (requestCode == DRUGCODE) {
            if (data != null) {
                drug = (Drug) data.getSerializableExtra("data");

                tvDrugName.setText(drug.getDrupCategoryCnName() + "/" + drug.getDrugCnName());
                if("".equals(tvDrugName.getText().toString())){
                    tvChange.setText("添加药品");


                }else{
                    tvChange.setText("更换药品");
                    if(count==0){
                        final View child = getLayoutInflater().inflate(R.layout.layout_drug_num, null);
                        TextView tvNum = (TextView) child.findViewById(R.id.tv_num);
                        TextView tvTime = (TextView) child.findViewById(R.id.tv_time);
                        tvNum.setText("1" + drug.getUnit());
                        tvTime.setText("08:00");
                        final DrugChildItem item=new DrugChildItem();
                        item.setDose(1);
                        item.setMedicationTime("08:00");
                        child.setTag(item);
                        item.setIndex(count);
                        child.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DrugChildItem di= (DrugChildItem) view.getTag();
                                Intent intent = new Intent(NewDrugActivity.this, DrugNumActivity.class);
                                intent.putExtra("count", di.getIndex());
                                intent.putExtra("data", di);
                                intent.putExtra("unit", drug.getUnit());
                                startActivityForResult(intent, DRUGNUMCODE, null);
                            }
                        });
                        llTime.addView(child);
                        count++;
                        tvCount.setText(count + "");
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
