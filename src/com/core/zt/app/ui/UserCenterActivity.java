package com.core.zt.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.Case;
import com.core.zt.model.ResponeModel;
import com.core.zt.model.Type;
import com.core.zt.model.User;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.UserServer;
import com.core.zt.util.DateUtil;
import com.core.zt.util.FileUtils;
import com.core.zt.util.MediaUtil;
import com.core.zt.widget.CircleImageView;
import com.core.zt.widget.HeaderBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UserCenterActivity extends BaseActivity {

    private HeaderBar bar;
    private RelativeLayout rl1;
    private RelativeLayout r1, r2, r3, r4, r5;
    private TextView tv1, tv2, tv3, tv4, tv5;
    public final static int CODE1 = 1001;
    public final static int CODE2 = 1002;
    public final static int CODE3 = 1003;
    public final static int CODE4 = 1004;
    public final static int CODE5 = 1005;
    public Case item;

    private static final int requestCode_updateAddress = 100;
    private TextView tvNickName, tvName, tvPhone, tvSex, tvDate, tvArea;
    private RelativeLayout rlNickName, rlName, rlPhone, rlSex, rlDate, rlArea, rlPhoto;
    private Intent intent;
    public static String appPath = "/zt/";
    private String userPhotoName = "img.jpg";
    private CircleImageView ivPhoto;


    public UserCenterActivity() {
        super(R.layout.activity_user_center);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);
        rl1 = (RelativeLayout) findViewById(R.id.rl_r1);
        r1 = (RelativeLayout) findViewById(R.id.rl_1);
        r2 = (RelativeLayout) findViewById(R.id.rl_2);
        r3 = (RelativeLayout) findViewById(R.id.rl_3);
        r4 = (RelativeLayout) findViewById(R.id.rl_4);
        r5 = (RelativeLayout) findViewById(R.id.rl_5);
        tv1 = (TextView) findViewById(R.id.tv_1);
        tv2 = (TextView) findViewById(R.id.tv_2);
        tv3 = (TextView) findViewById(R.id.tv_3);
        tv4 = (TextView) findViewById(R.id.tv_4);
        tv5 = (TextView) findViewById(R.id.tv_5);


        tvNickName = (TextView) findViewById(R.id.tv_nickname);
        tvName = (TextView) findViewById(R.id.tv_name);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
        tvSex = (TextView) findViewById(R.id.tv_sex);
        tvDate = (TextView) findViewById(R.id.tv_date);
        tvArea = (TextView) findViewById(R.id.tv_area);

        rlNickName = (RelativeLayout) findViewById(R.id.rl_nickname);
        rlName = (RelativeLayout) findViewById(R.id.rl_name);
        rlPhone = (RelativeLayout) findViewById(R.id.rl_phone);
        rlSex = (RelativeLayout) findViewById(R.id.rl_sex);
        rlDate = (RelativeLayout) findViewById(R.id.rl_date);
        rlArea = (RelativeLayout) findViewById(R.id.rl_area);
        rlPhoto = (RelativeLayout) findViewById(R.id.rl_photo);
        ivPhoto = (CircleImageView) findViewById(R.id.iv_photo);
    }

    @Override
    protected void onResume() {
        tvNickName.setText(AppContext.currentUser.getNickName());
        tvName.setText(AppContext.currentUser.getRealName());
        tvPhone.setText(AppContext.currentUser.getUserName());
        tvSex.setText(AppContext.currentUser.getSex() == 1 ? "男" : "女");
        if (AppContext.currentUser.getBirthday() != null) {
            Date date = DateUtil.parseDate(AppContext.currentUser.getBirthday(), "yyyy-MM-dd HH:mm:ss");
            tvDate.setText(DateUtil.getDate(date));
        } else {
            tvDate.setText("");
        }
        tvArea.setText(AppContext.currentUser.getArea());
        super.onResume();
    }

    @Override
    public void initData() {
        bar.setTitle("个人档案");
    }

    @Override
    public void bindViews() {
        item = new Case();
        UserServer.getInstance(context).getCase(
                AppContext.currentUser.getUserId(), new CustomAsyncResponehandler() {
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            item = (Case) baseModel.getResultObj();
                            tv1.setText(item.getDiabetesTypeLabel());
                            tv2.setText(item.getDiagnosisTime());
                            tv3.setText(item.getConcurrentTypeLabel());
                            tv4.setText(item.getHealthTypeLabel());
                            tv5.setText(item.getSolutionTypeLabel());
                        }
                    }
                });
        rl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JumpToActivity(UserCenterActivity.class, false);
            }
        });
        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go(1);
            }
        });

        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        UserCenterActivity.this);
                View v = getLayoutInflater().inflate(
                        R.layout.date_time_dialog, null);
                final DatePicker datePicker = (DatePicker) v
                        .findViewById(R.id.date_picker);
                final TimePicker timePicker = (android.widget.TimePicker) v
                        .findViewById(R.id.time_picker);
                timePicker.setVisibility(View.GONE);
                datePicker.setVisibility(View.VISIBLE);
                builder.setView(v);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                datePicker.init(cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH), null);

                timePicker.setIs24HourView(true);
                timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(Calendar.MINUTE);
                builder.setTitle("请选择确诊时间");
                builder.setPositiveButton("确  定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                StringBuffer sb = new StringBuffer();
                                sb.append(String.format("%d-%02d-%02d",
                                        datePicker.getYear(),
                                        datePicker.getMonth() + 1,
                                        datePicker.getDayOfMonth()));
//                                sb.append("  ");
//                                sb.append(timePicker.getCurrentHour())
//                                        .append(":")
//                                        .append(timePicker.getCurrentMinute());
                                item.setDiagnosisTime(sb.toString());
                                update();
                                tv2.setText(sb);
                                dialog.cancel();
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });
        r3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go(2);
            }
        });
        r4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go(3);
            }
        });
        r5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go(4);
            }
        });


        AppContext.setImage(AppContext.currentUser.getPhoto(), ivPhoto, AppContext.memberPhotoOption);
        tvNickName.setText(AppContext.currentUser.getNickName());
        tvName.setText(AppContext.currentUser.getRealName());
        tvPhone.setText(AppContext.currentUser.getMobile());
        tvSex.setText(AppContext.currentUser.getSex() == 1 ? "男" : "女");
        tvDate.setText(AppContext.currentUser.getBirthday());
        tvArea.setText(AppContext.currentUser.getArea());
        rlNickName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserCenterActivity.this, EditActivity.class);
                intent.putExtra("title", "修改昵称");
                intent.putExtra("type", 1);
                startActivity(intent);
            }
        });
        rlName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserCenterActivity.this, EditActivity.class);
                intent.putExtra("title", "修改姓名");
                intent.putExtra("type", 2);
                startActivity(intent);
            }
        });
        tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                intent = new Intent(UserCenterActivity.this, EditActivity.class);
//                intent.putExtra("title", "修改电话");
//                intent.putExtra("type", 3);
//                startActivity(intent);
            }
        });


        rlArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                intent = new Intent(UserCenterActivity.this, EditActivity.class);
//                intent.putExtra("title", "修改区域");
//                intent.putExtra("type", 7);
//                startActivity(intent);
                // 点击地址更新
                Intent intent_address = new Intent(UserCenterActivity.this,
                        AddressActivity.class);
                // this.startActivity(intent_address);
                intent_address.putExtra("address", tvArea.getText().toString());
                startActivityForResult(intent_address, requestCode_updateAddress);
            }
        });

        rlDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        UserCenterActivity.this);
                View v = getLayoutInflater().inflate(
                        R.layout.date_time_dialog, null);
                final DatePicker datePicker = (DatePicker) v
                        .findViewById(R.id.date_picker);
                final TimePicker timePicker = (android.widget.TimePicker) v
                        .findViewById(R.id.time_picker);
                timePicker.setVisibility(View.GONE);
                datePicker.setVisibility(View.VISIBLE);
                builder.setView(v);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                datePicker.init(cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH), null);

                timePicker.setIs24HourView(true);
                timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(Calendar.MINUTE);
                builder.setTitle("请选择出生日期");
                builder.setPositiveButton("确  定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                StringBuffer sb = new StringBuffer();
                                sb.append(String.format("%d-%02d-%02d",
                                        datePicker.getYear(),
                                        datePicker.getMonth() + 1,
                                        datePicker.getDayOfMonth()));
                                sb.append("");
                             /*   sb.append(timePicker.getCurrentHour())
                                        .append(":")
                                        .append(timePicker.getCurrentMinute());*/
                                final User user = AppContext.currentUser;
                                user.setBirthday(sb.toString());
                                UserServer.getInstance(context).updateInfo(
                                        user, new CustomAsyncResponehandler() {
                                            public void onFinish() {

                                            }

                                            ;

                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onSuccess(ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel != null && baseModel.isStatus()) {
                                                    AppContext.currentUser = user;
                                                    UserServer.getInstance(context).updateUser(user);
                                                    tvDate.setText(AppContext.currentUser.getBirthday());
                                                }
                                            }
                                        });
                                dialog.cancel();
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });

        rlSex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(UserCenterActivity.this)
                        .setTitle("请选择性别")
                        .setSingleChoiceItems(new String[]{"女", "男"}, 0,
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        final User user = AppContext.currentUser;
                                        user.setSex(which);
                                        UserServer.getInstance(context).updateInfo(
                                                user, new CustomAsyncResponehandler() {
                                                    public void onFinish() {

                                                    }

                                                    ;

                                                    @SuppressWarnings("unchecked")
                                                    @Override
                                                    public void onSuccess(ResponeModel baseModel) {
                                                        super.onSuccess(baseModel);
                                                        if (baseModel != null && baseModel.isStatus()) {
                                                            AppContext.currentUser = user;
                                                            UserServer.getInstance(context).updateUser(user);
                                                            tvSex.setText(AppContext.currentUser.getSex() == 1 ? "男" : "女");
                                                        }
                                                    }
                                                });
                                        dialog.dismiss();
                                    }
                                }
                        )
                        .show();
            }
        });

        rlPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPhotoDialog(new OnSurePress() {
                    @Override
                    public void onClick(View view) {
                        if (FileUtils.isSDCardExisd()) {
                            MediaUtil.searhcAlbum(UserCenterActivity.this,
                                    MediaUtil.ALBUM);
                        } else {
                            showToast("SDCARD不存在！");
                        }
                    }
                }, new OnSurePress() {
                    @Override
                    public void onClick(View view) {
                        if (FileUtils.isSDCardExisd()) {
                            MediaUtil.takePhoto(UserCenterActivity.this,
                                    MediaUtil.PHOTO, appPath, userPhotoName);
                        } else {
                            showToast("SDCARD不存在！");
                        }
                    }


                });
            }
        });


    }

    private void go(final int type) {
        UserServer.getInstance(context).getType(
                type, AppContext.currentUser.getUserId(), new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Type> dataList = (List<Type>) baseModel
                                    .getResultObj();
                            if (dataList != null && dataList.size() > 0) {

                                Intent intent = new Intent(UserCenterActivity.this, TypeActivity.class);
                                intent.putExtra("data", (Serializable) dataList);
                                intent.putExtra("type", 1);
                                if (type == 1) {
                                    intent.putExtra("title", "选择糖尿病类型");
                                    for (Type type : dataList) {
                                        if (type.getLabel().trim().equals(tv1.getText().toString().trim())) {
                                            type.setCheck(true);
                                        }
                                    }
                                    startActivityForResult(intent, CODE1, null);
                                }
                                if (type == 2) {
                                    intent.putExtra("title", "选择并发情况");
                                    intent.putExtra("type", 0);
                                    String[] str = tv3.getText().toString().split(";");
                                    for (Type type : dataList) {
                                        for (String s : str) {
                                            if (type.getLabel().equals(s)) {
                                                type.setCheck(true);
                                            }
                                        }
                                    }
                                    startActivityForResult(intent, CODE3, null);
                                }
                                if (type == 3) {
                                    intent.putExtra("title", "选择健康情况");
                                    intent.putExtra("type", 0);
                                    String[] str = tv4.getText().toString().split(";");
                                    for (Type type : dataList) {
                                        for (String s : str) {
                                            if (type.getLabel().equals(s)) {
                                                type.setCheck(true);
                                            }
                                        }
                                    }
                                    startActivityForResult(intent, CODE4, null);
                                }
                                if (type == 4) {
                                    for (Type type : dataList) {
                                        if (type.getLabel().trim().equals(tv5.getText().toString().trim())) {
                                            type.setCheck(true);
                                        }
                                    }
                                    intent.putExtra("title", "选择高血糖治疗方案");
                                    startActivityForResult(intent, CODE5, null);
                                }
                            }
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == CODE1) {
            if (data != null) {
                String value = "";
                List<Type> dataList = (List<Type>) data.getSerializableExtra("data");
                int type = data.getIntExtra("type", 0);

                String str = "";
                for (Type item : dataList) {
                    if (item.isCheck()) {
                        str += item.getLabel() + " ";
                        value = item.getValue();
                    }
                }
                if (type == 1) {
                    tv1.setText(str);
                    return;
                }
                tv1.setText(str);
                item.setDiabetesType(value);
                update();
            } else {
                tv1.setText("未选择");
            }
        }
        if (requestCode == CODE2) {
            if (data != null) {
                List<Type> dataList = (List<Type>) data.getSerializableExtra("data");
                String str = "";
                for (Type item : dataList) {
                    if (item.isCheck()) {
                        str += item.getLabel() + " ";
                    }
                }
                tv2.setText(str);
            } else {
                tv2.setText("未选择");
            }
        }
        if (requestCode == CODE3) {
            if (data != null) {
                String value = "";
                List<Type> dataList = (List<Type>) data.getSerializableExtra("data");
                int type = data.getIntExtra("type", 0);

                String str = "";
                for (Type item : dataList) {
                    if (item.isCheck()) {
                        str += item.getLabel() + ";";
                        value += item.getValue() + ";";
                    }
                }
                if (type == 1) {
                    tv3.setText(str);
                    return;
                }
                tv3.setText(str);
                item.setConcurrentType(value);
                update();
            } else {
                tv3.setText("未选择");
            }
        }
        if (requestCode == CODE4) {
            if (data != null) {
                String value = "";
                List<Type> dataList = (List<Type>) data.getSerializableExtra("data");
                int type = data.getIntExtra("type", 0);

                String str = "";
                for (Type item : dataList) {
                    if (item.isCheck()) {
                        System.out.print(str);
                        str += item.getLabel() + ";";
                        value += item.getValue() + ";";
                    }
                }
                if (type == 1) {
                    tv4.setText(str);
                    return;
                }
                tv4.setText(str);
                item.setHealthType(value);
                update();
            } else {
                tv4.setText("未选择");
            }
        }
        if (requestCode == CODE5) {
            if (data != null) {
                String value = "";
                List<Type> dataList = (List<Type>) data.getSerializableExtra("data");
                int type = data.getIntExtra("type", 0);

                String str = "";
                for (Type item : dataList) {
                    if (item.isCheck()) {
                        str += item.getLabel() + " ";
                        value = item.getValue();
                    }
                }
                if (type == 1) {
                    tv5.setText(str);
                    return;
                }
                tv5.setText(str);
                item.setSolutionType(value);
                update();
            } else {
                tv5.setText("未选择");
            }
        }
        switch (requestCode) {
            case MediaUtil.ALBUM:
                if (data != null || resultCode == Activity.RESULT_OK) {
                    startPhotoZoom(data.getData());
                }
                break;
            case MediaUtil.PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    File temp = new File(MediaUtil.ROOTPATH + appPath
                            + userPhotoName);
                    startPhotoZoom(Uri.fromFile(temp));
                }
                break;
            case MediaUtil.ZOOMPHOTO:
                if (data != null || resultCode == Activity.RESULT_OK) {
                    setPicToView(data);
                }
                break;
            // 地址修改
            case requestCode_updateAddress:
                switch (resultCode) {
                    case RESULT_OK:
                        // 修改用户邮箱成功
                        final String address = data.getStringExtra("address");
                        if (address != null && !"".equals(address)) {

                            final User user = AppContext.currentUser;
                            user.setArea(address);
                            UserServer.getInstance(context).updateInfo(
                                    user, new CustomAsyncResponehandler() {
                                        public void onFinish() {

                                        }

                                        ;

                                        @SuppressWarnings("unchecked")
                                        @Override
                                        public void onSuccess(ResponeModel baseModel) {
                                            super.onSuccess(baseModel);
                                            if (baseModel != null && baseModel.isStatus()) {
                                                AppContext.currentUser = user;
                                                UserServer.getInstance(context).updateUser(user);
                                                tvArea.setText(address);
                                            }
                                        }
                                    });
                        }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void update() {
        UserServer.getInstance(this).updateCase(item, new CustomAsyncResponehandler() {
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    showToast("修改成功");
                }
            }
        });
    }

    private void setPicToView(Intent data) {
        Bundle extras = data.getExtras();
        if (extras != null) {
            final Bitmap img = extras.getParcelable("data");
            ivPhoto.setImageBitmap(img);
            UserServer.getInstance(context).uploadImg(
                    new File(MediaUtil.ROOTPATH + appPath + userPhotoName),
                    new CustomAsyncResponehandler() {
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel.isStatus()) {
                                String url = "";
                                try {
                                    JSONObject object = new JSONObject(baseModel.getResult());
                                    url = object.getString("url");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Log.d("url:", url);

                                final User user = AppContext.currentUser;
                                user.setPhoto(url);
                                UserServer.getInstance(context).updateInfo(
                                        user, new CustomAsyncResponehandler() {
                                            public void onFinish() {

                                            }

                                            ;

                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onSuccess(ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel != null && baseModel.isStatus()) {
                                                    AppContext.currentUser = user;
                                                    UserServer.getInstance(context).updateUser(user);
                                                    ivPhoto.setImageBitmap(img);
                                                }
                                            }
                                        });
                            }
                        }
                    });
        }
    }

    /**
     * 裁剪图片方法实现
     *
     * @param uri
     */
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        File dir = new File((MediaUtil.ROOTPATH + appPath));
        if (!dir.exists())
            dir.mkdirs();

        File saveFile = new File(MediaUtil.ROOTPATH + appPath + userPhotoName);

        intent.putExtra("output", Uri.fromFile(saveFile)); // 传入目标文件
        intent.putExtra("outputFormat", "JPEG"); // 输入文件格式
        Intent it = Intent.createChooser(intent,
                "剪裁图片");
        startActivityForResult(it, MediaUtil.ZOOMPHOTO);
    }

}
