package com.core.zt.app.ui;

import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.model.Drug;
import com.core.zt.model.DrugType;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.NoticeServer;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.lib.PullToRefreshBase;
import com.core.zt.widget.lib.PullToRefreshExpandableListView;

import java.io.Serializable;
import java.util.List;

public class DrugTypeActivity extends BaseActivity {
    public DrugTypeActivity() {
        super(R.layout.activity_drugtype);
    }
    private EditText editText;
    private PullToRefreshExpandableListView expandableListView;
    private ExpandableListView mView;
    private List<DrugType> groupInfoList;
    private MyExpListAdapter adapter;
    private HeaderBar bar;
    private int tag = 0;
    private int flag = -1;

    public void initViews() {
        expandableListView = (PullToRefreshExpandableListView) findViewById(R.id.expandableListView);

        bar = (HeaderBar) findViewById(R.id.header);
        editText = (EditText) findViewById(R.id.search_edit);
        editText.setHint("输入关键字搜索");
        editText.setHintTextColor(getResColor(R.color.gray));
        editText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
    }

    @SuppressWarnings("unchecked")
    public void initData() {
        groupInfoList = (List<DrugType>) getIntent().getSerializableExtra(
                "data");
        tag = getIntent().getIntExtra("tag", 0);
        flag = getIntent().getIntExtra("flag", 0);
    }

    @SuppressWarnings("deprecation")
    public void bindViews() {
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
               String searchKey = editText.getText().toString();
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    NoticeServer.getInstance(context).getDrug(searchKey,"" , new CustomAsyncResponehandler() {
                        @SuppressWarnings("unchecked")
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel != null && baseModel.isStatus()) {
                                List<Drug> lists = (List<Drug>) baseModel
                                        .getResultObj();
                                if (lists != null && lists.size() > 0) {
                                    Intent intent = new Intent();
                                    intent.putExtra("data", (Serializable) lists);
                                    intent.setClass(DrugTypeActivity.this, DrugActivity.class);
                                    startActivityForResult(intent, NewNoticeActivity.DRUGCODE, null);
                                } else {
                                    showToast("无搜索结果");
                                }
                            }
                        }
                    });
                    return true;
                }
                return false;
            }
        });
        mView = expandableListView.getAdapterView();

        expandableListView.setPullToRefreshEnabled(false);
        expandableListView.setOnUpPullRefreshListener(new PullToRefreshBase.OnRefreshListener() {

            @Override
            public void onRefresh() {

            }
        });

        mView.setOnGroupExpandListener(new OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });
        mView.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                NoticeServer.getInstance(context).getDrug("",groupInfoList.get(groupPosition).getChild().get(childPosition).getId(), new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Drug> lists = (List<Drug>) baseModel
                                    .getResultObj();
                            if (lists != null && lists.size() >0) {
                                Intent intent=new Intent();
                                intent.putExtra("data", (Serializable) lists);
                                intent.setClass(DrugTypeActivity.this, DrugActivity.class);
                                startActivityForResult(intent,NewNoticeActivity.DRUGCODE, null);
                            }else{
                                showToast("暂无药品");
                            }
                        }
                    }
                });
                return false;
            }
        });

        bar.setTitle("药品分类");
        adapter = new MyExpListAdapter(DrugTypeActivity.this);
        mView.setAdapter(adapter);
    }

    public class MyExpListAdapter extends BaseExpandableListAdapter {
        private Context context;
        ViewHolder holder;

        public MyExpListAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getGroupCount() {

            return groupInfoList == null ? 0 : groupInfoList.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return groupInfoList.get(groupPosition) == null ? 0 : groupInfoList
                    .get(groupPosition).getChild() == null ? 0 : groupInfoList
                    .get(groupPosition).getChild().size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupInfoList.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return groupInfoList.get(groupPosition).getChild()
                    .get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public View getGroupView(final int groupPosition,
                                 final boolean isExpanded, View convertView, ViewGroup parent) {
            View v = convertView;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.group_item, null);
            }
            TextView groupText = (TextView) v.findViewById(R.id.groupText);

            final String gname = groupInfoList.get(groupPosition).getDrupCategoryCnName();
            groupText.setText(gname);

            return v;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.child_item, null);
                holder = new ViewHolder();
                holder.cText = (TextView) convertView
                        .findViewById(R.id.childText);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.cText.setText(groupInfoList.get(groupPosition).getChild()
                    .get(childPosition).getDrupCategoryCnName());

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    }

    private static class ViewHolder {
        TextView cText;

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NewNoticeActivity.DRUGCODE) {
            if (data != null) {
                setResult(NewNoticeActivity.DRUGCODE, data);
                DrugTypeActivity.this.finish();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}