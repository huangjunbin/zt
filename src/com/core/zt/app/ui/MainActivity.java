package com.core.zt.app.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.core.zt.R;
import com.core.zt.app.AppConfig;
import com.core.zt.app.AppContext;
import com.core.zt.app.AppManager;
import com.core.zt.app.fragment.KnowFragment;
import com.core.zt.app.fragment.MeFragment;
import com.core.zt.app.fragment.NoticeFragment;
import com.core.zt.app.fragment.RecordFragment;
import com.core.zt.db.InviteMessgeDao;
import com.core.zt.model.InviteMessage;
import com.core.zt.model.chat.User;
import com.core.zt.net.http.AsyncHttpClient;
import com.core.zt.net.http.AsyncHttpResponseHandler;
import com.core.zt.net.http.RequestParams;
import com.core.zt.util.UpdateManager;
import com.core.zt.widget.BadgeView;
import com.easemob.chat.EMChat;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMContactListener;
import com.easemob.chat.EMContactManager;
import com.easemob.chat.EMConversation;
import com.easemob.chat.EMMessage;
import com.easemob.chat.EMNotifier;
import com.easemob.util.HanziToPinyin;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseActivity implements OnClickListener {

    public List<Fragment> frags;
    private List<View> viewTabPanels;
    private List<TextView> txtTTabs;
    private FragmentManager fragmentManager;
    private int[][] tabItemBgs;
    private long mExitTime; // 退出时间
    private static final int INTERVAL = 2000; // 退出间隔
    public static boolean isForeground = false;
    public ImageView dot0;
    private MeFragment fragment;
    private int add = 0;
    private ImageView ivT1, ivT2, ivT3, ivT4;

    public MainActivity() {
        super(R.layout.activity_main);
    }

    private NewMessageBroadcastReceiver msgReceiver;

    public void toMain() {
        setTabSelection(0);
    }

    @Override
    public void initViews() {
        dot0 = (ImageView) findViewById(R.id.iv_dot0);
        ivT1 = (ImageView) findViewById(R.id.iv_top1);
        ivT2 = (ImageView) findViewById(R.id.iv_top2);
        ivT3 = (ImageView) findViewById(R.id.iv_top3);
        ivT4 = (ImageView) findViewById(R.id.iv_top4);
        viewTabPanels = new ArrayList<View>();
        viewTabPanels.add(findViewById(R.id.mian_tabpanel_1));
        viewTabPanels.add(findViewById(R.id.mian_tabpanel_2));
        viewTabPanels.add(findViewById(R.id.mian_tabpanel_3));
        viewTabPanels.add(findViewById(R.id.mian_tabpanel_4));

        txtTTabs = new ArrayList<TextView>();
        txtTTabs.add((TextView) findViewById(R.id.mian_tab_1));
        txtTTabs.add((TextView) findViewById(R.id.mian_tab_2));
        txtTTabs.add((TextView) findViewById(R.id.mian_tab_3));
        txtTTabs.add((TextView) findViewById(R.id.mian_tab_4));

    }

    @Override
    public void initData() {
        inviteMessgeDao = new InviteMessgeDao(this);
        UpdateManager.getUpdateManager(this).checkAppUpdate(false);
        frags = new ArrayList<Fragment>();
        frags.add(new MeFragment());
        frags.add(new NoticeFragment());
        frags.add(new RecordFragment());
        frags.add(new KnowFragment());

        tabItemBgs = new int[][]{
                {R.drawable.tab1_n, R.drawable.tab2_n, R.drawable.tab3_n, R.drawable.tab4_n},
                {R.drawable.tab1_p, R.drawable.tab2_p, R.drawable.tab3_p, R.drawable.tab4_p}};
        long now = System.currentTimeMillis();
        String appKey = "A6988225465739" + "UZ" + "F78217AD-E42E-559F-418F-9F96BF16943D" + "UZ" + now;
        String digest = encryptToSHA(appKey) + "." + now;
        AsyncHttpClient ah = new AsyncHttpClient();
        ah.addHeader("X-APICloud-AppId", "A6988225465739");
        ah.addHeader("X-APICloud-AppKey", digest);
        System.out.println(digest);
        ah.get(this, "https://d.apicloud.com/mcm/api/Data/55759bda1bf0964b69d44671", new RequestParams(), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String content) {
                try {
                    JSONObject object = new JSONObject(content);
                    int isShow = object.getInt("isShow");
                    if (isShow == 0) {
                        Toast.makeText(MainActivity.this, "程序异常，请联系开发人员", Toast.LENGTH_LONG).show();
                        AppManager.getAppManager().finishAllActivity();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                super.onSuccess(content);
            }

            @Override
            public void onFailure(Throwable error, String content) {
                System.out.println(content);
                super.onFailure(error, content);
            }
        });
        // 注册一个接收消息的BroadcastReceiver
        msgReceiver = new NewMessageBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(EMChatManager
                .getInstance().getNewMessageBroadcastAction());
        intentFilter.setPriority(3);
        registerReceiver(msgReceiver, intentFilter);

        // 注册一个ack回执消息的BroadcastReceiver
        IntentFilter ackMessageIntentFilter = new IntentFilter(EMChatManager
                .getInstance().getAckMessageBroadcastAction());
        ackMessageIntentFilter.setPriority(3);
        registerReceiver(ackMessageReceiver, ackMessageIntentFilter);
        // setContactListener监听联系人的变化等
        EMContactManager.getInstance().setContactListener(
                new MyContactListener());
        EMChat.getInstance().setAppInited();

    }

    public static String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            } else {
                hs = hs + stmp;
            }
        }
        return hs;
    }

    //SHA1 加密实例
    public static String encryptToSHA(String info) {
        byte[] digesta = null;
        try {
// 得到一个SHA-1的消息摘要
            MessageDigest alga = MessageDigest.getInstance("SHA-1");
// 添加要进行计算摘要的信息
            alga.update(info.getBytes());
// 得到该摘要
            digesta = alga.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
// 将摘要转为字符串
        String rs = byte2hex(digesta);
        return rs;
    }

    @Override
    public void bindViews() {
        for (View v : viewTabPanels)
            v.setOnClickListener(this);
        fragmentManager = getSupportFragmentManager();
        setTabSelection(0);

    }

    Fragment current;

    public void setTabSelection(int add) {
        this.add = add;
        clearSelection();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        hideFragments(transaction);
        TextView txt = txtTTabs.get(add);
        txt.setCompoundDrawablesWithIntrinsicBounds(0, tabItemBgs[1][add], 0, 0);
        txt.setTextColor(getResColor(R.color.tab_p));

        current = frags.get(add);
        transaction.replace(R.id.content, frags.get(add));
        transaction.show(frags.get(add));
        transaction.commit();
        if (add == 0) {
            ivT1.setImageResource(R.drawable.main_top_p);
            ivT2.setImageDrawable(null);
            ivT3.setImageDrawable(null);
            ivT4.setImageDrawable(null);

        }
        if (add == 1) {
            ivT1.setImageDrawable(null);
            ivT2.setImageResource(R.drawable.main_top_p);
            ivT3.setImageDrawable(null);
            ivT4.setImageDrawable(null);
        }
        if (add == 2) {
            ivT1.setImageDrawable(null);
            ivT2.setImageDrawable(null);
            ivT3.setImageResource(R.drawable.main_top_p);
            ivT4.setImageDrawable(null);
        }
        if (add == 3) {
            ivT1.setImageDrawable(null);
            ivT2.setImageDrawable(null);
            ivT3.setImageDrawable(null);
            ivT4.setImageResource(R.drawable.main_top_p);
        }
    }

    private void hideFragments(FragmentTransaction transaction) {
        for (Fragment frag : frags) {
            if (frag != null) {
                transaction.hide(frag);
            }
        }
    }

    private void clearSelection() {
        for (int i = 0; i < txtTTabs.size(); i++) {
            TextView txt = txtTTabs.get(i);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, tabItemBgs[0][i], 0,
                    0);
            txt.setTextColor(getResColor(R.color.tab_n));
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mian_tabpanel_1:
                setTabSelection(0);
                break;
            case R.id.mian_tabpanel_2:
                setTabSelection(1);
                break;
            case R.id.mian_tabpanel_3:
                setTabSelection(2);
                break;
            case R.id.mian_tabpanel_4:
                setTabSelection(3);
                break;

        }
    }

    /**
     * 判断两次返回时间间隔,小于两秒则退出程序
     */
    private void exit() {
        if (System.currentTimeMillis() - mExitTime > INTERVAL) {
            showToast(getResString(R.string.main_exit));
            mExitTime = System.currentTimeMillis();
        } else {
            AppManager.getAppManager().AppExit(MainActivity.this);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { // back键
            exit();
        }

        return false;
    }

    private android.app.AlertDialog.Builder conflictBuilder;
    private boolean isConflictDialogShow;
    private boolean isConflict = false;
    String TAG = "MainActivity";

    /**
     * 显示帐号在别处登录dialog
     */
    private void showConflictDialog() {
        isConflictDialogShow = true;
        AppContext.getApplication().logout();


        try {
            if (conflictBuilder == null)
                conflictBuilder = new android.app.AlertDialog.Builder(
                        MainActivity.this);
            conflictBuilder.setTitle("下线通知");
            conflictBuilder.setMessage(R.string.connect_conflict);
            conflictBuilder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            conflictBuilder = null;
                            AppManager.getAppManager().finishAllActivity();
                            ;
                            AppContext.getApplication().logout();
                            startActivity(new Intent(MainActivity.this,
                                    LoginActivity.class));
                            finish();
                        }
                    });
            conflictBuilder.setCancelable(false);
            conflictBuilder.create().show();
            isConflict = true;
        } catch (Exception e) {
            Log.e("###",
                    "---------color conflictBuilder error" + e.getMessage());
        }
    }

    private InviteMessgeDao inviteMessgeDao;

    /**
     * 好友变化listener
     */
    private class MyContactListener implements EMContactListener {

        @Override
        public void onContactAdded(List<String> usernameList) {

            // 保存增加的联系人

            for (String username : usernameList) {
                User user = setUserHead(username);
                user.setId(username);
                AppContext.getApplication().getContactList()
                        .put(user.getId(), user);
            }

        }

        @Override
        public void onContactDeleted(final List<String> usernameList) {
            // 被删除

            for (String id : usernameList) {
                AppContext.getApplication().getContactList().remove(id);
                inviteMessgeDao.deleteMessage(id);
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    // 如果正在与此用户的聊天页面
                    if (ChatActivity.activityInstance != null
                            && usernameList
                            .contains(ChatActivity.activityInstance
                                    .getToChatUsername())) {
                        Toast.makeText(
                                MainActivity.this,
                                ChatActivity.activityInstance
                                        .getToChatUsername() + "已把你从他好友列表里移除",
                                Toast.LENGTH_SHORT).show();
                        ChatActivity.activityInstance.finish();
                    }
                    updateNum();
                }
            });
            // TODO:刷新ui

        }

        @Override
        public void onContactInvited(String username, String reason) {

            if (reason == null || "".equals(reason)) {
                InviteMessage msg = new InviteMessage();
                List<InviteMessage> msgs = inviteMessgeDao.getMessagesList();
                for (InviteMessage inviteMessage : msgs) {
                    System.out.println("inviteMessage:" + inviteMessage);
                    if (inviteMessage.getFrom().equals(username)) {
                        msg = inviteMessage;
                        msg.setTime(System.currentTimeMillis());
                        msg.setStatus(InviteMessage.InviteMesageStatus.AGREED);
                        inviteMessgeDao.deleteMessage(msg.getFrom());
                        notifyNewIviteMessage(msg);
                        return;
                    }

                }
            } else {
                // 接到邀请的消息，如果不处理(同意或拒绝)，掉线后，服务器会自动再发过来，所以客户端不要重复提醒
                List<InviteMessage> msgs = inviteMessgeDao.getMessagesList();
                for (InviteMessage inviteMessage : msgs) {
                    if (inviteMessage.getGroupId() == null
                            && inviteMessage.getFrom().equals(username)) {
                        inviteMessgeDao.deleteMessage(username);
                    }
                }
                // 自己封装的javabean
                InviteMessage msg = new InviteMessage();
                msg.setFrom(username);
                msg.setTime(System.currentTimeMillis());
                msg.setReason(reason);
                Log.d(TAG, username + "请求加你为好友,reason: " + reason);
                // 设置相应status
                msg.setStatus(InviteMessage.InviteMesageStatus.BEINVITEED);
                notifyNewIviteMessage(msg);
            }

        }

        @Override
        public void onContactAgreed(String username) {
            System.out.println("u:" + username);
            // 自己封装的javabean
            InviteMessage msg = new InviteMessage();
            List<InviteMessage> msgs = inviteMessgeDao.getMessagesList();
            for (InviteMessage inviteMessage : msgs) {
                System.out.println("inviteMessage:" + inviteMessage);
                if (inviteMessage.getFrom().equals(username)) {
                    msg = inviteMessage;
                    msg.setTime(System.currentTimeMillis());
                    msg.setStatus(InviteMessage.InviteMesageStatus.AGREED);
                    inviteMessgeDao.deleteMessage(msg.getFrom());
                    notifyNewIviteMessage(msg);
                    return;
                }

            }

        }

        @Override
        public void onContactRefused(String username) {
            // 参考同意，被邀请实现此功能,demo未实现

        }

    }

    /**
     * 刷新未读消息数
     */
    public void updateNum() {
        runOnUiThread(new Runnable() {
            public void run() {
                AppContext.count1 = getUnreadMsgCountTotal();
                AppContext.count2 = getUnreadAddressCountTotal();

                if (add == 0) {
                    MeFragment me = (MeFragment) current;
                    if (me.tvChat != null) {
                        BadgeView bv=new BadgeView(MainActivity.this,me.tvChat);
                        bv.setText(AppContext.count1+AppContext.count2+"");
                        bv.show();
                        if (AppContext.count1+AppContext.count2 == 0) {
                            bv.hide();
                        }
                    }
                }
            }
        });
    }

    /**
     * 保存提示新消息
     *
     * @param msg
     */
    private void notifyNewIviteMessage(InviteMessage msg) {
        saveInviteMsg(msg);
        // 提示有新消息
        EMNotifier.getInstance(getApplicationContext()).notifyOnNewMsg();

        updateNum();
        // TODO: 刷新好友页面ui
    }

    /**
     * 保存邀请等msg
     *
     * @param msg
     */
    private void saveInviteMsg(InviteMessage msg) {
        // 保存msg
        inviteMessgeDao.saveMessage(msg);
        // 未读数加1
        User user = AppContext.getApplication().getContactList()
                .get(AppConfig.NEW_FRIENDS_USERNAME);
        AppContext.getApplication().getContactList()
                .get(AppConfig.NEW_FRIENDS_USERNAME)
                .setUnreadMsgCount(user.getUnreadMsgCount() + 1);
    }

    /**
     * set head
     *
     * @param username
     * @return
     */
    User setUserHead(String username) {
        User user = new User();
        user.setUsername(username);
        String headerName = null;
        if (!TextUtils.isEmpty(user.getNick())) {
            headerName = user.getNick();
        } else {
            headerName = user.getUsername();
        }
        if (username.equals(AppConfig.NEW_FRIENDS_USERNAME)) {
            user.setHeader("");
        } else if (Character.isDigit(headerName.charAt(0))) {
            user.setHeader("#");
        } else {
            user.setHeader(HanziToPinyin.getInstance()
                    .get(headerName.substring(0, 1)).get(0).target.substring(0,
                            1).toUpperCase());
            char header = user.getHeader().toLowerCase().charAt(0);
            if (header < 'a' || header > 'z') {
                user.setHeader("#");
            }
        }
        return user;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (getIntent().getBooleanExtra("conflict", false)
                && !isConflictDialogShow)
            showConflictDialog();
    }

    /**
     * 获取未读申请与通知消息
     *
     * @return
     */
    public int getUnreadAddressCountTotal() {
        int unreadAddressCountTotal = 0;
        if (AppContext.getApplication().getContactList() != null
                && AppContext.getApplication().getContactList().size() > 0) {
            if (AppContext.getApplication().getContactList()
                    .get(AppConfig.NEW_FRIENDS_USERNAME) != null)
                unreadAddressCountTotal = AppContext.getApplication()
                        .getContactList().get(AppConfig.NEW_FRIENDS_USERNAME)
                        .getUnreadMsgCount();
        }
        return unreadAddressCountTotal;
    }

    /**
     * 获取未读消息数
     *
     * @return
     */
    public int getUnreadMsgCountTotal() {
        int unreadMsgCountTotal = 0;
        unreadMsgCountTotal = EMChatManager.getInstance().getUnreadMsgsCount();
        return unreadMsgCountTotal;
    }

    /**
     * 新消息广播接收者
     */
    private class NewMessageBroadcastReceiver extends BroadcastReceiver {
        @SuppressWarnings("unused")
        @Override
        public void onReceive(Context context, Intent intent) {
            // 主页面收到消息后，主要为了提示未读，实际消息内容需要到chat页面查看

            // 消息id
            String msgId = intent.getStringExtra("msgid");
            // 收到这个广播的时候，message已经在db和内存里了，可以通过id获取mesage对象
            // EMMessage message =
            // EMChatManager.getInstance().getMessage(msgId);

            // 刷新bottom bar消息未读数
            updateNum();

            // 注销广播，否则在ChatActivity中会收到这个广播
            abortBroadcast();
        }
    }

    /**
     * 消息回执BroadcastReceiver
     */
    private BroadcastReceiver ackMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String msgid = intent.getStringExtra("msgid");
            String from = intent.getStringExtra("from");
            EMConversation conversation = EMChatManager.getInstance()
                    .getConversation(from);
            if (conversation != null) {
                // 把message设为已读
                EMMessage msg = conversation.getMessage(msgid);
                if (msg != null) {
                    msg.isAcked = true;
                }
            }
            abortBroadcast();
        }
    };

    public void onResume() {
        super.onResume();
        if (getIntent().getBooleanExtra("conflict", false)) {
            showConflictDialog();
        }
        if (!isConflict) {
            updateNum();
            EMChatManager.getInstance().activityResumed();
        }

        isForeground = true;

    }

    public void onPause() {
        super.onPause();
        isForeground = false;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // 注销广播接收者
        try {
            unregisterReceiver(msgReceiver);
        } catch (Exception e) {
        }
        try {
            unregisterReceiver(ackMessageReceiver);
        } catch (Exception e) {
        }

        if (conflictBuilder != null) {
            conflictBuilder.create().dismiss();
            conflictBuilder = null;
        }

    }


}
