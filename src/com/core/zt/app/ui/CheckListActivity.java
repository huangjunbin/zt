package com.core.zt.app.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class CheckListActivity extends BaseActivity {
    protected static final int PICTURE = 0;
    protected static final int ADD = 3;
    private LinearLayout container;
    private List<String> photoUrls;
    private int type;
    private String[] items = new String[]{"从相册选择", "拍照"};
    private String filePath = "";
    private boolean isPhoto;
    private boolean isAlready;
    LayoutInflater inflater;
    private HeaderBar bar;
    private RelativeLayout rlWeek;
    private TextView tvTime;
    private Button btnSave;
    private Record record;
    private Record data;
    private  Button btnDelete;
    public CheckListActivity() {
        super(R.layout.activity_checklist);
    }

    public List<String> strList = new ArrayList<>();

    @Override
    protected void onResume() {
        super.onResume();
        isPhoto = getIntent().getBooleanExtra("isPhoto", false);
        if (isPhoto && !isAlready) {
            isAlready = true;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void initViews() {
        data = (Record) getIntent().getSerializableExtra("data");
        record = new Record();
        bar = (HeaderBar) findViewById(R.id.header);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnDelete = (Button) findViewById(R.id.btn_delete);
        tvTime= (TextView) findViewById(R.id.tv_time);
        container = (LinearLayout) findViewById(R.id.homeAdd_container);
        inflater = getLayoutInflater();


            bar.setTitle("检验单详情");
            String[] str = data.getLogVal3().split(",");
            for (int i = 0; i < str.length; i++) {
                strList.add(str[i]);

                FrameLayout con = (FrameLayout) inflater.inflate(
                        R.layout.layout_checklist, null);
                ImageView pic = (ImageView) con.findViewById(R.id.pic);
                AppContext.setImage(str[i], pic, AppContext.imageOption);
                pic.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(CheckListActivity.this,BigImageActivity.class);
                        intent.putExtra("url",data.getLogVal3());
                        startActivity(intent);
                    }
                });
                AddView(con);
            }
        Date date= DateUtil.parseDate(data.getLogTime(), "yyyy-MM-dd HH:mm");
        tvTime.setText(DateUtil.getDateNoSecond(date));
        rlWeek = (RelativeLayout) findViewById(R.id.rl_week);
        tvTime = (TextView) findViewById(R.id.tv_time);
    }

    public void initData() {
        photoUrls = new ArrayList<String>();

    }

    @Override
    public void bindViews() {
        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                RecordServer.getInstance(CheckListActivity.this).removeRecord(
                        data.getId(), new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            ;

                            @SuppressWarnings("unchecked")
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    showToast("删除成功");
                                    CheckListActivity.this.finish();
                                }
                            }
                        });
            }
        });
        btnSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("data", data);
                intent.setClass(CheckListActivity.this, NewCheckListActivity.class);
                startActivity(intent);
            }
        });

    }




    @SuppressWarnings("deprecation")
    public void AddView(View v) {
        container.addView(v, container.getChildCount() );
        ImageButton delPic = (ImageButton) (container.getChildAt(container
                .getChildCount() - 1).findViewById(R.id.del));
        delPic.setTag(container.getChildCount() - 1);
        delPic.setVisibility(View.GONE);
        container.postInvalidate(); // 提示UI重绘界面

    }

    @SuppressWarnings("deprecation")
    public void AddView(Bitmap bitmap) {

        FrameLayout con = (FrameLayout) inflater.inflate(
                R.layout.layout_checklist, null);
        ImageView pic = (ImageView) con.findViewById(R.id.pic);
        pic.setBackgroundDrawable(new BitmapDrawable(bitmap));
        container.addView(con, container.getChildCount() );
        ImageButton delPic = (ImageButton) (container.getChildAt(container
                .getChildCount() - 1).findViewById(R.id.del));
        delPic.setTag(container.getChildCount() - 1);
        delPic.setVisibility(View.VISIBLE);
        container.postInvalidate(); // 提示UI重绘界面

    }

    public void deleteView(View view) {
        int index = (Integer) view.getTag();
        container.removeView((View) view.getParent());
        container.invalidate();
        strList.remove(index);
    }

}
