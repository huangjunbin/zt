package com.core.zt.app.ui;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;

import com.core.zt.R;
import com.core.zt.app.adapter.TypeAdapter;
import com.core.zt.model.Type;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.XListView;

import java.io.Serializable;
import java.util.List;

public class TypeActivity extends BaseActivity {

    private HeaderBar headerBar;
    private XListView xlvData;
    private TypeAdapter adapter;
    public static List<Type> dataList;
    public static List<Type> olddataList;
    private String title = "";
    private int type;

    public TypeActivity() {
        super(R.layout.activity_type);

    }

    @Override
    public void initViews() {

        headerBar = (HeaderBar) findViewById(R.id.header);
        headerBar.top_right_btn.setVisibility(View.VISIBLE);
        headerBar.top_right_btn.setText("完成");
        xlvData = (XListView) findViewById(R.id.xlv_data);
        xlvData.setPullRefreshEnable(true);
        xlvData.setPullLoadEnable(true);

    }

    @Override
    public void initData() {
        dataList = (List<Type>) getIntent().getSerializableExtra("data");
        olddataList = (List<Type>) getIntent().getSerializableExtra("data");
        title = getIntent().getStringExtra("title");
        type = getIntent().getIntExtra("type", 0);
        headerBar.setTitle(title);
        adapter = new TypeAdapter(this, dataList, type);
        xlvData.setAdapter(adapter);
        xlvData.setPullRefreshEnable(false);
        xlvData.setPullLoadEnable(false);
        xlvData.setFooterDividersEnabled(false);
    }

    @Override
    public void bindViews() {
        headerBar.top_right_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("data", (Serializable) dataList);
                setResult(NewNoticeActivity.CODE, intent);
                TypeActivity.this.finish();
            }
        });
        headerBar.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("data", (Serializable) olddataList);
                intent.putExtra("type", 1);
                setResult(NewNoticeActivity.CODE, intent);
                TypeActivity.this.finish();
            }
        });
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Intent intent = new Intent();
            intent.putExtra("data", (Serializable) olddataList);
            intent.putExtra("type", 1);
            setResult(NewNoticeActivity.CODE, intent);
            TypeActivity.this.finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
