package com.core.zt.app.ui;

import android.view.View;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.widget.HeaderBar;


public class ControlSugarActivity extends BaseActivity {
    private HeaderBar headerBar;
    private TextView tvNan1,tvNan2,tvNu1,tvNu2;
    public ControlSugarActivity() {
        super(R.layout.activity_control_sugar);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        headerBar.setTitle("控糖目标");
        tvNan1= (TextView) findViewById(R.id.tv_nan1);
        tvNan2= (TextView) findViewById(R.id.tv_nan2);
        tvNu1= (TextView) findViewById(R.id.tv_nv1);
        tvNu2= (TextView) findViewById(R.id.tv_nv2);
    }

    @Override
    public void initData() {
    }

    @Override
    public void bindViews() {
        if(AppContext.currentUser.getSex()==1){
            tvNan1.setVisibility(View.VISIBLE);
            tvNan2.setVisibility(View.VISIBLE);
        }else{
            tvNu1.setVisibility(View.VISIBLE);
            tvNu2.setVisibility(View.VISIBLE);
        }
    }

}
