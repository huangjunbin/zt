package com.core.zt.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.ResponeModel;
import com.core.zt.model.User;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.UserServer;
import com.core.zt.util.FileUtils;
import com.core.zt.util.MediaUtil;
import com.core.zt.widget.CircleImageView;
import com.core.zt.widget.HeaderBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;

public class UserInfoActivity extends BaseActivity {

    private HeaderBar bar;
    private static final int requestCode_updateAddress = 100;
    private TextView tvNickName, tvName, tvPhone, tvSex, tvDate, tvSg, tvTz, tvMaxTz, tvArea, tvQd;
    private RelativeLayout rlNickName, rlName, rlPhone, rlSex, rlDate, rlSg, rlTz, rlMaxTz, rlArea, rlPhoto, rlQd;
    private Intent intent;
    public static String appPath = "/zt/";
    private String userPhotoName = "img.jpg";
    private CircleImageView ivPhoto;

    public UserInfoActivity() {
        super(R.layout.activity_user_info);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);
        tvNickName = (TextView) findViewById(R.id.tv_nickname);
        tvName = (TextView) findViewById(R.id.tv_name);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
        tvSex = (TextView) findViewById(R.id.tv_sex);
        tvDate = (TextView) findViewById(R.id.tv_date);
        tvSg = (TextView) findViewById(R.id.tv_sg);
        tvTz = (TextView) findViewById(R.id.tv_tz);
        tvMaxTz = (TextView) findViewById(R.id.tv_maxtz);
        tvArea = (TextView) findViewById(R.id.tv_area);
        tvQd = (TextView) findViewById(R.id.tv_qd);

        rlNickName = (RelativeLayout) findViewById(R.id.rl_nickname);
        rlName = (RelativeLayout) findViewById(R.id.rl_name);
        rlPhone = (RelativeLayout) findViewById(R.id.rl_phone);
        rlSex = (RelativeLayout) findViewById(R.id.rl_sex);
        rlDate = (RelativeLayout) findViewById(R.id.rl_date);
        rlSg = (RelativeLayout) findViewById(R.id.rl_sg);
        rlTz = (RelativeLayout) findViewById(R.id.rl_tz);
        rlMaxTz = (RelativeLayout) findViewById(R.id.rl_maxtz);
        rlArea = (RelativeLayout) findViewById(R.id.rl_area);
        rlPhoto = (RelativeLayout) findViewById(R.id.rl_photo);
        rlQd = (RelativeLayout) findViewById(R.id.rl_qd);
        ivPhoto = (CircleImageView) findViewById(R.id.iv_photo);
    }

    @Override
    public void initData() {
        bar.setTitle("个人信息");

    }

    @Override
    protected void onResume() {
        tvNickName.setText(AppContext.currentUser.getNickName());
        tvName.setText(AppContext.currentUser.getUserName());
        tvPhone.setText(AppContext.currentUser.getMobile());
        tvSex.setText(AppContext.currentUser.getSex() == 1 ? "男" : "女");
        tvDate.setText(AppContext.currentUser.getBirthday());
        tvSg.setText(AppContext.currentUser.getStature() + "cm");
        tvTz.setText(AppContext.currentUser.getCurrentWeight() + "kg");
        tvMaxTz.setText(AppContext.currentUser.getMaxWeigth() + "kg");
        tvQd.setText(AppContext.currentUser.getLabourIntensity() + "");
        tvArea.setText(AppContext.currentUser.getArea());
        super.onResume();
    }

    @Override
    public void bindViews() {
        AppContext.setImage(AppContext.currentUser.getPhoto(), ivPhoto, AppContext.memberPhotoOption);
        tvNickName.setText(AppContext.currentUser.getNickName());
        tvName.setText(AppContext.currentUser.getUserName());
        tvPhone.setText(AppContext.currentUser.getMobile());
        tvSex.setText(AppContext.currentUser.getSex() == 1 ? "男" : "女");
        tvDate.setText(AppContext.currentUser.getBirthday());
        tvSg.setText(AppContext.currentUser.getStature() + "cm");
        tvTz.setText(AppContext.currentUser.getCurrentWeight() + "kg");
        tvMaxTz.setText(AppContext.currentUser.getMaxWeigth() + "kg");
        tvArea.setText(AppContext.currentUser.getArea());
        rlNickName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserInfoActivity.this, EditActivity.class);
                intent.putExtra("title", "修改昵称");
                intent.putExtra("type", 1);
                startActivity(intent);
            }
        });
        rlName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserInfoActivity.this, EditActivity.class);
                intent.putExtra("title", "修改姓名");
                intent.putExtra("type", 2);
                startActivity(intent);
            }
        });
        tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(UserInfoActivity.this, EditActivity.class);
                intent.putExtra("title", "修改电话");
                intent.putExtra("type", 3);
                startActivity(intent);
            }
        });
        rlSg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                intent = new Intent(UserInfoActivity.this, EditActivity.class);
//                intent.putExtra("title", "修改身高");
//                intent.putExtra("type", 4);
//                startActivity(intent);
                final CharSequence[] item = {"140", "141", "142", "143", "144",
                        "145", "146", "147", "148", "149", "150", "151", "152", "153", "154",
                        "155", "156", "157", "158", "159", "160", "161", "162", "163", "164",
                        "165", "166", "167", "168", "169", "170", "171", "172", "173", "174",
                        "175", "176", "177", "178", "179", "180", "181", "182", "183", "184",
                        "185", "186", "187", "188", "189", "190", "191", "192", "193", "194",
                        "195", "196", "197", "198", "199"};
                new AlertDialog.Builder(UserInfoActivity.this)
                        .setTitle("选择身高")
                        .setItems(item, new DialogInterface.OnClickListener() { // content
                            @Override
                            public void onClick(DialogInterface dialog,
                                                final int which) {
                                final User user = AppContext.currentUser;
                                user.setStature(Integer.parseInt(item[which].toString()));
                                UserServer.getInstance(context).updateInfo(
                                        user, new CustomAsyncResponehandler() {
                                            public void onFinish() {

                                            }

                                            ;

                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onSuccess(ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel != null && baseModel.isStatus()) {
                                                    AppContext.currentUser = user;
                                                    UserServer.getInstance(context).updateUser(user);
                                                    tvSg.setText(item[which] + "cm");
                                                }
                                            }
                                        });

                            }

                        })
                        .setNegativeButton("取消",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss(); // 关闭alertDialog
                                    }
                                }).show();
            }
        });
        rlTz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                intent = new Intent(UserInfoActivity.this, EditActivity.class);
//                intent.putExtra("title", "修改体重");
//                intent.putExtra("type", 5);
//                startActivity(intent);
                final CharSequence[] item = {"40", "41", "42", "43", "44",
                        "45", "46", "47", "48", "49", "50", "51", "52", "53", "54",
                        "55", "56", "57", "58", "59", "60", "61", "62", "63", "64",
                        "65", "66", "67", "68", "69", "70", "71", "72", "73", "74",
                        "75", "76", "77", "78", "79", "80", "81", "82", "83", "84",
                        "85", "86", "87", "88", "89", "90", "91", "92", "93", "94",
                        "95", "96", "97", "98", "99", "100"};
                new AlertDialog.Builder(UserInfoActivity.this)
                        .setTitle("选择当前体重")
                        .setItems(item, new DialogInterface.OnClickListener() { // content
                            @Override
                            public void onClick(DialogInterface dialog,
                                                final int which) {
                                final User user = AppContext.currentUser;
                                user.setCurrentWeight(Integer.parseInt(item[which].toString()));
                                UserServer.getInstance(context).updateInfo(
                                        user, new CustomAsyncResponehandler() {
                                            public void onFinish() {

                                            }

                                            ;

                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onSuccess(ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel != null && baseModel.isStatus()) {
                                                    AppContext.currentUser = user;
                                                    UserServer.getInstance(context).updateUser(user);
                                                    tvTz.setText(item[which] + "kg");
                                                }
                                            }
                                        });

                            }

                        })
                        .setNegativeButton("取消",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss(); // 关闭alertDialog
                                    }
                                }).show();
            }
        });
        rlMaxTz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                intent = new Intent(UserInfoActivity.this, EditActivity.class);
//                intent.putExtra("title", "修改最大体重");
//                intent.putExtra("type", 6);
//                startActivity(intent);
                final CharSequence[] item = {"40", "41", "42", "43", "44",
                        "45", "46", "47", "48", "49", "50", "51", "52", "53", "54",
                        "55", "56", "57", "58", "59", "60", "61", "62", "63", "64",
                        "65", "66", "67", "68", "69", "70", "71", "72", "73", "74",
                        "75", "76", "77", "78", "79", "80", "81", "82", "83", "84",
                        "85", "86", "87", "88", "89", "90", "91", "92", "93", "94",
                        "95", "96", "97", "98", "99", "100"};
                new AlertDialog.Builder(UserInfoActivity.this)
                        .setTitle("选择最大体重")
                        .setItems(item, new DialogInterface.OnClickListener() { // content
                            @Override
                            public void onClick(DialogInterface dialog,
                                                final int which) {
                                final User user = AppContext.currentUser;
                                user.setMaxWeigth(Integer.parseInt(item[which].toString()));
                                UserServer.getInstance(context).updateInfo(
                                        user, new CustomAsyncResponehandler() {
                                            public void onFinish() {

                                            }

                                            ;

                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onSuccess(ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel != null && baseModel.isStatus()) {
                                                    AppContext.currentUser = user;
                                                    UserServer.getInstance(context).updateUser(user);
                                                    tvMaxTz.setText(item[which] + "kg");
                                                }
                                            }
                                        });

                            }

                        })
                        .setNegativeButton("取消",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss(); // 关闭alertDialog
                                    }
                                }).show();
            }
        });
        rlArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                intent = new Intent(UserInfoActivity.this, EditActivity.class);
//                intent.putExtra("title", "修改区域");
//                intent.putExtra("type", 7);
//                startActivity(intent);
                // 点击地址更新
                Intent intent_address = new Intent(UserInfoActivity.this,
                        AddressActivity.class);
                // this.startActivity(intent_address);
                intent_address.putExtra("address", tvArea.getText().toString());
                startActivityForResult(intent_address, requestCode_updateAddress);
            }
        });
        rlQd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] item = {"轻体力劳动", "中体力劳动", "重体力劳动"};
                new AlertDialog.Builder(UserInfoActivity.this)
                        .setTitle("选择劳动强度")
                        .setItems(item, new DialogInterface.OnClickListener() { // content
                            @Override
                            public void onClick(DialogInterface dialog,
                                                final int which) {
                                final User user = AppContext.currentUser;
                                user.setLabourIntensity(item[which].toString());
                                UserServer.getInstance(context).updateInfo(
                                        user, new CustomAsyncResponehandler() {
                                            public void onFinish() {

                                            }

                                            ;

                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onSuccess(ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel != null && baseModel.isStatus()) {
                                                    AppContext.currentUser = user;
                                                    UserServer.getInstance(context).updateUser(user);
                                                    tvQd.setText(item[which]);
                                                }
                                            }
                                        });

                            }

                        })
                        .setNegativeButton("取消",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss(); // 关闭alertDialog
                                    }
                                }).show();
            }
        });
        rlDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        UserInfoActivity.this);
                View v = getLayoutInflater().inflate(
                        R.layout.date_time_dialog, null);
                final DatePicker datePicker = (DatePicker) v
                        .findViewById(R.id.date_picker);
                final TimePicker timePicker = (android.widget.TimePicker) v
                        .findViewById(R.id.time_picker);
                timePicker.setVisibility(View.GONE);
                datePicker.setVisibility(View.VISIBLE);
                builder.setView(v);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                datePicker.init(cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH), null);

                timePicker.setIs24HourView(true);
                timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(Calendar.MINUTE);
                builder.setTitle("请选择出生日期");
                builder.setPositiveButton("确  定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                StringBuffer sb = new StringBuffer();
                                sb.append(String.format("%d-%02d-%02d",
                                        datePicker.getYear(),
                                        datePicker.getMonth() + 1,
                                        datePicker.getDayOfMonth()));
                                sb.append("  ");
                             /*   sb.append(timePicker.getCurrentHour())
                                        .append(":")
                                        .append(timePicker.getCurrentMinute());*/
                                final User user = AppContext.currentUser;
                                user.setBirthday(sb.toString());
                                UserServer.getInstance(context).updateInfo(
                                        user, new CustomAsyncResponehandler() {
                                            public void onFinish() {

                                            }

                                            ;

                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onSuccess(ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel != null && baseModel.isStatus()) {
                                                    AppContext.currentUser = user;
                                                    UserServer.getInstance(context).updateUser(user);
                                                    tvDate.setText(AppContext.currentUser.getBirthday());
                                                }
                                            }
                                        });
                                dialog.cancel();
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });

        rlSex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(UserInfoActivity.this)
                        .setTitle("请选择性别")
                        .setSingleChoiceItems(new String[]{"女", "男"}, 0,
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        final User user = AppContext.currentUser;
                                        user.setSex(which);
                                        UserServer.getInstance(context).updateInfo(
                                                user, new CustomAsyncResponehandler() {
                                                    public void onFinish() {

                                                    }

                                                    ;

                                                    @SuppressWarnings("unchecked")
                                                    @Override
                                                    public void onSuccess(ResponeModel baseModel) {
                                                        super.onSuccess(baseModel);
                                                        if (baseModel != null && baseModel.isStatus()) {
                                                            AppContext.currentUser = user;
                                                            UserServer.getInstance(context).updateUser(user);
                                                            tvSex.setText(AppContext.currentUser.getSex() == 1 ? "男" : "女");
                                                        }
                                                    }
                                                });
                                        dialog.dismiss();
                                    }
                                }
                        )
                        .show();
            }
        });

        rlPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPhotoDialog(new OnSurePress() {
                    @Override
                    public void onClick(View view) {
                        if (FileUtils.isSDCardExisd()) {
                            MediaUtil.searhcAlbum(UserInfoActivity.this,
                                    MediaUtil.ALBUM);
                        } else {
                            showToast("SDCARD不存在！");
                        }
                    }
                }, new OnSurePress() {
                    @Override
                    public void onClick(View view) {
                        if (FileUtils.isSDCardExisd()) {
                            MediaUtil.takePhoto(UserInfoActivity.this,
                                    MediaUtil.PHOTO, appPath, userPhotoName);
                        } else {
                            showToast("SDCARD不存在！");
                        }
                    }


                });
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case MediaUtil.ALBUM:
                if (data != null || resultCode == Activity.RESULT_OK) {
                    startPhotoZoom(data.getData());
                }
                break;
            case MediaUtil.PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    File temp = new File(MediaUtil.ROOTPATH + appPath
                            + userPhotoName);
                    startPhotoZoom(Uri.fromFile(temp));
                }
                break;
            case MediaUtil.ZOOMPHOTO:
                if (data != null || resultCode == Activity.RESULT_OK) {
                    setPicToView(data);
                }
                break;
            // 地址修改
            case requestCode_updateAddress:
                switch (resultCode) {
                    case RESULT_OK:
                        // 修改用户邮箱成功
                       final String address = data.getStringExtra("address");
                        if (address != null && !"".equals(address)) {

                            final User user = AppContext.currentUser;
                            user.setArea(address);
                            UserServer.getInstance(context).updateInfo(
                                    user, new CustomAsyncResponehandler() {
                                        public void onFinish() {

                                        }

                                        ;

                                        @SuppressWarnings("unchecked")
                                        @Override
                                        public void onSuccess(ResponeModel baseModel) {
                                            super.onSuccess(baseModel);
                                            if (baseModel != null && baseModel.isStatus()) {
                                                AppContext.currentUser = user;
                                                UserServer.getInstance(context).updateUser(user);
                                                tvArea.setText(address);
                                            }
                                        }
                                    });
                        }
                }
                break;
        }
    }

    private void setPicToView(Intent data) {
        Bundle extras = data.getExtras();
        if (extras != null) {
            final Bitmap img = extras.getParcelable("data");
            ivPhoto.setImageBitmap(img);
            UserServer.getInstance(context).uploadImg(
                    new File(MediaUtil.ROOTPATH + appPath + userPhotoName),
                    new CustomAsyncResponehandler() {
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel.isStatus()) {
                                String url = "";
                                try {
                                    JSONObject object = new JSONObject(baseModel.getResult());
                                    url = object.getString("url");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Log.d("url:", url);

                                final User user = AppContext.currentUser;
                                user.setPhoto(url);
                                UserServer.getInstance(context).updateInfo(
                                        user, new CustomAsyncResponehandler() {
                                            public void onFinish() {

                                            }

                                            ;

                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onSuccess(ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel != null && baseModel.isStatus()) {
                                                    AppContext.currentUser = user;
                                                    UserServer.getInstance(context).updateUser(user);
                                                    ivPhoto.setImageBitmap(img);
                                                }
                                            }
                                        });
                            }
                        }
                    });
        }
    }

    /**
     * 裁剪图片方法实现
     *
     * @param uri
     */
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        File dir = new File((MediaUtil.ROOTPATH + appPath));
        if (!dir.exists())
            dir.mkdirs();

        File saveFile = new File(MediaUtil.ROOTPATH + appPath + userPhotoName);

        intent.putExtra("output", Uri.fromFile(saveFile)); // 传入目标文件
        intent.putExtra("outputFormat", "JPEG"); // 输入文件格式
        Intent it = Intent.createChooser(intent,
                "剪裁图片");
        startActivityForResult(it, MediaUtil.ZOOMPHOTO);
    }

}
