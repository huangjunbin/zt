package com.core.zt.app.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.core.zt.R;
import com.core.zt.app.AppConfig;
import com.core.zt.app.AppContext;
import com.core.zt.app.AppManager;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.service.UserServer;
import com.core.zt.util.DateUtil;
import com.core.zt.util.Utils;
import com.core.zt.widget.HeaderBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class NewCheckListActivity extends BaseActivity {
    protected static final int PICTURE = 0;
    protected static final int ADD = 3;
    private LinearLayout container;
    private List<String> photoUrls;
    private int type;
    private String[] items = new String[]{"从相册选择", "拍照"};
    private String filePath = "";
    private boolean isPhoto;
    private boolean isAlready;
    LayoutInflater inflater;
    private HeaderBar bar;
    private RelativeLayout rlWeek;
    private TextView tvTime;
    private Button btnSave;
    private Record record;
    private Record data;

    public NewCheckListActivity() {
        super(R.layout.activity_new_checklist);
    }

    public List<String> strList = new ArrayList<>();

    @Override
    protected void onResume() {
        super.onResume();
        isPhoto = getIntent().getBooleanExtra("isPhoto", false);
        if (isPhoto && !isAlready) {
            isAlready = true;
            openCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void initViews() {
        data = (Record) getIntent().getSerializableExtra("data");
        record = new Record();
        bar = (HeaderBar) findViewById(R.id.header);
        btnSave = (Button) findViewById(R.id.btn_save);
        tvTime= (TextView) findViewById(R.id.tv_time);
        container = (LinearLayout) findViewById(R.id.homeAdd_container);
        inflater = getLayoutInflater();
        FrameLayout fl = (FrameLayout) inflater.inflate(
                R.layout.layout_checklist, null);

        fl.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (container.getChildCount() - 1 < 9) {
                    showDialog();
                } else {
                    showToast("选择图片不能超过9张");
                }
            }
        });
        container.addView(fl);
        if (data == null) {
            bar.setTitle("新增检验单");
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            StringBuffer sb = new StringBuffer();
            sb.append(String.format("%d-%02d-%02d",
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH) + 1,
                    cal.get(Calendar.DAY_OF_MONTH)));
            sb.append("  ");
            sb.append(DateUtil.getZero(cal.get(Calendar.HOUR_OF_DAY)))
                    .append(":")
                    .append(DateUtil.getZero(cal.get(Calendar.MINUTE)));
            tvTime.setText(sb);
        } else {
            bar.setTitle("编辑检验单");
            String[] str = data.getLogVal3().split(",");
            for (int i = 0; i < str.length; i++) {
                strList.add(str[i]);

                FrameLayout con = (FrameLayout) inflater.inflate(
                        R.layout.layout_checklist, null);
                ImageView pic = (ImageView) con.findViewById(R.id.pic);
                AppContext.setImage(str[i], pic, AppContext.imageOption);
                AddView(con);
            }
            Date date= DateUtil.parseDate(data.getLogTime(), "yyyy-MM-dd HH:mm");
            tvTime.setText(DateUtil.getDateNoSecond(date));
        }
        rlWeek = (RelativeLayout) findViewById(R.id.rl_week);
        tvTime = (TextView) findViewById(R.id.tv_time);
    }

    public void initData() {
        photoUrls = new ArrayList<String>();

    }

    @Override
    public void bindViews() {
        btnSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(strList);

                if (strList.size() < 1) {
                    showToast("请选择化验单");
                    return;
                }
                if ("".equals(tvTime.getText().toString())) {
                    showToast("请选择时间");
                }

                String str = "";
                for (String s : strList) {
                    str += s + ",";
                }
                record.setLogTime(tvTime.getText().toString());
                record.setLogVal3(str);
                record.setLogType("4");
                if(data!=null){
                    record.setId(data.getId());
                }
                RecordServer.getInstance(NewCheckListActivity.this).saveRecord(
                        record, new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            ;

                            @SuppressWarnings("unchecked")
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {

                                    if (data != null) {
                                        showToast("编辑成功");
                                        AppManager.getAppManager().finishActivity(CheckListActivity.class);
                                        NewCheckListActivity.this.finish();
//                                        Intent intent=new Intent();
//                                        intent.putExtra("data", record);
//                                        intent.setClass(NewCheckListActivity.this, CheckListActivity.class);
//                                        startActivity(intent);
                                    } else {
                                        showToast("添加成功");
                                    }
                                    NewCheckListActivity.this.finish();
                                }
                            }
                        });
            }
        });
        rlWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        NewCheckListActivity.this);
                View v = getLayoutInflater().inflate(
                        R.layout.date_time_dialog, null);
                final DatePicker datePicker = (DatePicker) v
                        .findViewById(R.id.date_picker);
                final TimePicker timePicker = (android.widget.TimePicker) v
                        .findViewById(R.id.time_picker);
                timePicker.setVisibility(View.VISIBLE);
                datePicker.setVisibility(View.VISIBLE);
                builder.setView(v);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                datePicker.init(cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH), null);

                timePicker.setIs24HourView(true);
                timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
                builder.setTitle("选取起始时间");
                builder.setPositiveButton("确  定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                StringBuffer sb = new StringBuffer();
                                sb.append(String.format("%d-%02d-%02d",
                                        datePicker.getYear(),
                                        datePicker.getMonth() + 1,
                                        datePicker.getDayOfMonth()));
                                sb.append("  ");
                                sb.append(DateUtil.getZero(timePicker.getCurrentHour()))
                                        .append(":")
                                        .append(DateUtil.getZero(timePicker.getCurrentMinute()));
                                tvTime.setText(sb);
                                dialog.cancel();
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    /**
     * 显示选择对话框
     */
    private void showDialog() {
        new AlertDialog.Builder(this).setTitle("选择图片")
                .setItems(items, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(intent, ADD);
                                break;
                            case 1:
                                openCamera();
                                break;
                        }
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    /**
     * 根据日历获取文件时间戳名称
     */
    @SuppressLint("DefaultLocale")
    private String getFileStamp() {

        Calendar calendar = Calendar.getInstance();
        int Y = calendar.get(Calendar.YEAR);
        int M = calendar.get(Calendar.MONTH) + 1;
        int D = calendar.get(Calendar.DAY_OF_MONTH);
        int h = calendar.get(Calendar.HOUR_OF_DAY);
        int m = calendar.get(Calendar.MINUTE);
        int s = calendar.get(Calendar.SECOND);
        Object[] args = new Object[]{Y, M, D, h, m, s};
        String stamp = String.format("%04d%02d%02d%02d%02d%02d", args);
        return stamp;
    }

    /**
     * 打开摄像头
     */
    private void openCamera() {
        Intent localIntent = new Intent();
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, "SD卡当前不可用，无法拍照！", Toast.LENGTH_SHORT).show();
            return;
        }
        filePath = AppConfig.DEFAULT_SAVE_PATH + getFileStamp() + ".jpg";
        localIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        localIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(filePath)));
        startActivityForResult(localIntent, PICTURE);


    }

    @SuppressWarnings({"unused", "deprecation"})
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == PICTURE) {
            if (filePath != null && filePath.length() > 0) {
                int width = (int) getResources().getDimension(R.dimen.size_100);
                final Bitmap bitmap = Utils.copressImage(filePath, width, width);
                filePath = AppConfig.DEFAULT_SAVE_PATH + getFileStamp() + ".jpg";
                Utils.saveBitmap(filePath, bitmap);

                //TODO 上传图片
                UserServer.getInstance(context).uploadImg(
                        new File(filePath),
                        new CustomAsyncResponehandler() {
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel.isStatus()) {
                                    String url = "";
                                    try {
                                        JSONObject object = new JSONObject(baseModel.getResult());
                                        url = object.getString("url");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    strList.add(url);
                                    AddView(bitmap);
                                }
                            }
                        });

            }
        } else if (requestCode == ADD) {
            Uri uri = data.getData();
            try {

                Uri contentUri = data.getData();
                if(DocumentsContract.isDocumentUri(context, contentUri)){
                    String wholeID = DocumentsContract.getDocumentId(contentUri);
                    String id = wholeID.split(":")[1];
                    String[] column = { MediaStore.Images.Media.DATA };
                    String sel = MediaStore.Images.Media._ID +"=?";
                    Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                            sel, new String[] { id }, null);
                    int columnIndex = cursor.getColumnIndex(column[0]);
                    if (cursor.moveToFirst()) {
                        filePath = cursor.getString(columnIndex);
                    }
                    cursor.close();
                }else{
                    String[] projection = { MediaStore.Images.Media.DATA };
                    Cursor cursor = context.getContentResolver().query(contentUri, projection, null, null, null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    filePath = cursor.getString(column_index);
                }
                int width = (int) getResources().getDimension(R.dimen.size_100);
                final Bitmap bitmap = Utils.copressImage(filePath, width, width);
                filePath = AppConfig.DEFAULT_SAVE_PATH + getFileStamp() + ".jpg";
                Utils.saveBitmap(filePath, bitmap);
                //TODO 上传图片
                UserServer.getInstance(context).uploadImg(
                        new File(filePath),
                        new CustomAsyncResponehandler() {
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel.isStatus()) {
                                    String url = "";
                                    try {
                                        JSONObject object = new JSONObject(baseModel.getResult());
                                        url = object.getString("url");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    strList.add(url);
                                    AddView(bitmap);
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("deprecation")
    public void AddView(View v) {
        container.addView(v, container.getChildCount() - 1);
        ImageButton delPic = (ImageButton) (container.getChildAt(container
                .getChildCount() - 2).findViewById(R.id.del));
        delPic.setTag(container.getChildCount() - 2);
        delPic.setVisibility(View.VISIBLE);
        container.postInvalidate(); // 提示UI重绘界面

    }

    @SuppressWarnings("deprecation")
    public void AddView(Bitmap bitmap) {

        FrameLayout con = (FrameLayout) inflater.inflate(
                R.layout.layout_checklist, null);
        ImageView pic = (ImageView) con.findViewById(R.id.pic);
        pic.setBackgroundDrawable(new BitmapDrawable(bitmap));
        container.addView(con, container.getChildCount() - 1);
        ImageButton delPic = (ImageButton) (container.getChildAt(container
                .getChildCount() - 2).findViewById(R.id.del));
        delPic.setTag(container.getChildCount() - 2);
        delPic.setVisibility(View.VISIBLE);
        container.postInvalidate(); // 提示UI重绘界面

    }

    public void deleteView(View view) {
        int index = (Integer) view.getTag();
        container.removeView((View) view.getParent());
        container.invalidate();
        strList.remove(index);
    }

}
