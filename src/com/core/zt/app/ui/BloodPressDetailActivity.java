package com.core.zt.app.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.DrugServer;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;

import java.util.Date;

public class BloodPressDetailActivity extends BaseActivity {

    private HeaderBar bar;
    private Record data;
    private TextView tvTime,tvValue1,tvValue2;
    private Button btnEdit,btnRemove;
    private Intent intent;
    public BloodPressDetailActivity() {
        super(R.layout.activity_bloodpress_detail);

    }

    @Override
    public void initViews() {
        bar = (HeaderBar) findViewById(R.id.header);
        tvTime= (TextView) findViewById(R.id.tv_time);
        tvValue1= (TextView) findViewById(R.id.tv_value1);
        tvValue2= (TextView) findViewById(R.id.tv_value2);
        btnEdit= (Button) findViewById(R.id.btn_save);
        btnRemove= (Button) findViewById(R.id.btn_delete);

    }

    @Override
    public void initData() {
        data= (Record) getIntent().getSerializableExtra("data");
        bar.setTitle("血压");
    }

    @Override
    public void bindViews() {
        tvValue1.setText(data.getLogVal1());
        tvValue2.setText(data.getLogVal2());
        Date date= DateUtil.parseDate(data.getLogTime(), "yyyy-MM-dd HH:mm");
        tvTime.setText(DateUtil.getDateNoSecond(date));
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent();
                intent.putExtra("data", data);
                intent.setClass(BloodPressDetailActivity.this, NewBloodPressActivity.class);
                startActivity(intent);
            }
        });
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showSureDialog(BloodPressDetailActivity.this, "确定删除?", "确定", new OnSurePress() {
                    @Override
                    public void onClick(View view) {
                        RecordServer.getInstance(BloodPressDetailActivity.this).removeRecord(
                                data.getId(), new CustomAsyncResponehandler() {
                                    public void onFinish() {

                                    }

                                    ;

                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {
                                            showToast("删除成功");
                                            BloodPressDetailActivity.this.finish();
                                        }
                                    }
                                });
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {

                    }
                });

            }
        });
    }




}
