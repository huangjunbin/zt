package com.core.zt.app.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.core.zt.R;
import com.core.zt.app.AppManager;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.TuneWheel;

import java.util.Calendar;
import java.util.Date;

public class NewBloodPressActivity extends BaseActivity {

    private HeaderBar bar;
    private RelativeLayout rlWeek;
    private TextView tvTime;
    private Record record;
    private Button btnSave;
    private EditText etH,etL;
    private int index=0;
    private Record data;
    private TuneWheel tw;
    private TuneWheel tw1;
    public NewBloodPressActivity() {
        super(R.layout.activity_new_bloodpress);

    }

    @Override
    public void initViews() {
        tw= (TuneWheel) findViewById(R.id.tw_value);
        tw1= (TuneWheel) findViewById(R.id.tw_value1);
        data = (Record) getIntent().getSerializableExtra("data");
        bar = (HeaderBar) findViewById(R.id.header);
        rlWeek = (RelativeLayout) findViewById(R.id.rl_week);
        tvTime = (TextView) findViewById(R.id.tv_time);
        etH = (EditText) findViewById(R.id.et_h);
        etL = (EditText) findViewById(R.id.et_l);
        btnSave = (Button) findViewById(R.id.btn_save);
    }

    @Override
    public void initData() {

    }

    @Override
    public void bindViews() {
        record=new Record();
        tw.setValueChangeListener(new TuneWheel.OnValueChangeListener() {
            @Override
            public void onValueChange(float value) {
                etH.setText((int)value+"");
            }
        });
        tw1.setValueChangeListener(new TuneWheel.OnValueChangeListener() {
            @Override
            public void onValueChange(float value) {
                etL.setText((int)value+"");
            }
        });
        if (data != null) {
            record.setId(data.getId());
            Date date= DateUtil.parseDate(data.getLogTime(), "yyyy-MM-dd HH:mm");
            tvTime.setText(DateUtil.getDateNoSecond(date));
            etH.setText(data.getLogVal1());
            etL.setText(data.getLogVal2());
            bar.setTitle("编辑血压记录");
            tw.initViewParam((int) (Float.parseFloat(data.getLogVal1()) ), 300, TuneWheel.MOD_TYPE_ONE,false);
            tw1.initViewParam((int) (Float.parseFloat(data.getLogVal2())), 300, TuneWheel.MOD_TYPE_ONE,false);
        }else{
            bar.setTitle("新增血压记录");
            tw.initViewParam(120, 300, TuneWheel.MOD_TYPE_ONE,false);
            tw1.initViewParam(80, 160, TuneWheel.MOD_TYPE_ONE,false);
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            StringBuffer sb = new StringBuffer();
            sb.append(String.format("%d-%02d-%02d",
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH) + 1,
                    cal.get(Calendar.DAY_OF_MONTH)));
            sb.append("  ");
            sb.append(DateUtil.getZero(cal.get(Calendar.HOUR_OF_DAY)))
                    .append(":")
                    .append(DateUtil.getZero(cal.get(Calendar.MINUTE)));
            tvTime.setText(sb);
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(etH.getText().toString())) {
                    showToast("请输入收缩压");
                    return;
                }
                if ("".equals(etL.getText().toString())) {
                    showToast("请输入舒张压");
                    return;
                }
                if (Float.parseFloat(etH.getText().toString())<=Float.parseFloat(etL.getText().toString())) {
                    showToast("收缩压一定要大于舒张压");
                    return;
                }
                if ("".equals(tvTime.getText().toString())) {
                    showToast("请选择时间");
                    return;
                }

                record.setLogTime(tvTime.getText().toString());
                record.setLogVal1(etH.getText().toString());
                record.setLogVal2(etL.getText().toString());
                record.setLogType("2");
                RecordServer.getInstance(NewBloodPressActivity.this).saveRecord(
                        record, new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            ;

                            @SuppressWarnings("unchecked")
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    if (data != null) {
                                        showToast("编辑成功");
                                        AppManager.getAppManager().finishActivity(BloodPressDetailActivity.class);
                                        NewBloodPressActivity.this.finish();
//                                        Intent intent=new Intent();
//                                        intent.putExtra("data", record);
//                                        intent.setClass(NewBloodPressActivity.this, BloodPressDetailActivity.class);
//                                        startActivity(intent);
                                    }else{
                                        showToast("添加成功");
                                    }
                                    NewBloodPressActivity.this.finish();
                                }
                            }
                        });
            }
        });
        rlWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        NewBloodPressActivity.this);
                View v = getLayoutInflater().inflate(
                        R.layout.date_time_dialog, null);
                final DatePicker datePicker = (DatePicker) v
                        .findViewById(R.id.date_picker);
                final TimePicker timePicker = (android.widget.TimePicker) v
                        .findViewById(R.id.time_picker);
                timePicker.setVisibility(View.VISIBLE);
                datePicker.setVisibility(View.VISIBLE);
                builder.setView(v);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                datePicker.init(cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH), null);

                timePicker.setIs24HourView(true);
                timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
                builder.setTitle("选取起始时间");
                builder.setPositiveButton("确  定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                StringBuffer sb = new StringBuffer();
                                sb.append(String.format("%d-%02d-%02d",
                                        datePicker.getYear(),
                                        datePicker.getMonth() + 1,
                                        datePicker.getDayOfMonth()));
                                sb.append("  ");
                                sb.append(DateUtil.getZero(timePicker.getCurrentHour()))
                                        .append(":")
                                        .append(DateUtil.getZero(timePicker.getCurrentMinute()));
                                tvTime.setText(sb);
                                dialog.cancel();
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });
    }


}
