package com.core.zt.app.ui;

import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.model.ResponeModel;
import com.core.zt.model.User;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.UserServer;
import com.core.zt.util.StringUtils;
import com.core.zt.util.UIHelper;
import com.core.zt.widget.HeaderBar;


public class EditActivity extends BaseActivity {
	private HeaderBar headerBar;
	private EditText editTextContent;
	private int type;
	private String title;
	public EditActivity() {
		super(R.layout.activity_edit);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.header);
		title=getIntent().getStringExtra("title");
		type=getIntent().getIntExtra("type",0);
		headerBar.setTitle(title);
		headerBar.top_right_btn.setText("修改");
		editTextContent = (EditText) findViewById(R.id.feedback_content);

	}

	@Override
	public void initData() {

	}
	
	@Override
	public void bindViews() {
		if(type==3){
			editTextContent.setInputType(InputType.TYPE_CLASS_NUMBER);
		}
		if(type==4){
			editTextContent.setInputType(InputType.TYPE_CLASS_NUMBER);
		}
		if(type==5){
			editTextContent.setInputType(InputType.TYPE_CLASS_NUMBER);
		}
		if(type==6){
			editTextContent.setInputType(InputType.TYPE_CLASS_NUMBER);
		}
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (checkInput()) {
					final User user= AppContext.currentUser;
					if(type==1){
						user.setNickName(editTextContent.getText().toString());
					}
					if(type==2){
						user.setRealName(editTextContent.getText().toString());
					}
					if(type==3){
						user.setMobile(editTextContent.getText().toString());
					}
					if(type==4){
						user.setStature(Integer.parseInt(editTextContent.getText().toString()));
					}
					if(type==5){
						user.setCurrentWeight(Integer.parseInt(editTextContent.getText().toString()));
					}
					if(type==6){
						user.setMaxWeigth(Integer.parseInt(editTextContent.getText().toString()));
					}
					if(type==7){
						user.setArea(editTextContent.getText().toString());
					}
					UserServer.getInstance(context).updateInfo(
							user, new CustomAsyncResponehandler() {
								public void onFinish() {

								};
								@SuppressWarnings("unchecked")
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel != null && baseModel.isStatus()) {
										AppContext.currentUser=user;
										UserServer.getInstance(context).updateUser(user);
										EditActivity.this.finish();
									}
								}
							});
				}
			}
		});
	}

	private boolean checkInput() {
		if (StringUtils.isEmpty(editTextContent.getText().toString())) {
			UIHelper.ShowMessage(context, "请先输入修改内容！");
			return false;
		}
		return true;
	}
}
