package com.core.zt.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.app.ui.ControlSugarActivity;
import com.core.zt.app.ui.MyContractActivity;
import com.core.zt.app.ui.MyDrugActivity;
import com.core.zt.app.ui.NotificationActivity;
import com.core.zt.app.ui.ReportActivity;
import com.core.zt.app.ui.SetActivity;
import com.core.zt.app.ui.UserCenterActivity;
import com.core.zt.model.MyRecord;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.widget.BadgeView;
import com.core.zt.widget.CircleImageView;
import com.core.zt.widget.HeaderBar;

public class MeFragment extends BaseFragment {
    private HeaderBar bar;
    private RelativeLayout rlSet,rlUser,rlDoctor,rlMessage,rlReport;
    private Button btnControl,btnDrug;
    private TextView tvName,tvSex;
    private CircleImageView ivPhoto;
    public TextView tvChat;
    private TextView tvOmin,tv1min,tv3min,tv4min,tv5min,tv6min,tv7min,tv8min,tvp1min,tvp2min,tvbmimin,tvhbamin;
    private TextView tvOmint,tv1mint,tv3mint,tv4mint,tv5mint,tv6mint,tv7mint,tv8mint,tvp1mint,tvp2mint,tvbmimint,tvhbamint;
    private TextView tvOmax,tv1max,tv3max,tv4max,tv5max,tv6max,tv7max,tv8max,tvp1max,tvp2max,tvbmimax,tvhbamax;
    private TextView tvOmaxt,tv1maxt,tv3maxt,tv4maxt,tv5maxt,tv6maxt,tv7maxt,tv8maxt,tvp1maxt,tvp2maxt,tvbmimmaxt,tvhbammaxt;
    private TextView tvOavg,tv1avg,tv3avg,tv4avg,tv5avg,tv6avg,tv7avg,tv8avg,tvp1avg,tvp2avg,tvbmiavg,tvhbaavg;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_me, null);
        return mView;
    }

    @Override
    protected void initData() {
        rlSet = (RelativeLayout) findViewWithId(R.id.rl_set);
        rlUser= (RelativeLayout) findViewWithId(R.id.rl_user);
        tvName=findViewWithId(R.id.tv_name);
        tvSex=findViewWithId(R.id.tv_sex);
        btnControl=findViewWithId(R.id.btn_control);
        ivPhoto=findViewWithId(R.id.iv_photo);
        tvOmin=findViewWithId(R.id.tv_0min);
        tv1min=findViewWithId(R.id.tv_1min);
        tv3min=findViewWithId(R.id.tv_3min);
        tv4min=findViewWithId(R.id.tv_4min);
        tv5min=findViewWithId(R.id.tv_5min);
        tv6min=findViewWithId(R.id.tv_6min);
        tv7min=findViewWithId(R.id.tv_7min);
        tv8min=findViewWithId(R.id.tv_8min);
        tvOmint=findViewWithId(R.id.tv_0mint);
        tv1mint=findViewWithId(R.id.tv_1mint);
        tv3mint=findViewWithId(R.id.tv_3mint);
        tv4mint=findViewWithId(R.id.tv_4mint);
        tv5mint=findViewWithId(R.id.tv_5mint);
        tv6mint=findViewWithId(R.id.tv_6mint);
        tv7mint=findViewWithId(R.id.tv_7mint);
        tv8mint=findViewWithId(R.id.tv_8mint);
        tvp1min=findViewWithId(R.id.tv_p1min);
        tvp2min=findViewWithId(R.id.tv_p2min);
        tvp1mint=findViewWithId(R.id.tv_p1mint);
        tvp2mint=findViewWithId(R.id.tv_p2mint);
        tvbmimin=findViewWithId(R.id.tv_bmimin);
        tvbmimint=findViewWithId(R.id.tv_bmimint);
        tvhbamin=findViewWithId(R.id.tv_hbamin);
        tvhbamint=findViewWithId(R.id.tv_hbamint);

        tvOmax=findViewWithId(R.id.tv_0max);
        tv1max=findViewWithId(R.id.tv_1max);
        tv3max=findViewWithId(R.id.tv_3max);
        tv4max=findViewWithId(R.id.tv_4max);
        tv5max=findViewWithId(R.id.tv_5max);
        tv6max=findViewWithId(R.id.tv_6max);
        tv7max=findViewWithId(R.id.tv_7max);
        tv8max=findViewWithId(R.id.tv_8max);
        tvOmaxt=findViewWithId(R.id.tv_0maxt);
        tv1maxt=findViewWithId(R.id.tv_1maxt);
        tv3maxt=findViewWithId(R.id.tv_3maxt);
        tv4maxt=findViewWithId(R.id.tv_4maxt);
        tv5maxt=findViewWithId(R.id.tv_5maxt);
        tv6maxt=findViewWithId(R.id.tv_6maxt);
        tv7maxt=findViewWithId(R.id.tv_7maxt);
        tv8maxt=findViewWithId(R.id.tv_8maxt);
        tvp1max=findViewWithId(R.id.tv_p1max);
        tvp2max=findViewWithId(R.id.tv_p2max);
        tvp1maxt=findViewWithId(R.id.tv_p1maxt);
        tvp2maxt=findViewWithId(R.id.tv_p2maxt);
        tvbmimax=findViewWithId(R.id.tv_bmimax);
        tvbmimmaxt=findViewWithId(R.id.tv_bmimaxt);
        tvhbamax=findViewWithId(R.id.tv_hbamax);
        tvhbammaxt=findViewWithId(R.id.tv_hbamaxt);

        tvOavg=findViewWithId(R.id.tv_0avg);
        tv1avg=findViewWithId(R.id.tv_1avg);
        tv3avg=findViewWithId(R.id.tv_3avg);
        tv4avg=findViewWithId(R.id.tv_4avg);
        tv5avg=findViewWithId(R.id.tv_5avg);
        tv6avg=findViewWithId(R.id.tv_6avg);
        tv7avg=findViewWithId(R.id.tv_7avg);
        tv8avg=findViewWithId(R.id.tv_8avg);
        tvp1avg=findViewWithId(R.id.tv_p1avg);
        tvp2avg=findViewWithId(R.id.tv_p2avg);
        tvbmiavg=findViewWithId(R.id.tv_bmiavg);
        tvhbaavg=findViewWithId(R.id.tv_hbaavg);

        rlDoctor=findViewWithId(R.id.rl_doctor);
        rlMessage=findViewWithId(R.id.rl_message);
        rlReport=findViewWithId(R.id.rl_report);
        btnDrug=findViewWithId(R.id.btn_drug);
        tvChat=findViewWithId(R.id.tv_chat);
    }

    @Override
    protected void initView() {
        bar = findViewWithId(R.id.header);
    }

    @Override
    public void onResume() {
        BadgeView bv=new BadgeView(getActivity(),tvChat);
        bv.setText(AppContext.count1+AppContext.count2+"");
        bv.show();
        if (AppContext.count1+AppContext.count2 == 0) {
            bv.hide();
        }
        AppContext.setImage(AppContext.currentUser.getPhoto(),ivPhoto,AppContext.memberPhotoOption);
        tvName.setText(AppContext.currentUser.getNickName());
        tvSex.setText(AppContext.currentUser.getSex()==1?"男":"女");
        RecordServer.getInstance(getActivity()).getMyRecord(AppContext.currentUser.getUserId(), new CustomAsyncResponehandler() {
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    MyRecord item = (MyRecord) baseModel.getResultObj();
                    if (item.getBloodSugar0().getMin() == null) {
                        tvOmin.setText("-");
                        tvOmint.setText("");
                    } else {
                        tvOmin.setText(item.getBloodSugar0().getMin().getLogVal1());
                        tvOmint.setText(item.getBloodSugar0().getMin().getMD());
                    }
                    if (item.getBloodSugar1().getMin() == null) {
                        tv1min.setText("-");
                        tv1mint.setText("");
                    } else {
                        tv1min.setText(item.getBloodSugar1().getMin().getLogVal1());
                        tv1mint.setText(item.getBloodSugar1().getMin().getMD());
                    }
                    if (item.getBloodSugar3().getMin() == null) {
                        tv3min.setText("-");
                        tv3mint.setText("");
                    } else {
                        tv3min.setText(item.getBloodSugar3().getMin().getLogVal1());
                        tv3mint.setText(item.getBloodSugar3().getMin().getMD());
                    }
                    if (item.getBloodSugar4().getMin() == null) {
                        tv4min.setText("-");
                        tv4mint.setText("");
                    } else {
                        tv4min.setText(item.getBloodSugar4().getMin().getLogVal1());
                        tv4mint.setText(item.getBloodSugar4().getMin().getMD());
                    }
                    if (item.getBloodSugar5().getMin() == null) {
                        tv5min.setText("-");
                        tv5mint.setText("");
                    } else {
                        tv5min.setText(item.getBloodSugar5().getMin().getLogVal1());
                        tv5mint.setText(item.getBloodSugar5().getMin().getMD());
                    }
                    if (item.getBloodSugar6().getMin() == null) {
                        tv6min.setText("-");
                        tv6mint.setText("");
                    } else {
                        tv6min.setText(item.getBloodSugar6().getMin().getLogVal1());
                        tv6mint.setText(item.getBloodSugar6().getMin().getMD());
                    }
                    if (item.getBloodSugar7().getMin() == null) {
                        tv7min.setText("-");
                        tv7mint.setText("");
                    } else {
                        tv7min.setText(item.getBloodSugar7().getMin().getLogVal1());
                        tv7mint.setText(item.getBloodSugar7().getMin().getMD());
                    }
                    if (item.getBloodSugar8().getMin() == null) {
                        tv8min.setText("-");
                        tv8mint.setText("");
                    } else {
                        tv8min.setText(item.getBloodSugar8().getMin().getLogVal1());
                        tv8mint.setText(item.getBloodSugar8().getMin().getMD());
                    }

                    if (item.getBloodPressure().getMin() == null) {
                        tvp1min.setText("-");
                        tvp1mint.setText("");
                        tvp2min.setText("-");
                        tvp2mint.setText("");
                    } else {
                        tvp1min.setText(item.getBloodPressure().getMin().getLogVal1());
                        tvp1mint.setText(item.getBloodPressure().getMin().getMD());
                        tvp1min.setText(item.getBloodPressure().getMin().getLogVal2());
                        tvp2mint.setText(item.getBloodPressure().getMin().getMD());
                    }

                    if (item.getBMI().getMin() == null) {
                        tvbmimin.setText("-");
                        tvbmimint.setText("");
                    } else {
                        tvbmimin.setText(item.getBMI().getMin().getLogVal1());
                        tvbmimint.setText(item.getBMI().getMin().getMD());
                    }
                    if (item.getHemoglobin().getMin() == null) {
                        tvhbamin.setText("-");
                        tvhbamint.setText("");
                    } else {
                        tvhbamin.setText(item.getHemoglobin().getMin().getLogVal1());
                        tvhbamint.setText(item.getHemoglobin().getMin().getMD());
                    }


                    if (item.getBloodSugar0().getMax() == null) {
                        tvOmax.setText("-");
                        tvOmaxt.setText("");
                    } else {
                        tvOmax.setText(item.getBloodSugar0().getMax().getLogVal1());
                        tvOmaxt.setText(item.getBloodSugar0().getMax().getMD());
                    }
                    if (item.getBloodSugar1().getMax() == null) {
                        tv1max.setText("-");
                        tv1maxt.setText("");
                    } else {
                        tv1max.setText(item.getBloodSugar1().getMax().getLogVal1());
                        tv1maxt.setText(item.getBloodSugar1().getMax().getMD());
                    }
                    if (item.getBloodSugar3().getMax() == null) {
                        tv3max.setText("-");
                        tv3maxt.setText("");
                    } else {
                        tv3max.setText(item.getBloodSugar3().getMax().getLogVal1());
                        tv3maxt.setText(item.getBloodSugar3().getMax().getMD());
                    }
                    if (item.getBloodSugar4().getMax() == null) {
                        tv4max.setText("-");
                        tv4maxt.setText("");
                    } else {
                        tv4max.setText(item.getBloodSugar4().getMax().getLogVal1());
                        tv4maxt.setText(item.getBloodSugar4().getMax().getMD());
                    }
                    if (item.getBloodSugar5().getMax() == null) {
                        tv5max.setText("-");
                        tv5maxt.setText("");
                    } else {
                        tv5max.setText(item.getBloodSugar5().getMax().getLogVal1());
                        tv5maxt.setText(item.getBloodSugar5().getMax().getMD());
                    }
                    if (item.getBloodSugar6().getMax() == null) {
                        tv6max.setText("-");
                        tv6maxt.setText("");
                    } else {
                        tv6max.setText(item.getBloodSugar6().getMax().getLogVal1());
                        tv6maxt.setText(item.getBloodSugar6().getMax().getMD());
                    }
                    if (item.getBloodSugar7().getMax() == null) {
                        tv7max.setText("-");
                        tv7maxt.setText("");
                    } else {
                        tv7max.setText(item.getBloodSugar7().getMax().getLogVal1());
                        tv7maxt.setText(item.getBloodSugar7().getMax().getMD());
                    }
                    if (item.getBloodSugar8().getMax() == null) {
                        tv8max.setText("-");
                        tv8maxt.setText("");
                    } else {
                        tv8max.setText(item.getBloodSugar8().getMax().getLogVal1());
                        tv8maxt.setText(item.getBloodSugar8().getMax().getMD());
                    }

                    if (item.getBloodPressure().getMax() == null) {
                        tvp1max.setText("-");
                        tvp1maxt.setText("");
                        tvp2max.setText("-");
                        tvp2maxt.setText("");
                    } else {
                        tvp1max.setText(item.getBloodPressure().getMax().getLogVal1());
                        tvp1maxt.setText(item.getBloodPressure().getMax().getMD());
                        tvp1max.setText(item.getBloodPressure().getMax().getLogVal2());
                        tvp2maxt.setText(item.getBloodPressure().getMax().getMD());
                    }

                    if (item.getBMI().getMax() == null) {
                        tvbmimax.setText("-");
                        tvbmimmaxt.setText("");
                    } else {
                        tvbmimax.setText(item.getBMI().getMax().getLogVal1());
                        tvbmimmaxt.setText(item.getBMI().getMax().getMD());
                    }
                    if (item.getHemoglobin().getMax() == null) {
                        tvhbamax.setText("-");
                        tvhbammaxt.setText("");
                    } else {
                        tvhbamax.setText(item.getHemoglobin().getMax().getLogVal1());
                        tvhbammaxt.setText(item.getHemoglobin().getMax().getMD());
                    }

                    tvOavg.setText(item.getBloodSugar0().getAvgNumerical()==0?"-":(item.getBloodSugar0().getAvgNumerical()+""));
                    tv1avg.setText(item.getBloodSugar1().getAvgNumerical()==0?"-":(item.getBloodSugar1().getAvgNumerical()+""));
                    tv3avg.setText(item.getBloodSugar2().getAvgNumerical()==0?"-":(item.getBloodSugar2().getAvgNumerical()+""));
                    tv4avg.setText(item.getBloodSugar3().getAvgNumerical()==0?"-":(item.getBloodSugar3().getAvgNumerical()+""));
                    tv5avg.setText(item.getBloodSugar4().getAvgNumerical()==0?"-":(item.getBloodSugar4().getAvgNumerical()+""));
                    tv6avg.setText(item.getBloodSugar5().getAvgNumerical()==0?"-":(item.getBloodSugar5().getAvgNumerical()+""));
                    tv7avg.setText(item.getBloodSugar6().getAvgNumerical()==0?"-":(item.getBloodSugar6().getAvgNumerical()+""));
                    tv8avg.setText(item.getBloodSugar7().getAvgNumerical()==0?"-":(item.getBloodSugar7().getAvgNumerical()+""));

                    tvp1avg.setText(item.getBloodPressure().getAvgNumerical()==0?"-":(item.getBloodPressure().getAvgNumerical()+""));
                    tvp2avg.setText(item.getBloodPressure().getAvgNumerical()==0?"-":(item.getBloodPressure().getAvgNumerical()+""));
                    tvbmiavg.setText(item.getBMI().getAvgNumerical()==0?"-":(item.getBMI().getAvgNumerical()+""));
                    tvhbaavg.setText(item.getHemoglobin().getAvgNumerical()==0?"-":(item.getHemoglobin().getAvgNumerical()+""));
                }
            }
        });
        super.onResume();
    }

    @Override
    protected void bindView() {

        AppContext.setImage(AppContext.currentUser.getPhoto(), ivPhoto, AppContext.memberPhotoOption);
        bar.back.setVisibility(View.GONE);
        tvName.setText(AppContext.currentUser.getNickName());
        tvSex.setText(AppContext.currentUser.getSex()==1?"男":"女");
        bar.setTitle("我的");
        bar.top_right_img.setImageResource(R.drawable.icon_setting);
        bar.top_right_img.setVisibility(View.VISIBLE);
        bar.top_right_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.JumpToActivity(SetActivity.class, false);
            }
        });
        rlUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.JumpToActivity(UserCenterActivity.class, false);
            }
        });
        btnControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.JumpToActivity(ControlSugarActivity.class, false);
            }
        });
        rlDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.JumpToActivity(MyContractActivity.class, false);
            }
        });
        rlMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.JumpToActivity(NotificationActivity.class, false);
            }
        });
        rlReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.JumpToActivity(ReportActivity.class, false);
            }
        });
        btnDrug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MyDrugActivity.class));
            }
        });
    }

}
