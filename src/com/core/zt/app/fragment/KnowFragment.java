package com.core.zt.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.core.zt.R;
import com.core.zt.app.ui.MessageActivity;
import com.core.zt.app.ui.MessageListActivity;
import com.core.zt.app.ui.WebViewActivity;
import com.core.zt.model.Advert;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.Urls;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.LessonServer;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.SlideShowView;

import java.util.ArrayList;
import java.util.List;

public class KnowFragment extends BaseFragment {

    private HeaderBar bar;
    private SlideShowView slideShowView;
    private Button btnK1, btnK2;
    private Intent intent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_know, null);
        return mView;
    }

    @Override
    protected void initData() {
        intent = new Intent();
        getBanner();
    }

    @Override
    protected void initView() {
        bar = findViewWithId(R.id.header);
        slideShowView = findViewWithId(R.id.slideshowview);
        btnK1 = (Button) findViewWithId(R.id.btn_k1);
        btnK2 = (Button) findViewWithId(R.id.btn_k2);
    }

    @Override
    protected void bindView() {
        bar.setTitle("知识");
        bar.back.setVisibility(View.GONE);
        List<Integer> strList = new ArrayList<Integer>();
        strList.add(R.drawable.h0);
        slideShowView.setImagesById(strList);
        btnK1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.setClass(getActivity(), MessageActivity.class);
                intent.putExtra("title", "精品课程");
                startActivity(intent);
            }
        });

        btnK2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.setClass(getActivity(), MessageListActivity.class);
                intent.putExtra("title", "特约专栏");
                intent.putExtra("type", 1);
                startActivity(intent);
            }
        });
    }

    private void getBanner() {
        LessonServer.getInstance(context).getPlayPic(new CustomAsyncResponehandler() {
            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    final List<Advert> lists = (List<Advert>) baseModel
                            .getResultObj();
                    if (lists != null && lists.size() > 0) {
                        List<String> strList = new ArrayList<String>();
                        for (Advert item : lists) {
                            strList.add(Urls.IMAGE_URL + item.getPicPath());
                        }
                        slideShowView.setImageUris(strList);
                        slideShowView.myCallBack=new SlideShowView.OnBranchClickListener() {
                            @Override
                            public void OnBranchClick(int position) {
                                Advert item=lists.get(position);
                                intent.setClass(getActivity(), WebViewActivity.class);
                                intent.putExtra("title",item.getPicName());
                                intent.putExtra("url",item.getPicDesc());
                                startActivity(intent);
                            }
                        };
                    }
                }
            }
        });
    }


}
