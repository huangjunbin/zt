package com.core.zt.app.fragment;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.AppContext;
import com.core.zt.app.adapter.RecordAdapter;
import com.core.zt.app.ui.BMIChartActivity;
import com.core.zt.app.ui.BMIDetailActivity;
import com.core.zt.app.ui.BaseActivity;
import com.core.zt.app.ui.BloodPressChartActivity;
import com.core.zt.app.ui.BloodPressDetailActivity;
import com.core.zt.app.ui.BloodSugarChartActivity;
import com.core.zt.app.ui.BloodSugarDetailActivity;
import com.core.zt.app.ui.CheckListActivity;
import com.core.zt.app.ui.HbaChartActivity;
import com.core.zt.app.ui.HbaDetailActivity;
import com.core.zt.app.ui.NewBMIActivity;
import com.core.zt.app.ui.NewBloodPressActivity;
import com.core.zt.app.ui.NewBloodSugarActivity;
import com.core.zt.app.ui.NewCheckListActivity;
import com.core.zt.app.ui.NewHbarActivity;
import com.core.zt.model.Record;
import com.core.zt.model.ResponeModel;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.RecordServer;
import com.core.zt.util.DisplayUtils;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.XListView;

import java.util.ArrayList;
import java.util.List;

public class RecordFragment extends BaseFragment {

    private HeaderBar bar;
    private XListView xlvData;
    private List<Record> dataList;
    private RecordAdapter adapter;
    private PopupWindow popupWindow;
    private int type = 1;
    private TextView tvAdd;
    private int index = 0;
    private Intent intent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_record, null);
        return mView;
    }

    @Override
    protected void initData() {
        intent = new Intent();
        dataList = new ArrayList<>();
        adapter = new RecordAdapter((BaseActivity) getActivity(), dataList);
    }

    @Override
    protected void initView() {
        bar = findViewWithId(R.id.header);
        tvAdd = findViewWithId(R.id.tv_add);
        xlvData = findViewWithId(R.id.xlv_data);
    }

    @Override
    public void onResume() {
        getRecord(index);
        super.onResume();
    }

    @Override
    protected void bindView() {
        bar.back.setVisibility(View.GONE);
        bar.setTitle("所有记录");
        bar.top_right_img.setImageDrawable(getResources().getDrawable(R.drawable.btn_add));
        bar.top_right_img.setVisibility(View.VISIBLE);
        bar.top_title.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.icon_down), null);
        bar.top_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDropWindow( bar.top_title);
            }
        });
        bar.top_right_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDropWindow2( bar.top_title);
            }
        });
        xlvData.setFooterDividersEnabled(false);
        xlvData.setPullLoadEnable(false);
        xlvData.setAdapter(adapter);
        xlvData.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {
                getRecord(index);
                xlvData.stopRefresh();
            }

            @Override
            public void onLoadMore() {
                xlvData.stopLoadMore();
            }
        });
        xlvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Record data = dataList.get(i - 1);

                if ("1".equals(data.getLogType())) {//血糖
                    intent.putExtra("data", data);
                    intent.setClass(getActivity(), BloodSugarDetailActivity.class);
                    startActivity(intent);
                }
                if ("2".equals(data.getLogType())) {//血压
                    intent.putExtra("data", data);
                    intent.setClass(getActivity(), BloodPressDetailActivity.class);
                    startActivity(intent);
                }
                if ("3".equals(data.getLogType())) {//BMI
                    intent.putExtra("data", data);
                    intent.setClass(getActivity(), BMIDetailActivity.class);
                    startActivity(intent);
                }
                if ("4".equals(data.getLogType())) {//BMI
                    intent.putExtra("data", data);
                    intent.setClass(getActivity(), CheckListActivity.class);
                    startActivity(intent);
                }

                if ("5".equals(data.getLogType())) {//BMI
                    intent.putExtra("data", data);
                    intent.setClass(getActivity(), HbaDetailActivity.class);
                    startActivity(intent);
                }

            }
        });
    }

    private void showDropWindow(View position) {
        bar.top_title.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.icon_up), null);
        View popBar = getActivity().getLayoutInflater().inflate(R.layout.pop_record,
                null);
        TextView btnR1, btnR2, btnR3, btnR4, btnR5,btnR6;
        btnR1 = (TextView) popBar.findViewById(R.id.btn_r1);
        btnR1.setTag(1);
        btnR1.setOnClickListener(clickListener);
        btnR2 = (TextView) popBar.findViewById(R.id.btn_r2);
        btnR2.setTag(2);
        btnR2.setOnClickListener(clickListener);
        btnR3 = (TextView) popBar.findViewById(R.id.btn_r3);
        btnR3.setTag(3);
        btnR3.setOnClickListener(clickListener);
        btnR4 = (TextView) popBar.findViewById(R.id.btn_r4);
        btnR4.setTag(4);
        btnR4.setOnClickListener(clickListener);
        btnR5 = (TextView) popBar.findViewById(R.id.btn_r5);
        btnR5.setTag(5);
        btnR5.setOnClickListener(clickListener);
        btnR6 = (TextView) popBar.findViewById(R.id.btn_r6);
        btnR6.setTag(6);
        btnR6.setOnClickListener(clickListener);
        popBar.findViewById(R.id.ll_buttom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        popupWindow = new PopupWindow();
        // 设置弹框的宽度为布局文件的宽
        popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
        // popupWindow.setHeight(DisplayUtils.dip2px(context, 300));
        // 设置一个透明的背景，不然无法实现点击弹框外，弹框消失
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // popupWindow.setBackgroundDrawable(getResources().getDrawable(
        // R.drawable.pupback));
        // 设置点击弹框外部，弹框消失
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setContentView(popBar);
        // 设置弹框出现的位置，在v的正下方横轴偏移textview的宽度，为了对齐~纵轴不偏移
        popupWindow.showAsDropDown(position, 0,
                DisplayUtils.dip2px(getActivity(), 11));
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                bar.top_title.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.icon_down), null);
            }

        });
    }

    private void showDropWindow2(View position) {
        bar.top_right_img.setImageDrawable(getResources().getDrawable(R.drawable.btn_remove));
        View popBar = getActivity().getLayoutInflater().inflate(R.layout.pop_record_add,
                null);
        TextView btnR1, btnR2, btnR3, btnR4,btnR5;
        btnR1 = (TextView) popBar.findViewById(R.id.btn_r1);
        btnR1.setTag(1);
        btnR1.setOnClickListener(clickListener2);
        btnR2 = (TextView) popBar.findViewById(R.id.btn_r2);
        btnR2.setTag(2);
        btnR2.setOnClickListener(clickListener2);
        btnR3 = (TextView) popBar.findViewById(R.id.btn_r3);
        btnR3.setTag(3);
        btnR3.setOnClickListener(clickListener2);
        btnR4 = (TextView) popBar.findViewById(R.id.btn_r4);
        btnR4.setTag(4);
        btnR4.setOnClickListener(clickListener2);
        btnR5 = (TextView) popBar.findViewById(R.id.btn_r5);
        btnR5.setTag(5);
        btnR5.setOnClickListener(clickListener2);
        popBar.findViewById(R.id.ll_buttom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        popupWindow = new PopupWindow();
        // 设置弹框的宽度为布局文件的宽
        popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
        // popupWindow.setHeight(DisplayUtils.dip2px(context, 300));
        // 设置一个透明的背景，不然无法实现点击弹框外，弹框消失
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // popupWindow.setBackgroundDrawable(getResources().getDrawable(
        // R.drawable.pupback));
        // 设置点击弹框外部，弹框消失
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setContentView(popBar);
        // 设置弹框出现的位置，在v的正下方横轴偏移textview的宽度，为了对齐~纵轴不偏移
        popupWindow.showAsDropDown(position, 0,
                DisplayUtils.dip2px(getActivity(), 11));
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                bar.top_right_img.setImageDrawable(getResources().getDrawable(R.drawable.btn_add));
            }

        });
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            popupWindow.dismiss();
            type = (int) view.getTag();

            if (type == 1) {
                index = 0;
                bar.top_left_btn.setText("");
                bar.setTitle("所有记录");
            }
            if (type == 2) {
                bar.setTitle("血糖");
                index = 1;
                bar.top_left_btn.setText("图表");
                bar.top_left_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        baseActivity.JumpToActivity(BloodSugarChartActivity.class, false);
                    }
                });
            }
            if (type == 3) {
                bar.setTitle("血压");
                index = 2;
                bar.top_left_btn.setText("图表");
                bar.top_left_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        baseActivity.JumpToActivity(BloodPressChartActivity.class, false);
                    }
                });
            }
            if (type == 4) {
                bar.setTitle("体重指数");
                index = 3;
                bar.top_left_btn.setText("图表");
                bar.top_left_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        baseActivity.JumpToActivity(BMIChartActivity.class, false);
                    }
                });
            }
            if (type == 5) {
                index=4;
                bar.setTitle("化验单");
                bar.top_left_btn.setText("");

            }  if (type == 6) {
                index=5;
                bar.setTitle("HbA1c");
                bar.top_left_btn.setText("图表");
                bar.top_left_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        baseActivity.JumpToActivity(HbaChartActivity.class, false);
                    }
                });

            }
            getRecord(index);
        }
    };

    View.OnClickListener clickListener2 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            popupWindow.dismiss();
            type = (int) view.getTag();

            if (type == 1) {
                baseActivity.JumpToActivity(NewBloodSugarActivity.class, false);

            }
            if (type == 2) {
                baseActivity.JumpToActivity(NewBloodPressActivity.class, false);
            }
            if (type == 3) {
                baseActivity.JumpToActivity(NewBMIActivity.class, false);
            }
            if (type == 4) {
                baseActivity.JumpToActivity(NewCheckListActivity.class, false);
            }
            if (type == 5) {
                baseActivity.JumpToActivity(NewHbarActivity.class, false);
            }
        }
    };

    private void getRecord(int logType) {
        RecordServer.getInstance(context).getRecord(AppContext.currentUser.getUserId(),
                logType + "", new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Record> lists = (List<Record>) baseModel
                                    .getResultObj();
                            if (lists != null && lists.size() >=0) {
                                dataList.clear();
                                dataList.addAll(0, lists);
                                adapter.notifyDataSetChanged();
                                ;
                            }
                        }
                    }
                });
    }
}
