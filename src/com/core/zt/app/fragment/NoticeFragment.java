package com.core.zt.app.fragment;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.core.zt.R;
import com.core.zt.app.adapter.NoticeAdapter;
import com.core.zt.app.ui.BaseActivity;
import com.core.zt.app.ui.NewNoticeActivity;
import com.core.zt.model.Notice;
import com.core.zt.service.NoticeServer;
import com.core.zt.util.DisplayUtils;
import com.core.zt.widget.HeaderBar;
import com.core.zt.widget.XListView;

import java.util.List;

public class NoticeFragment extends BaseFragment {
    private HeaderBar bar;
    private XListView xlvData;
    private List<Notice> dataList;
    private NoticeAdapter adapter;
    private PopupWindow popupWindow;
    private NoticeServer noticeServer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_notice, null);
        return mView;
    }

    @Override
    public void onResume() {
        dataList=noticeServer.getNoticeList();
        adapter = new NoticeAdapter((BaseActivity) getActivity(), dataList);
        xlvData.setAdapter(adapter);
        super.onResume();
    }

    @Override
    protected void initData() {
        noticeServer=new NoticeServer(getActivity());
        dataList=noticeServer.getNoticeList();
        adapter = new NoticeAdapter((BaseActivity) getActivity(), dataList);
        xlvData.setAdapter(adapter);
    }

    @Override
    protected void initView() {
        bar = findViewWithId(R.id.header);
        xlvData = findViewWithId(R.id.xlv_data);
    }

    @Override
    protected void bindView() {
        bar.back.setVisibility(View.GONE);
        bar.setTitle("提醒");
        dataList=noticeServer.getNoticeList();
        adapter = new NoticeAdapter((BaseActivity) getActivity(), dataList);
        bar.top_right_img.setImageDrawable(getResources().getDrawable(R.drawable.btn_add));
        bar.top_right_img.setVisibility(View.VISIBLE);
        bar.top_right_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDropWindow( bar.top_title);
            }
        });
        xlvData.setFooterDividersEnabled(false);
        xlvData.setPullLoadEnable(false);
        xlvData.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {
                xlvData.stopRefresh();
                onResume();
            }

            @Override
            public void onLoadMore() {
                xlvData.stopLoadMore();
            }
        });
        xlvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), NewNoticeActivity.class);
                intent.putExtra("tag", 1);
                intent.putExtra("type",dataList.get(i-1).getType());
                intent.putExtra("data", dataList.get(i-1));
                startActivity(intent);
            }
        });
    }

    private void showDropWindow(View position) {
        bar.top_right_img.setImageDrawable(getResources().getDrawable(R.drawable.btn_remove));
        final View popBar = getActivity().getLayoutInflater().inflate(R.layout.pop_notice,
                null);
        TextView btn1, btn2, btn3;
        btn1 = (TextView) popBar.findViewById(R.id.btn_n1);
        btn1.setTag(1);
        btn2 = (TextView) popBar.findViewById(R.id.btn_n2);
        btn2.setTag(2);
        btn3 = (TextView) popBar.findViewById(R.id.btn_n3);
        btn3.setTag(3);
        popBar.findViewById(R.id.ll_buttom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        btn1.setOnClickListener(clickListener);
        btn2.setOnClickListener(clickListener);
        btn3.setOnClickListener(clickListener);
        popupWindow = new PopupWindow();
        // 设置弹框的宽度为布局文件的宽
        popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
        // popupWindow.setHeight(DisplayUtils.dip2px(context, 300));
        // 设置一个透明的背景，不然无法实现点击弹框外，弹框消失
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // popupWindow.setBackgroundDrawable(getResources().getDrawable(
        // R.drawable.pupback));
        // 设置点击弹框外部，弹框消失
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setContentView(popBar);
        // 设置弹框出现的位置，在v的正下方横轴偏移textview的宽度，为了对齐~纵轴不偏移
        popupWindow.showAsDropDown(position, 0,
                DisplayUtils.dip2px(getActivity(), 11));
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                bar.top_right_img.setImageDrawable(getResources().getDrawable(R.drawable.btn_add));
            }

        });

    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            popupWindow.dismiss();
            Intent intent = new Intent(getActivity(), NewNoticeActivity.class);
            intent.putExtra("type", (int) view.getTag());
            startActivity(intent);
        }
    };
}
