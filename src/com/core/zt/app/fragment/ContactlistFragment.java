/**
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.core.zt.app.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.core.zt.R;
import com.core.zt.app.AppConfig;
import com.core.zt.app.AppContext;
import com.core.zt.app.adapter.ContactAdapter;
import com.core.zt.app.ui.ChatActivity;
import com.core.zt.app.ui.MessageHistoryActivity;
import com.core.zt.app.ui.NewFriendsMsgActivity;
import com.core.zt.db.InviteMessgeDao;
import com.core.zt.model.Doctor;
import com.core.zt.model.ResponeModel;
import com.core.zt.model.chat.User;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.service.IMService;
import com.core.zt.util.DateUtil;
import com.core.zt.widget.BadgeView;
import com.core.zt.widget.Sidebar;
import com.core.zt.widget.XListView;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMContactManager;
import com.easemob.exceptions.EaseMobException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 联系人列表页
 * 
 */
public class ContactlistFragment extends Fragment {
	private String TAG = "ContactlistFragment";
	private ContactAdapter adapter;
	private List<User> contactList;
	private XListView listView;
	private boolean hidden;
	private Sidebar sidebar;
	private InputMethodManager inputMethodManager;
	TextView addContactView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_contact_list, container,
				false);
	}
	@Override
	public void onDestroy() {
		super.onDestroy();

		// 注销广播接收者
		try {
			getActivity().unregisterReceiver(msgReceiver);
		} catch (Exception e) {
		}


	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		bv=new BadgeView(getActivity(),addContactView);
		// 注册一个接收消息的BroadcastReceiver
		msgReceiver = new NewMessageBroadcastReceiver();
		IntentFilter intentFilter = new IntentFilter(EMChatManager
				.getInstance().getNewMessageBroadcastAction());
		intentFilter.setPriority(3);
		getActivity().registerReceiver(msgReceiver, intentFilter);

		inputMethodManager = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST);
		listView = (XListView) getView().findViewById(R.id.list);
		sidebar = (Sidebar) getView().findViewById(R.id.sidebar);
		sidebar.setListView(listView);
		contactList = new ArrayList<User>();
		
		// 设置adapter
		adapter = new ContactAdapter(getActivity(), R.layout.row_contact,
				contactList, sidebar);
		listView.setAdapter(adapter);
		refreshContactList();
		getView().findViewById(R.id.btn_top_back).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						getActivity().finish();
					}
				});
		listView.setPullLoadEnable(false);
		listView.setFooterDividersEnabled(false);
		listView.setXListViewListener(new XListView.IXListViewListener() {

			@Override
			public void onRefresh() {
				listView.setRefreshTime(DateUtil.getDateTime(new Date(System
						.currentTimeMillis())));
				refreshContactList();

			}

			@Override
			public void onLoadMore() {

			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				String username = adapter.getItem(position - 1).getUsername();
				if (AppConfig.NEW_FRIENDS_USERNAME.equals(username)) {
					// TODO进入申请与通知页面
					User user = AppContext.getApplication().getContactList()
							.get(AppConfig.NEW_FRIENDS_USERNAME);
					user.setUnreadMsgCount(0);
					startActivity(new Intent(getActivity(),
							NewFriendsMsgActivity.class));
				} else {

					// demo中直接进入聊天页面，实际一般是进入用户详情页
					startActivity(new Intent(getActivity(), ChatActivity.class)
							.putExtra("userId",
									adapter.getItem(position - 1).getId())
							.putExtra("userName",
									adapter.getItem(position - 1).getUsername()));

				}
			}
		});
		listView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// 隐藏软键盘
				if (getActivity().getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
					if (getActivity().getCurrentFocus() != null)
						inputMethodManager.hideSoftInputFromWindow(
								getActivity().getCurrentFocus()
										.getWindowToken(),
								InputMethodManager.HIDE_NOT_ALWAYS);
				}
				return false;
			}
		});

		  addContactView = (TextView) getView().findViewById(
				R.id.iv_new_contact);
		// 进入添加好友页
		addContactView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(),
						MessageHistoryActivity.class));
			}
		});



		registerForContextMenu(listView);

	}


	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		// 长按前两个不弹menu
		if (((AdapterContextMenuInfo) menuInfo).position > 2) {
			getActivity().getMenuInflater().inflate(
					R.menu.context_contact_list, menu);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.delete_contact) {
			User tobeDeleteUser = adapter
					.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position - 1);
			// 删除此联系人
			deleteContact(tobeDeleteUser);
			// 删除相关的邀请消息
			InviteMessgeDao dao = new InviteMessgeDao(getActivity());
			dao.deleteMessage(tobeDeleteUser.getUsername());
			return true;
		} /*
		 * else if (item.getItemId() == R.id.add_to_blacklist) { User user =
		 * adapter.getItem(((AdapterContextMenuInfo) item
		 * .getMenuInfo()).position); moveToBlacklist(user.getUsername());
		 * return true; }
		 */
		return super.onContextItemSelected(item);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		this.hidden = hidden;
		if (!hidden) {
			refresh();
		}
	}
	BadgeView bv;
	@Override
	public void onResume() {
		super.onResume();

		bv.setText(AppContext.count1 + "");
		bv.show();
		if(AppContext.count1==0){
			bv.hide();
		}
		if (!hidden) {
			refresh();
		}
	}

	/**
	 * 删除联系人
	 *
	 */
	public void deleteContact(final User tobeDeleteUser) {
		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setMessage("正在删除...");
		pd.setCanceledOnTouchOutside(false);
		pd.show();
		new Thread(new Runnable() {
			public void run() {
				try {
					EMContactManager.getInstance().deleteContact(
							tobeDeleteUser.getId());
					// 删除db和内存中此用户的数据

					AppContext.getApplication().getContactList()
							.remove(tobeDeleteUser.getId());
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							pd.dismiss();
							adapter.remove(tobeDeleteUser);
							adapter.notifyDataSetChanged();

						}
					});
				} catch (final Exception e) {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							pd.dismiss();
							Toast.makeText(getActivity(),
									"删除失败: " + e.getMessage(), Toast.LENGTH_SHORT).show();
						}
					});

				}

			}
		}).start();

	}

	/**
	 * 把user移入到黑名单
	 */
	private void moveToBlacklist(final String username) {
		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setMessage("正在移入黑名单...");
		pd.setCanceledOnTouchOutside(false);
		pd.show();
		new Thread(new Runnable() {
			public void run() {
				try {
					// 加入到黑名单
					EMContactManager.getInstance().addUserToBlackList(username,
							true);
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							pd.dismiss();
							Toast.makeText(getActivity(), "移入黑名单成功", Toast.LENGTH_SHORT).show();
						}
					});
				} catch (EaseMobException e) {
					e.printStackTrace();
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							pd.dismiss();
							Toast.makeText(getActivity(), "移入黑名单失败", Toast.LENGTH_SHORT).show();
						}
					});
				}
			}
		}).start();

	}

	// 刷新ui
	public void refresh() {
		try {
			// 可能会在子线程中调到这方法
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					refreshContactList();
					adapter.notifyDataSetChanged();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getContactList() {
		contactList.clear();
		Map<String, User> users = AppContext.getApplication().getContactList();
		Iterator<Entry<String, User>> iterator = users.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, User> entry = iterator.next();
			if (!entry.getKey().equals(AppConfig.NEW_FRIENDS_USERNAME)
					&& !entry.getKey().equals(AppConfig.GROUP_USERNAME))
				contactList.add(entry.getValue());
		}
		// 排序
		Collections.sort(contactList, new Comparator<User>() {

			@Override
			public int compare(User lhs, User rhs) {
				return lhs.getUsername().compareToIgnoreCase(rhs.getUsername());
			}
		});

		// 加入"申请与通知"和"群聊"
		// contactList.add(0, users.get(AppConfig.GROUP_USERNAME));
		// 把"申请与通知"添加到首位
		contactList.add(0, users.get(AppConfig.NEW_FRIENDS_USERNAME));
		for (User user : contactList) {
			if (user.getHeader() != null && !"".equals(user.getHeader())) {
				user.setHeader(user.getHeader().toUpperCase());
			}
		}

		for (User user : contactList) {
			Log.d(TAG, user.getUsername());
		}
	}

	private void refreshContactList() {
		IMService.getInstance(getActivity()).getFriendsList(
				AppContext.currentUser.getUserId(),
				new CustomAsyncResponehandler() {

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						listView.stopRefresh();
						List<Doctor> memberList = (List<Doctor>) baseModel
								.getResultObj();
						Map<String, User> userlist = new HashMap<String, User>();
						if (memberList != null) {
							for (Doctor member : memberList) {
								User user = new User();
								user.setNick(member.getDoctorName());
								user.setUsername(member.getUserName());
								user.setId(member.getUserName());
								user.setUrl(member.getPhoto());
								IMService.getInstance(getActivity())
										.setUserHearder(member.getDoctorName()==null?member.getUserName():member.getDoctorName(),
												user);

								userlist.put(member.getUserName() + "", user);
							}

							// 添加user"申请与通知"
							User newFriends = new User();
							newFriends.setUsername(AppConfig.NEW_FRIENDS_USERNAME);
							newFriends.setNick("申请与通知");
							newFriends.setHeader("");
							userlist.put(AppConfig.NEW_FRIENDS_USERNAME, newFriends);

							// 存入内存
							AppContext.getApplication().setContactList(userlist);

							getContactList();
							adapter.notifyDataSetChanged();
						}
						super.onSuccess(baseModel);
					}

					@Override
					public void onFinish() {
						super.onFinish();
						listView.stopRefresh();
					}
				});
	}
	private NewMessageBroadcastReceiver msgReceiver;
	/**
	 * 新消息广播接收者
	 */
	private class NewMessageBroadcastReceiver extends BroadcastReceiver {
		@SuppressWarnings("unused")
		@Override
		public void onReceive(Context context, Intent intent) {
			bv.setText(AppContext.count1 + "");
			bv.show();
			if(AppContext.count1==0){
				bv.hide();
			}
			// 注销广播，否则在ChatActivity中会收到这个广播
			abortBroadcast();
		}
	}
}
