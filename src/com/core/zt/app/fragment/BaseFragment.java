package com.core.zt.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.core.zt.app.ui.BaseActivity;


public abstract class BaseFragment extends Fragment {

    protected Context context;
    protected View mView;
    protected int mIndex;
    protected BaseActivity baseActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseActivity = (BaseActivity) getActivity();
    }

    protected abstract void initData();

    protected abstract void initView();

    protected abstract void bindView();

    public void lastLoad() {
    }

    ;

    @SuppressWarnings("unchecked")
    public <T> T findViewWithId(int resId) {
        return (T) mView.findViewById(resId);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        bindView();
        lastLoad();
    }

}
