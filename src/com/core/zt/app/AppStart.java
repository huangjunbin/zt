package com.core.zt.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

import com.core.zt.R;
import com.core.zt.app.ui.LoginActivity;
import com.core.zt.app.ui.MainActivity;
import com.core.zt.model.User;
import com.core.zt.service.UserServer;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;


public class AppStart extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final View view = View.inflate(this, R.layout.activity_start, null);
        setContentView(view);// 渐变展示启动屏
        AlphaAnimation aa = new AlphaAnimation(0.3f, 1.0f);
        aa.setDuration(2000);
        view.startAnimation(aa);
        aa.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                login();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {

            }
        });

    }

    /**
     * 登录
     */
    private void login() {
        User cacheUser = UserServer.getInstance(this).getCacheUser();
        if (cacheUser != null) {
            AppContext.currentUser = cacheUser;
            System.out.println(AppContext.currentUser.getUserName());
            System.out.println(AppContext.currentUser.getPassword());
            loginHX(AppContext.currentUser.getUserName(), AppContext.currentUser.getPassword(), new EMCallBack() {
                @Override
                public void onSuccess() {
                    Log.d("HX", "success");
                    startActivity(new Intent(AppStart.this, MainActivity.class));
                    finish();
                }

                @Override
                public void onError(int i, String s) {
                    Log.d("HX", s);
                    startActivity(new Intent(AppStart.this, MainActivity.class));
                    finish();
                }

                @Override
                public void onProgress(int i, String s) {

                }
            });



        } else {
            startActivity(new Intent(AppStart.this, LoginActivity.class));
            finish();
        }
    }

    public void loginHX(final String mobileNumber, final String password, final EMCallBack callback) {
        AppContext.getApplication().setPassword(password);
        Log.d("HX", mobileNumber + "_____" + password);
        // 调用sdk登陆方法登陆聊天服务器
        new Thread(new Runnable() {
            public void run() {
                EMChatManager.getInstance().login(mobileNumber, password,
                        callback);
            }
        }).start();
    }

}
