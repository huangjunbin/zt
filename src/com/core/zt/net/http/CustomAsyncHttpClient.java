package com.core.zt.net.http;

import android.content.Context;
import android.view.View;

import com.core.zt.model.RequestModel;
import com.core.zt.model.ResponeModel;
import com.core.zt.util.JsonUtil;
import com.core.zt.util.LogUtil;
import com.core.zt.util.UIHelper;
import com.core.zt.widget.MyProgressDialog;

import org.apache.http.message.BasicNameValuePair;

public class CustomAsyncHttpClient {
    private String TAG = "CustomAsyncHttpClient";
    private boolean isTag = true;

    private AsyncHttpClient asyncHttpClient;
    private MyProgressDialog dialog;
    private Context mContext;
    private ResponeModel baseModel;

    public CustomAsyncHttpClient(Context context) {
        asyncHttpClient = new AsyncHttpClient();
        mContext = context;
        if (mContext != null) {
            dialog = new MyProgressDialog(mContext, "", true);
            dialog.tv_value.setVisibility(View.GONE);
        }
        baseModel = new ResponeModel();
    }

    public void post(final RequestModel requestModel,
                     final CustomAsyncResponehandler responseHandler) {

        RequestParams newParams = new RequestParams();
        com.alibaba.fastjson.JSONObject jsonObject = new
                com.alibaba.fastjson.JSONObject();
        for (BasicNameValuePair param :
                requestModel.getParams().getParamsList()) {
            jsonObject.put(param.getName(), param.getValue());
        }
        newParams.fileParams=requestModel.getParams().fileParams;

        newParams.put("p", jsonObject.toString());


        LogUtil.d(TAG, requestModel.getUrl() + "?"
                + newParams.toString(), isTag);
        asyncHttpClient.post(requestModel.getUrl(), newParams,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        LogUtil.d(TAG, "onStart___", isTag);
                        if (requestModel.isShowDialog()) {// 显示网络对话框
                            if (mContext != null) {
                                dialog.show();
                            }
                        }
                        responseHandler.onStart();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LogUtil.d(TAG, "onFinish___", isTag);
                        if (requestModel.isShowDialog()) {// 隐藏网络对话框
                            if (mContext != null) {
                                dialog.dismiss();
                            }
                        }
                        responseHandler.onFinish();
                    }

                    @Override
                    public void onSuccess(String content) {
                        super.onSuccess(content);
                        LogUtil.d(TAG, "onSuccess___" + content, isTag);

                        // TODO:解密返回的参数
                        baseModel = JsonUtil.convertJsonToObject(content,
                                ResponeModel.class);

                        if (baseModel != null) {
                            baseModel.setCls(requestModel.getCls());
                            baseModel.setJson(content);
                            baseModel.init();
                            if(baseModel.isStatus()){
                                baseModel.setMsg("操作成功");
                            }
                        }

                        if (requestModel.isShowErrorMessage()&&!baseModel.isStatus()) {
                            if (mContext != null) {
                                UIHelper.ShowMessage(mContext,
                                        baseModel.getMsg());
                            }
                        }

                        responseHandler.onSuccess(baseModel);
                    }

                    @Override
                    public void onFailure(Throwable error, String content) {
                        super.onFailure(error, content);
                        LogUtil.d(TAG, "onFailure___" + content, isTag);
                        if (mContext != null) {
                            UIHelper.ShowMessage(mContext,
                                   "网络异常");
                        }
                        responseHandler.onFailure(error, content);
                    }
                });
    }
}
