package com.core.zt.net.http;

import com.core.zt.model.ResponeModel;

public class CustomAsyncResponehandler extends AsyncHttpResponseHandler {

	private CustomAsyncResponehandler customAsyncResponehandler;

	public CustomAsyncResponehandler() {
		customAsyncResponehandler = null;
	}

	public CustomAsyncResponehandler(
			CustomAsyncResponehandler customAsyncResponehandler) {
		if (customAsyncResponehandler != null)
			this.customAsyncResponehandler = customAsyncResponehandler;
	}

	public void onSuccess(ResponeModel baseModel) {
		if (customAsyncResponehandler != null)
			customAsyncResponehandler.onSuccess(baseModel);
	}

	@Override
	public void onStart() {
		super.onStart();
		if (customAsyncResponehandler != null)
			customAsyncResponehandler.onStart();
	}

	@Override
	public void onFinish() {
		super.onFinish();
		if (customAsyncResponehandler != null)
			customAsyncResponehandler.onFinish();
	}

	@Override
	public void onFailure(Throwable error, String content) {
		super.onFailure(error, content);
		if (customAsyncResponehandler != null)
			customAsyncResponehandler.onFailure(error, content);
	}
}
