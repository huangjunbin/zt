package com.core.zt.net;

/**
 * 业务数据URL地址（注意标注地址注释）
 *
 * @author allen
 * @version 1.0.0
 * @created 2014-7-10
 */
public class Urls {
    public static final boolean TEST_MODE = true;// 当前连接服务器模式，测试模式还是产线模式
    /**
     * 默认的API头地址
     */
    public static final String TEST_HEAD_URL = "http://120.24.97.63:9090/zhitang/api/";
    public static final String ONLINE_HEAD_URL = "http://120.24.97.63:9090/zhitang/api/";
    public static final String HEAD_URL = TEST_MODE ? TEST_HEAD_URL
            : ONLINE_HEAD_URL;
    public static final String TEST_IMAGE_URL = "http://120.24.97.63:9090/zhitang/";
    public static final String ONLINE_IMAGE_URL = "http://120.24.97.63:9090/zhitang/";
    public static final String IMAGE_URL = TEST_MODE ? TEST_IMAGE_URL
            : ONLINE_IMAGE_URL;
    // 示例
    public static final String example_action = HEAD_URL + "example.action";

    //uploadFile 上传文件
    public static final String uploadFile = HEAD_URL + "appUser/uploadFile";

    // 登陆
    public static final String userLogin = HEAD_URL + "appUser/userLogin";

    //appUser/getDiabetesType糖尿病类型
    public static final String getDiabetesType = HEAD_URL + "appUser/getDiabetesType";

    //并发情况
    public static final String getConcurrentSituation = HEAD_URL + "appUser/getConcurrentSituation";

    //appUser/getHealthyCondition健康情况
    public static final String getHealthyCondition = HEAD_URL + "appUser/getHealthyCondition";

    //appUser/getDiabetesTreatment血糖治疗方案
    public static final String getDiabetesTreatment = HEAD_URL + "appUser/getDiabetesTreatment";

    //appUser/updateUser 修改个人信息
    public static final String updateUser = HEAD_URL + "appUser/updateUser";

    //appUser/getCaseHistory 获取病例
    public static final String getCaseHistory = HEAD_URL + "appUser/getCaseHistory";

    //appUser/updateCaseHistory 修改病例

    public static final String updateCaseHistory = HEAD_URL + "appUser/updateCaseHistory";

    //record/getKpisRecord 获取记录列表
    public static final String getKpisRecord = HEAD_URL + "record/getKpisRecord";

    //record/updateKpisRecord 添加记录
    public static final String updateKpisRecord = HEAD_URL + "record/updateKpisRecord";

    //lesson/getSeries 获取课程列表
    public static final String getSeries = HEAD_URL + "lesson/getSeries";

    //lesson/getLesson 获取文章列表
    public static final String getLesson = HEAD_URL + "lesson/getLesson";

    //获得特栏列表 lesson/getSpecialBar
    public static final String getSpecialBar = HEAD_URL + "lesson/getSpecialBar";

    //lesson/updateBrowseCnt 浏览文章
    public static final String updateBrowseCnt = HEAD_URL + "lesson/updateBrowseCnt";

    //lesson/updateAcceptCnt 赞同文章
    public static final String updateAcceptCnt = HEAD_URL + "lesson/updateAcceptCnt";

    //lesson/updateUsefulCnt 点赞有用文章
    public static final String updateUsefulCnt = HEAD_URL + "lesson/updateUsefulCnt";

    //删除记录record/deleteKpisRecord
    public static final String deleteKpisRecord = HEAD_URL + "record/deleteKpisRecord";

    //appUser/getPlayPic 获得轮播列表
    public static final String getPlayPic = HEAD_URL + "appUser/getPlayPic";

    //appUser/updatePwd 修改密码
    public static final String updatePwd = HEAD_URL + "appUser/updatePwd";

    //appUser/addFeedback 意见反馈
    public static final String addFeedback = HEAD_URL + "appUser/addFeedback";

    //appUser/getVerifyCode 注册获得验证码
    public static final String getVerifyCode = HEAD_URL + "appUser/getVerifyCode";

    //appUser/userRegister 注册
    public static final String userRegister = HEAD_URL + "appUser/userRegister";

    //appUser/backPwd 找回密码
    public static final String backPwd = HEAD_URL + "appUser/backPwd";

    //监测版本更新 appUser/getAppVersion
    public static final String getAppVersion = HEAD_URL + "appUser/getAppVersion";

    //appUser/getYpCategory获取药品分类
    public static final String getYpCategory = HEAD_URL + "appUser/getYpCategory";

    //appUser/getYp 获取药品
    public static final String getYp = HEAD_URL + "appUser/getYp";

    //获取我的记录 record/getMyKpisRecord
    public static final String getMyKpisRecord = HEAD_URL + "record/getMyKpisRecord";

    //获取记录图表 record/getChartKpisRecord

    public static final String getChartKpisRecord = HEAD_URL + "record/getChartKpisRecord";

    //appUser/updatePwdByBackPwd
    public static final String updatePwdByBackPwd = HEAD_URL + "appUser/updatePwdByBackPwd";

    public static final String getMonthKpisRecord = HEAD_URL + "record/getMonthKpisRecord";

    public static final String getNotice = HEAD_URL + "notice/getNotice";

    public static final String getMyDoctor = HEAD_URL + "friend/getMyDoctor";

    public static final String getDoctor = HEAD_URL + "friend/getDoctor";

    public static final String addDoctorGroupFriends=HEAD_URL+"friend/addDoctorGroupFriends";

    //updateMedicationPlan
    public static final String getMedicationPlan = HEAD_URL + "appUser/getMedicationPlan";

    public static final String updateMedicationPlan = HEAD_URL + "appUser/updateMedicationPlan";

    public static final String deleteMedicationPlanByUserId = HEAD_URL + "appUser/deleteMedicationPlanByUserId";

    public static final String deleteMedicationPlan = HEAD_URL + "appUser/deleteMedicationPlan";

    public static final String updateMedicationPlanItem = HEAD_URL + "appUser/updateMedicationPlanItem";

    public static final String deleteMedicationPlanItem = HEAD_URL + "appUser/deleteMedicationPlanItem";

}
