package com.core.zt.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.core.zt.model.Day;
import com.core.zt.model.Notice;
import com.core.zt.model.Week;
import com.core.zt.receiver.AlarmReceiver;

import java.util.Calendar;
import java.util.List;

public class AlarmServer {

    private Context mContext;
    AlarmManager alarmMgr;

    public AlarmServer(Context context) {
        mContext = context;
        alarmMgr = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        ;
    }

    public int getMinFromList(List<Day> dayList) {
        int minDay = 24 * 60;
        int index = 0;
        for (int i = 0; i < dayList.size(); i++) {
            int count = dayList.get(i).hour * 60 + dayList.get(i).minutes;
            if (count <= minDay) {
                index = i;
                minDay = count;
            }
        }
        return index;
    }

    public int getMaxFromList(List<Day> dayList) {
        int minDay = 0;
        int index = 0;
        for (int i = 0; i < dayList.size(); i++) {
            int count = dayList.get(i).hour * 60 + dayList.get(i).minutes;
            if (count > minDay) {
                index = i;
                minDay = count;
            }
        }
        return index;
    }

    public int getNextFromList(List<Day> dayList) {
        Calendar current = Calendar.getInstance();
        int currentNum = current.get(Calendar.HOUR_OF_DAY) * 60 + current.get(Calendar.MINUTE);
        int maxDay = 24 * 60;
        int index = -1;
        for (int i = 0; i < dayList.size(); i++) {
            int count = dayList.get(i).hour * 60 + dayList.get(i).minutes;
            if (count - currentNum >0 && count <= maxDay) {
                index = i;
                maxDay = count;
            }
        }
        return index;
    }

    public int getMinWeekFromList(List<Week> weekList) {
        int minDay = 8;
        int index = 0;
        for (int i = 0; i < weekList.size(); i++) {
            int count = weekList.get(i).week;
            if (count <= minDay) {
                index = i;
                minDay = count;
            }
        }
        return index;
    }

    public int getMaxWeekFromList(List<Week> weekList) {
        int minDay = 0;
        int index = 0;
        for (int i = 0; i < weekList.size(); i++) {
            int count = weekList.get(i).week;
            if (count > minDay) {
                index = i;
                minDay = count;
            }
        }
        return index;
    }

    public int getNextWeekFromList(List<Week> weekList) {
        Calendar current = Calendar.getInstance();
        int day = current.get(Calendar.DAY_OF_WEEK);
        if (day == 1) {
            day = 7;
        } else {
            day = day - 1;
        }
        int maxDay = 7;
        int index = -1;
        for (int i = 0; i < weekList.size(); i++) {
            int count = weekList.get(i).week;
            if (count - day >=0 && count <=maxDay) {
                index = i;
                maxDay = count;
            }
        }
        return index;
    }

    public Calendar setDay(List<Day> dayList, List<Week> weekList) {
        Calendar nextCalendar = Calendar.getInstance();
        Calendar current = Calendar.getInstance();
        int day = current.get(Calendar.DAY_OF_WEEK);
        if (day == 1) {
            day = 7;
        } else {
            day = day - 1;
        }
        System.out.println("day:" + day);
        boolean contact = false;
        if (weekList != null) {
            int minWeekValue = getMinWeekFromList(weekList);
            int maxWeekValue = getMaxWeekFromList(weekList);
            int nextWeekValue = getNextWeekFromList(weekList);
            System.out.println("minWeekValue:" + minWeekValue);
            System.out.println("maxWeekValue:" + maxWeekValue);
            System.out.println("nextWeekValue:" + nextWeekValue);
            if (day <= weekList.get(minWeekValue).week) {//当前周 小于设置最小周
                nextCalendar.add(Calendar.DATE, weekList.get(minWeekValue).week - day);
            } else if (day >= weekList.get(maxWeekValue).week) {//当前周 大于设置最大周
                nextCalendar.add(Calendar.DATE, 7 - (day - weekList.get(minWeekValue).week));
            } else if (nextWeekValue != -1) {//介于时间中间值 取next
                nextCalendar.add(Calendar.DATE, weekList.get(nextWeekValue).week - day);
            }
            for (Week w : weekList) {
                if (w.week == day) {
                    contact = true;
                }
            }
        }
        if (weekList == null || contact) {
            int minValue = getMinFromList(dayList);
            int maxalue = getMaxFromList(dayList);
            int nextValue = getNextFromList(dayList);
            System.out.println("minValue:" + minValue);
            System.out.println("maxalue:" + maxalue);
            System.out.println("nextValue:" + nextValue);
            System.out.println(current.get(Calendar.HOUR_OF_DAY));
            System.out.println(current.get(Calendar.MINUTE));
            System.out.println(current.get(Calendar.HOUR_OF_DAY) * 60 + current.get(Calendar.MINUTE));
            System.out.println((dayList.get(minValue).hour * 60 + dayList.get(minValue).getMinutes()));
            System.out.println((dayList.get(maxalue).hour * 60 + dayList.get(maxalue).getMinutes()));
            if ((current.get(Calendar.HOUR_OF_DAY) * 60 + current.get(Calendar.MINUTE)) <= (dayList.get(minValue).hour * 60 + dayList.get(minValue).getMinutes())) {//时间小于设定最小值
                System.out.println("<:");
                nextCalendar.set(Calendar.HOUR_OF_DAY, dayList.get(minValue).hour);
                nextCalendar.set(Calendar.MINUTE, dayList.get(minValue).minutes);
            } else if ((current.get(Calendar.HOUR_OF_DAY) * 60 + current.get(Calendar.MINUTE)) >= (dayList.get(maxalue).hour * 60 + dayList.get(maxalue).getMinutes())) {//时间大于设定最大值
                System.out.println(">:");
                nextCalendar.set(Calendar.HOUR_OF_DAY, dayList.get(minValue).hour);
                nextCalendar.set(Calendar.MINUTE, dayList.get(minValue).minutes);
                nextCalendar.add(Calendar.DATE, 1);
            }
            if (nextValue != -1) {//介于时间中间值 取next
                nextCalendar.set(Calendar.HOUR_OF_DAY, dayList.get(nextValue).hour);
                nextCalendar.set(Calendar.MINUTE, dayList.get(nextValue).minutes);
            }
        }
        return nextCalendar;
    }

    public void AddAlarm(Notice notice) {
        System.out.println("add:" + notice);
        Intent intent = new Intent(mContext, AlarmReceiver.class);
        int requestCode = notice.getId();
        intent.putExtra("id", notice.getId());
        PendingIntent pendIntent = PendingIntent.getBroadcast(mContext,
                requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar current = Calendar.getInstance();
        int day = current.get(Calendar.DAY_OF_WEEK);
        if (day == 1) {
            day = 7;
        } else {
            day = day - 1;
        }
        Calendar nextCalendar = Calendar.getInstance();
        if (notice.getWeekList() == null || notice.getWeekList().size() <= 0) {//每天
            nextCalendar = setDay(notice.getDateList(), null);
        } else {
            nextCalendar = setDay(notice.getDateList(), notice.getWeekList());
        }
        long triggerAtTime = nextCalendar.getTimeInMillis() - current.getTimeInMillis();
        System.out.println(current.getTime());
        System.out.println(nextCalendar.getTime());
        System.out.println(SystemClock.elapsedRealtime() + triggerAtTime);
        if (triggerAtTime >= 60000) {
            // 5秒后发送广播，只发送一次
            alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + triggerAtTime, pendIntent);
        }
    }

    public void stopAlarm(int noticeId) {
        removeAlarm(noticeId);
    }

    public void startAlarm(Notice notice) {
        AddAlarm(notice);
    }

    public void removeAlarm(int noticeId) {
        int requestCode = noticeId;
        Intent intent = new Intent(mContext, AlarmReceiver.class);
        PendingIntent pendIntent = PendingIntent.getBroadcast(mContext,
                requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmMgr.cancel(pendIntent);
    }
}
