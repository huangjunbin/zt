package com.core.zt.service;

import android.content.Context;

import com.core.zt.app.AppContext;
import com.core.zt.model.Drug;
import com.core.zt.model.DrugType;
import com.core.zt.model.Notice;
import com.core.zt.model.RequestModel;
import com.core.zt.net.Urls;
import com.core.zt.net.http.CustomAsyncHttpClient;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.net.http.RequestParams;
import com.core.zt.util.SPUtil;

import java.util.ArrayList;
import java.util.List;

public class NoticeServer {

    private Context mContext;
    private AlarmServer alarmServer;
    private static CustomAsyncHttpClient httpClient;
    private static NoticeServer mInstance;

    public static NoticeServer getInstance(Context context) {
        httpClient = new CustomAsyncHttpClient(context);
        // 双重锁定
        if (mInstance == null) {
            synchronized (RecordServer.class) {
                if (mInstance == null) {
                    mInstance = new NoticeServer(context);
                }
            }
        }
        return mInstance;
    }

    public NoticeServer(Context context) {
        mContext = context;
        alarmServer = new AlarmServer(mContext);
    }

    public Notice getNotice(int id) {
        List<Notice> noticeList = getNoticeList();
        int j = -1;
        for (int i = 0; i < noticeList.size(); i++) {
            if (noticeList.get(i).getId() == id) {
                j = i;
            }
        }
        if (j != -1) {
            return noticeList.get(j);
        } else {
            return null;
        }
    }

    public void saveNotice(Notice notice) {
        notice.setId((int) System.currentTimeMillis());
        List<Notice> noticeList = getNoticeList();
        noticeList.add(0, notice);
        SPUtil.saveObjectToShare(mContext, AppContext.currentUser.getUserId() + "", noticeList);
        alarmServer.AddAlarm(notice);
    }

    public void startNotice(Notice notice) {
        notice.setState(1);
        List<Notice> noticeList = getNoticeList();
        int j = -1;
        for (int i = 0; i < noticeList.size(); i++) {
            if (noticeList.get(i).getId() == notice.getId()) {
                j = i;
            }
        }
        if (j != -1) {
            noticeList.remove(j);
            noticeList.add(j, notice);
            SPUtil.saveObjectToShare(mContext, AppContext.currentUser.getUserId() + "", noticeList);
        }
        alarmServer.startAlarm(notice);
    }

    public void stopNotice(Notice notice) {
        notice.setState(0);
        List<Notice> noticeList = getNoticeList();
        int j = -1;
        for (int i = 0; i < noticeList.size(); i++) {
            if (noticeList.get(i).getId() == notice.getId()) {
                j = i;
            }
        }
        if (j != -1) {
            noticeList.remove(j);
            noticeList.add(j, notice);
            SPUtil.saveObjectToShare(mContext, AppContext.currentUser.getUserId() + "", noticeList);
        }
        alarmServer.stopAlarm(notice.getId());
    }

    public void removeNotice(Notice notice) {
        List<Notice> noticeList = getNoticeList();
        int j = -1;
        for (int i = 0; i < noticeList.size(); i++) {
            if (noticeList.get(i).getId() == notice.getId()) {
                j = i;
            }
        }
        if (j != -1) {
            noticeList.remove(j);
            SPUtil.saveObjectToShare(mContext, AppContext.currentUser.getUserId() + "", noticeList);
        }
        alarmServer.removeAlarm(notice.getId());
    }

    public void updateNotice(Notice notice) {
        removeNotice(notice);
        saveNotice(notice);
    }

    public List<Notice> getNoticeList() {
        try {
            List<Notice> noticeList = SPUtil.getObjectFromShare(mContext, AppContext.currentUser.getUserId() + "");
            if (noticeList != null) {
                return noticeList;
            } else {
                return new ArrayList<>();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void addNotices() {
        List<Notice> noticeList = getNoticeList();
        for (Notice notice : noticeList) {
            if (notice.getState() == 1) {
                startNotice(notice);
            }
        }
    }

    public void removeNotices() {
        List<Notice> noticeList = getNoticeList();
        for (Notice notice : noticeList) {
            alarmServer.stopAlarm(notice.getId());
        }
    }

    public void getDrugType(CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(DrugType.class);
        RequestParams params = new RequestParams();
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getYpCategory);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /*
    drugName		String	药品名称
drugChemicalCategoryCode		String	分类编码
     */
    public void getDrug(String drugName, String drugChemicalCategoryCode, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Drug.class);
        RequestParams params = new RequestParams();
        params.put("drugName", drugName);
        params.put("drugChemicalCategoryCode", drugChemicalCategoryCode);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getYp);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }
}
