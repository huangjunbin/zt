package com.core.zt.service;

import android.content.Context;
import android.util.Log;

import com.core.zt.app.AppContext;
import com.core.zt.db.dao.UserDao;
import com.core.zt.model.Case;
import com.core.zt.model.RequestModel;
import com.core.zt.model.ResponeModel;
import com.core.zt.model.Type;
import com.core.zt.model.User;
import com.core.zt.net.Urls;
import com.core.zt.net.http.CustomAsyncHttpClient;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.net.http.RequestParams;
import com.core.zt.net.md5.MD5;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;

import java.io.File;
import java.io.FileNotFoundException;


public class UserServer {
    private String TAG = "UserService";
    private boolean isLog = true;
    private boolean LogSwitch = true;

    private static CustomAsyncHttpClient httpClient;
    private static UserServer mInstance;
    public static UserDao userDao;
    private User user;
    private static NoticeServer server;
    public static boolean isLoginIngHX;
    public static UserServer getInstance(Context context) {
        httpClient = new CustomAsyncHttpClient(context);
        userDao = new UserDao(context);
        server = new NoticeServer(context);
        // 双重锁定
        if (mInstance == null) {
            synchronized (UserServer.class) {
                if (mInstance == null) {
                    mInstance = new UserServer();
                }
            }
        }
        return mInstance;
    }


    /**
     * 登录
     */
    public void login(final String userName, final String password, final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("userName", userName);
        params.put("password", password);
        requestModel.setParams(params);
        requestModel.setCls(User.class);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.userLogin);
        httpClient.post(requestModel, new CustomAsyncResponehandler() {
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);

                if (baseModel != null && baseModel.isStatus()) {
                    AppContext.currentUser = (User) baseModel.getResultObj();
                    AppContext.currentUser.setUserName(userName);
                    if (userDao != null) {
                        userDao.delete();
                        userDao.insert(AppContext.currentUser);
                    }
                    server.addNotices();
                }
                handler.onSuccess(baseModel);
            }
        });
    }

    public void getType(int type, String userId,
                        CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Type.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        requestModel.setParams(params);
        if (type == 1) {
            requestModel.setUrl(Urls.getDiabetesType);
        }
        if (type == 2) {
            requestModel.setUrl(Urls.getConcurrentSituation);
        }
        if (type == 3) {
            requestModel.setUrl(Urls.getHealthyCondition);
        }
        if (type == 4) {
            requestModel.setUrl(Urls.getDiabetesTreatment);
        }
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /*
    userId	必须	String	id
    nickName	必须	String	姓名
    email		String	邮件
    mobile		String	手机号
    sex		Boolean	 True 男 false 女
    phone		String	电话
    mobile		String	手机号
    birthday		String	生日 格式 :1979-11-11
    area		String	地区名称
    age		Integer	年龄
    remarks		String	备注
    currentWeight		Integer	当前体重
    maxWeigth		Integer	最高体重
    labourIntensity		String	劳动强度
    photo		String	头像图片路径
     */
    public void updateInfo(User user,
                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(User.class);
        RequestParams params = new RequestParams();
        requestModel.setShowErrorMessage(true);
        params.put("userId", user.getUserId());
        params.put("nickName", user.getNickName());
        params.put("realName", user.getRealName());
        params.put("email", user.getEmail());
        params.put("sex", user.getSex() + "");
        params.put("phone", user.getPhone());
        params.put("mobile", user.getMobile());
        params.put("area", user.getArea());
        params.put("age", user.getAge() + "");
        params.put("currentWeight", user.getCurrentWeight() + "");
        params.put("maxWeigth", user.getMaxWeigth() + "");
        params.put("labourIntensity", user.getLabourIntensity());
        params.put("photo", user.getPhoto());
        params.put("birthday", user.getBirthday());
        requestModel.setParams(params);
        requestModel.setUrl(Urls.updateUser);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }


    public void getCase(String userId,
                        CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Case.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getCaseHistory);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /*
    diabetesType		String	糖尿病类型
    concurrentType		String	并发类型
    healthType		String	健康类型
    solutionType		String	治疗方案类型
    userId	必填	String	用户ID
    doctorId		String	看病的医生
    diagnosisTime		String	确诊时间
    diagnosisType		String	确诊类型（0：表示预约；1：表示确诊）
     */
    public void updateCase(Case item,
                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Case.class);
        RequestParams params = new RequestParams();
        params.put("diabetesType", item.getDiabetesType());
        params.put("concurrentType", item.getConcurrentType());
        params.put("healthType", item.getHealthType());
        params.put("solutionType", item.getSolutionType());
        params.put("userId", AppContext.currentUser.getUserId());
        params.put("doctorId", item.getDoctorId());
        params.put("diagnosisTime", item.getDiagnosisTime());
        params.put("diagnosisType", item.getDiagnosisType());
        requestModel.setParams(params);
        requestModel.setUrl(Urls.updateCaseHistory);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }


    /**
     * 上传文件
     *
     * @param file 文件
     */
    public void uploadImg(File file, final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        try {
            params.put("fileData", file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        requestModel.setParams(params);
        requestModel.setUrl(Urls.uploadFile);
        httpClient.post(requestModel, handler);
    }

    /*
    userId	必须	String	用户id
    oldPwd	必须	String	旧密码
    newPwd	必须	String	新密码
     */
    public void updatePwd(String userId, String oldPwd, String newPwd,
                          CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setShowErrorMessage(true);
        requestModel.setCls(Case.class);
        RequestParams params = new RequestParams();
        if (AppContext.currentUser != null) {
            params.put("userId", userId);
        } else {
            params.put("mobile", userId);
        }
        MD5 m = new MD5(oldPwd);
        params.put("oldPwd", oldPwd);
        params.put("newPwd", newPwd);
        requestModel.setParams(params);
        if (AppContext.currentUser != null) {
            requestModel.setUrl(Urls.updatePwd);
        } else {//updatePwdByBackPwd
            requestModel.setUrl(Urls.updatePwdByBackPwd);
        }
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /*
         title		String	标题
        opinion		String	内容
 */
    public void addFeedback(String title, String opinion,
                            CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setShowErrorMessage(true);
        requestModel.setCls(Case.class);
        RequestParams params = new RequestParams();
        params.put("title", title);
        params.put("opinion", opinion);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.addFeedback);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getVerifyCode(String mobile,
                              CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setShowErrorMessage(true);
        requestModel.setCls(Case.class);
        RequestParams params = new RequestParams();
        params.put("mobile", mobile);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getVerifyCode);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void backPwd(String mobile,
                        CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setShowErrorMessage(true);
        requestModel.setCls(Case.class);
        RequestParams params = new RequestParams();
        params.put("mobile", mobile);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.backPwd);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /*
    userName:'us229',password:'123456',nickName:'444'
     */
    public void userRegister(String userName, String password, String nickName,
                             CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(User.class);
        requestModel.setShowErrorMessage(true);
        RequestParams params = new RequestParams();
        params.put("userName", userName);
        params.put("nickName", nickName);
        params.put("password", password);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.userRegister);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }
    /**
     * 登陆环信
     *
     * @param mobileNumber
     * @param password
     */
    public void loginHX(final String mobileNumber, final String password, final EMCallBack callback) {
        MD5 md5 = new MD5();
        md5.Update(password);
        final String mPassword = md5.asHex();
        AppContext.getApplication().setPassword(mPassword);
        Log.d("HX", mobileNumber + "_____" + mPassword);
        // 调用sdk登陆方法登陆聊天服务器
        new Thread(new Runnable() {
            public void run() {
                isLoginIngHX = true;

                EMChatManager.getInstance().login(mobileNumber, mPassword,

                        callback);
            }
        }).start();
    }
    public User getCacheUser() {
        user = userDao.getUserByDefault();
        return user;
    }

    public void clearCacheUser() {
        userDao.delete();
    }

    public void updateUser(User user) {
        if (userDao != null) {
            userDao.delete();
            userDao.insert(user);
        }
    }
}
