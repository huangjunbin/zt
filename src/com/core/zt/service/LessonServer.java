package com.core.zt.service;

import android.content.Context;

import com.core.zt.app.AppContext;
import com.core.zt.model.Advert;
import com.core.zt.model.Lesson;
import com.core.zt.model.Notification;
import com.core.zt.model.RequestModel;
import com.core.zt.model.Series;
import com.core.zt.net.Urls;
import com.core.zt.net.http.CustomAsyncHttpClient;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.net.http.RequestParams;


public class LessonServer {
    private String TAG = "ExampleService";
    private boolean LogSwitch = true;

    private static  CustomAsyncHttpClient httpClient;
    private static LessonServer mInstance;

    private LessonServer(Context context) {

    }

    public static LessonServer getInstance(Context context) {
        httpClient = new CustomAsyncHttpClient(context);
        // 双重锁定
        if (mInstance == null) {
            synchronized (LessonServer.class) {
                if (mInstance == null) {
                    mInstance = new LessonServer(context);
                }
            }
        }
        return mInstance;
    }

    public void getSeries(CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Series.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getSeries);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /*
    seriesId	必须	String	课程id
    pageNumber		Integer	第几页
    pageSize		Integer	页数
     */
    public void getLesson(String seriesId, String pageNumber, String pageSize, int type, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Lesson.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        params.put("seriesId", seriesId);
        params.put("pageNumber", pageNumber);
        params.put("pageSize", pageSize);
        params.put("userId", AppContext.currentUser.getUserId());
        requestModel.setParams(params);
        if (type == 0) {
            requestModel.setUrl(Urls.getLesson);
        } else {
            requestModel.setUrl(Urls.getSpecialBar);
        }
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void editLesson(String lessonId,int type, boolean show,CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Lesson.class);
        requestModel.setShowDialog(true);
        requestModel.setShowErrorMessage(show);
        RequestParams params = new RequestParams();
        params.put("lessonId", lessonId);
        params.put("userId", AppContext.currentUser.getUserId());
        requestModel.setParams(params);
        if (type == 0) {
            requestModel.setUrl(Urls.updateBrowseCnt);
        }
        if (type == 1) {
            requestModel.setUrl(Urls.updateAcceptCnt);
        }
        if (type == 2) {
            requestModel.setUrl(Urls.updateUsefulCnt);
        }
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getPlayPic(CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Advert.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getPlayPic);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getNotification( String pageNumber, String pageSize, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Notification.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        params.put("userType", "1");
        params.put("pageNumber", pageNumber);
        params.put("pageSize", pageSize);
        params.put("ztId", AppContext.currentUser.getUserId());
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getNotice);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }
}
