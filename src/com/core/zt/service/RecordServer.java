package com.core.zt.service;

import android.content.Context;

import com.core.zt.app.AppContext;
import com.core.zt.model.MyRecord;
import com.core.zt.model.Record;
import com.core.zt.model.RequestModel;
import com.core.zt.net.Urls;
import com.core.zt.net.http.CustomAsyncHttpClient;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.net.http.RequestParams;

public class RecordServer {
    private String TAG = "RecordServer";
    private boolean LogSwitch = true;

    private static CustomAsyncHttpClient httpClient;
    private static RecordServer mInstance;

    private RecordServer(Context context) {

    }

    public static RecordServer getInstance(Context context) {
        httpClient = new CustomAsyncHttpClient(context);
        // 双重锁定
        if (mInstance == null) {
            synchronized (RecordServer.class) {
                if (mInstance == null) {
                    mInstance = new RecordServer(context);
                }
            }
        }
        return mInstance;
    }

    /*
    userId	必填	String	App用户id
    logType		String	记录类型（1：血糖；2：血压；3：BMI）
     */
    public void getRecord(String userId, String logType,
                          CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Record.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("logType", logType);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getKpisRecord);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * logTime		String	记录日期
     * logType		String	记录类型（1：血糖；2：血压；3：BMI）
     * logVal1		String	当类型为血糖 填写 血糖数值
     * 当类型为血压 填写 高压数值
     * 当类型为BMI 填写 BMI
     * logVal2		String	当类型为血压 填写 抵押数值
     * 当类型为BMI 填写 身高
     * logVal3		String	当类型为BMI 填写 体重
     */
    public void saveRecord(Record record,
                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Record.class);
        RequestParams params = new RequestParams();
        if (record.getId() != null) {
            params.put("kpisRecordId", record.getId());
        }
        params.put("userId", AppContext.currentUser.getUserId());
        params.put("logTime", record.getLogTime());
        params.put("logType", record.getLogType());
        params.put("logVal1", record.getLogVal1());
        params.put("logVal2", record.getLogVal2());
        params.put("logVal3", record.getLogVal3());
        requestModel.setParams(params);
        requestModel.setUrl(Urls.updateKpisRecord);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public String getSugarTime(String index) {
        if ("0".equals(index)) {
            return "早餐前";
        }
        if ("1".equals(index)) {
            return "早餐后";
        }
        if ("2".equals(index)) {
            return "凌晨";
        }
        if ("3".equals(index)) {
            return "午餐前";
        }
        if ("4".equals(index)) {
            return "午餐后";
        }
        if ("5".equals(index)) {
            return "睡前";
        }
        if ("6".equals(index)) {
            return "晚餐前";
        }
        if ("7".equals(index)) {
            return "晚餐后";
        }
        if ("8".equals(index)) {
            return "随机";
        }
        return "";
    }

    public void removeRecord(String kpisRecordId,
                             CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("kpisRecordId", kpisRecordId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.deleteKpisRecord);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getMyRecord(String userId,
                            CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(MyRecord.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getMyKpisRecord);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /*
    logType	必填	String	记录类型（1：血糖；2：血压；3：BMI 4,化学单）如果是0 返回所有记录
    day	必填	String	天数 1 7天
    2 30天
    3 60天
    4 90天
    5 120天
    6 180天
     */
    public void getChatRecord(String userId,String logType,String day,
                            CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Record.class);
        requestModel.setShowDialog(true);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("logType", logType);
        params.put("day", day);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getChartKpisRecord);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getChatRecord2(String userId,String logType,String day,String logVal2,
                              CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Record.class);
        requestModel.setShowDialog(true);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("logType", logType);
        params.put("day", day);
        params.put("logVal2", logVal2);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getChartKpisRecord);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getMonthKpisRecord(String userId,String logType,String year,String month,
                              CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Record.class);
        requestModel.setShowDialog(true);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("logType", logType);
        params.put("year", year);
        params.put("month", month);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getMonthKpisRecord);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }
}
