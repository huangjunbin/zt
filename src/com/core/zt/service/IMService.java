package com.core.zt.service;

import android.content.Context;
import android.text.TextUtils;

import com.core.zt.app.AppConfig;
import com.core.zt.app.AppContext;
import com.core.zt.model.Doctor;
import com.core.zt.model.Group;
import com.core.zt.model.GroupMember;
import com.core.zt.model.RequestModel;
import com.core.zt.model.ResponeModel;
import com.core.zt.model.chat.User;
import com.core.zt.net.Urls;
import com.core.zt.net.http.CustomAsyncHttpClient;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.net.http.RequestParams;
import com.easemob.chat.EMGroupManager;
import com.easemob.exceptions.EaseMobException;
import com.easemob.util.HanziToPinyin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IMService {
    private static CustomAsyncHttpClient httpClient;
    static IMService mInstance;

    public static IMService getInstance(Context context) {
        httpClient = new CustomAsyncHttpClient(context);
        if (mInstance == null) {
            // 只有第一次才彻底执行这里的代码
            synchronized (IMService.class) {
                // 再检查一次
                if (mInstance == null)
                    mInstance = new IMService();
            }
        }
        return mInstance;
    }

    /**
     * 获取好友列表
     */
    public void getFriendsList(String moblieNumber,
                               final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("userId", moblieNumber);
        requestModel.setParams(params);
        requestModel.setCls(Doctor.class);
        requestModel.setShowDialog(false);
        requestModel.setShowErrorMessage(false);
        requestModel.setUrl(Urls.getMyDoctor);
        httpClient.post(requestModel, handler);
        new Thread() {
            public void run() {
                // 存储所有群组
                try {
                    EMGroupManager.getInstance().getGroupsFromServer();
                } catch (EaseMobException e) {
                    e.printStackTrace();
                }
            }

            ;
        }.start();
    }

    /**
     * 获取好友列表S
     */
    public void getFriendsListS(String moblieNumber,
                                final CustomAsyncResponehandler handler, boolean show) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("userId", moblieNumber);
        requestModel.setParams(params);
        requestModel.setCls(Doctor.class);
        requestModel.setShowDialog(show);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.getMyDoctor);
        httpClient.post(requestModel, new CustomAsyncResponehandler() {

            @Override
            public void onFailure(Throwable error, String content) {
                super.onFailure(error, content);
                handler.onFailure(error, content);
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(ResponeModel baseModel) {
                if (baseModel.isStatus()) {
                    List<Doctor> memberList = (List<Doctor>) baseModel
                            .getResultObj();
                    Map<String, User> userlist = new HashMap<String, User>();
                    if (memberList != null) {
                        for (Doctor member : memberList) {
                            User user = new User();
                            user.setUsername(member.getDoctorName());
                            setUserHearder(member.getDoctorName(), user);
                            user.setId(member.getUserName());
                            user.setUrl(member.getPhoto());
                            userlist.put(member.getUserName() + "", user);
                        }

                        // 添加user"申请与通知"
                        User newFriends = new User();
                        newFriends.setUsername(AppConfig.NEW_FRIENDS_USERNAME);
                        newFriends.setNick("申请与通知");
                        newFriends.setHeader("");
                        userlist.put(AppConfig.NEW_FRIENDS_USERNAME, newFriends);

                        // 存入内存
                        AppContext.getApplication().setContactList(userlist);
                    }
                }
                handler.onSuccess(baseModel);
            }
        });
    }

    /**
     * 搜索好友
     */
    public void searchFriends(String keywordName,
                              final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("keywordName", keywordName);
        requestModel.setParams(params);
        requestModel.setCls(Doctor.class);
        requestModel.setShowDialog(true);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.getDoctor);
        httpClient.post(requestModel, handler);
    }

    /**
     *同意
     */
    public void agressFriends(String doctorId,String userId,
                              final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("doctorId", doctorId);
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setCls(Doctor.class);
        requestModel.setShowDialog(true);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.addDoctorGroupFriends);
        httpClient.post(requestModel, handler);
    }


    public void setUserHearder(String username, User user) {
        String headerName = null;
        if (!TextUtils.isEmpty(user.getNick())) {
            headerName = user.getNick();
        } else {
            headerName = user.getUsername();
        }
        if (username.equals(AppConfig.NEW_FRIENDS_USERNAME)) {
            user.setHeader("");
        } else if (Character.isDigit(headerName.charAt(0))) {
            user.setHeader("#");
        } else {
            System.out.println(HanziToPinyin.getInstance()
                    .get(headerName.substring(0, 1)));
            if (HanziToPinyin.getInstance()
                    .get(headerName.substring(0, 1))==null||HanziToPinyin.getInstance()
                    .get(headerName.substring(0, 1)).size()==0||HanziToPinyin.getInstance()
                    .get(headerName.substring(0, 1)).get(0) == null) {
                user.setHeader("#");
            } else {
                user.setHeader(HanziToPinyin.getInstance()
                        .get(headerName.substring(0, 1)).get(0).target.substring(0,
                                1).toUpperCase());
            }
            char header = user.getHeader().toLowerCase().charAt(0);
            if (header < 'a' || header > 'z') {
                user.setHeader("#");
            }
        }
    }

    /**
     * 根据组id获取成员
     */
    public void getGroupMember(String groupId,
                               final CustomAsyncResponehandler handler, boolean show) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("groupID", groupId);
        requestModel.setParams(params);
        requestModel.setCls(GroupMember.class);
        requestModel.setShowDialog(show);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.HEAD_URL);
        httpClient.post(requestModel, new CustomAsyncResponehandler() {

            @Override
            public void onFailure(Throwable error, String content) {
                super.onFailure(error, content);
                handler.onFailure(error, content);
            }

            @Override
            public void onSuccess(ResponeModel baseModel) {

                handler.onSuccess(baseModel);
            }
        });

    }

    /**
     * 根据操盘手ID获取组的ID
     */
    public void getGroupByTradeId(String treadeID,
                                  final CustomAsyncResponehandler handler, boolean show) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("traderID", treadeID);
        requestModel.setParams(params);
        requestModel.setCls(Group.class);
        requestModel.setShowDialog(show);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.HEAD_URL);
        httpClient.post(requestModel, new CustomAsyncResponehandler() {

            @Override
            public void onFailure(Throwable error, String content) {
                super.onFailure(error, content);
                handler.onFailure(error, content);
            }

            @Override
            public void onSuccess(ResponeModel baseModel) {

                handler.onSuccess(baseModel);
            }
        });


    }
}
