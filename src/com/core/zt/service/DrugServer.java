package com.core.zt.service;

import android.content.Context;

import com.core.zt.model.DrugItem;
import com.core.zt.model.Record;
import com.core.zt.model.RequestModel;
import com.core.zt.net.Urls;
import com.core.zt.net.http.CustomAsyncHttpClient;
import com.core.zt.net.http.CustomAsyncResponehandler;
import com.core.zt.net.http.RequestParams;

public class DrugServer {
    private String TAG = "RecordServer";
    private boolean LogSwitch = true;

    private static CustomAsyncHttpClient httpClient;
    private static DrugServer mInstance;

    private DrugServer(Context context) {

    }

    public static DrugServer getInstance(Context context) {
        httpClient = new CustomAsyncHttpClient(context);
        // 双重锁定
        if (mInstance == null) {
            synchronized (DrugServer.class) {
                if (mInstance == null) {
                    mInstance = new DrugServer(context);
                }
            }
        }
        return mInstance;
    }

    /*
    userId	必填	String	App用户id
    logType		String	记录类型（1：血糖；2：血压；3：BMI）
     */
    public void getMedicationPlan(String userId,
                                  CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(DrugItem.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getMedicationPlan);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }


    /**
     * id		String	id
     * userId		String	用户id
     * createDate		String	创建日期
     * remarks		String	备注
     * drudId		Integer	药品id
     * doses		String	剂量数组 以;隔开
     * medicationTimes		String	用药时间数组 以;隔开
     */
    public void updateMedicationPlan(String id, String userId, String drudId, String doses, String medicationTimes,
                                     CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Record.class);
        requestModel.setShowDialog(true);
        RequestParams params = new RequestParams();
        if (!"".equals(id)) {
            params.put("planId", id);
        }
        params.put("userId", userId);
        params.put("drudId", drudId);
        params.put("doses", doses);
        params.put("medicationTimes", medicationTimes);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.updateMedicationPlan);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void deleteMedicationPlanItem(String planItemId,
                             CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("planId", planItemId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.deleteMedicationPlan);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void deleteMedicationPlan(String userId,
                                         CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.deleteMedicationPlanByUserId);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }
}
