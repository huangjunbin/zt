package com.core.zt.model;

public class AppInfo {
	private String	appName		;//名称
	private String	appTitle	;//标题
	private String	appType		;//类型 1为android
	private String	packageUrl	;//下载路径
	private int version		 ;//	版本号
	private String	remarks	;//备注

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppTitle() {
		return appTitle;
	}

	public void setAppTitle(String appTitle) {
		this.appTitle = appTitle;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getPackageUrl() {
		return packageUrl;
	}

	public void setPackageUrl(String packageUrl) {
		this.packageUrl = packageUrl;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "AppInfo{" +
				"appName='" + appName + '\'' +
				", appTitle='" + appTitle + '\'' +
				", appType='" + appType + '\'' +
				", packageUrl='" + packageUrl + '\'' +
				", version=" + version +
				", remarks='" + remarks + '\'' +
				'}';
	}
}
