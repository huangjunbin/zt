package com.core.zt.model;


import com.core.zt.util.DateUtil;

import java.io.Serializable;

public class Day implements Serializable {
    public int hour;
    public int minutes;

    public Day(int s,int s1){
        hour=s;
        minutes=s1;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public String getDay(){
        return DateUtil.getZero(hour)+":"+DateUtil.getZero(minutes);
    }
    @Override
    public String toString() {
        return "Day{" +
                "hour='" + hour + '\'' +
                ", minutes='" + minutes + '\'' +
                '}';
    }
}
