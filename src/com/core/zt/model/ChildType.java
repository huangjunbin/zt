package com.core.zt.model;

import java.io.Serializable;

public class ChildType implements Serializable {

    private static final long serialVersionUID = 1L;
    private String sonSortId;
    private String name;
    public String getSonSortId() {
        return sonSortId;
    }

    public void setSonSortId(String sonSortId) {
        this.sonSortId = sonSortId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}