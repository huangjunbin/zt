package com.core.zt.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DrugChildItem implements Serializable {

    private String planItemId;//	用药Id
    private String planId;//	方案id
    private int dose;//
    private String medicationTime;//	时间
    private String medicationMethod;//	用药方法
    private int index=0;

    public String getPlanItemId() {
        return planItemId;
    }

    public void setPlanItemId(String planItemId) {
        this.planItemId = planItemId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public int getDose() {
        return dose;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }

    public String getMedicationTime() {
        return medicationTime;
    }

    public void setMedicationTime(String medicationTime) {
        this.medicationTime = medicationTime;
    }

    public String getMedicationMethod() {
        return medicationMethod;
    }

    public void setMedicationMethod(String medicationMethod) {
        this.medicationMethod = medicationMethod;
    }

    public static boolean isContant(List<DrugChildItem> list) {
        if (list == null) {
            return true;
        } else {
            List<String> str = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                DrugChildItem item = list.get(i);
                if (str.contains(item.getMedicationTime())) {
                    return true;
                } else {
                    str.add(item.getMedicationTime());
                }
            }
            return false;
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "DrugChildItem{" +
                "planItemId='" + planItemId + '\'' +
                ", planId='" + planId + '\'' +
                ", dose=" + dose +
                ", medicationTime='" + medicationTime + '\'' +
                ", medicationMethod='" + medicationMethod + '\'' +
                ", index=" + index +
                '}';
    }
}
