package com.core.zt.model;

import java.io.Serializable;

public class Notification implements Serializable {

	private String id	;//通知id
	private String ztId		;//用户id 或者医生id
	private String userType		;//	1 用户2 医生
	private String createUserName		;//发送者
	private String title		;//	标题
	private String content		;//内容
	private String isRead		;//1 未读 0 已读
	private String createDate		;//	创建时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getZtId() {
		return ztId;
	}

	public void setZtId(String ztId) {
		this.ztId = ztId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIsRead() {
		return isRead;
	}

	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Notification{" +
				"id='" + id + '\'' +
				", ztId='" + ztId + '\'' +
				", userType='" + userType + '\'' +
				", createUserName='" + createUserName + '\'' +
				", title='" + title + '\'' +
				", content='" + content + '\'' +
				", isRead='" + isRead + '\'' +
				", createDate='" + createDate + '\'' +
				'}';
	}
}
