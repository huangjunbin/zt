package com.core.zt.model;


import com.core.zt.util.DateUtil;

import java.io.Serializable;
import java.util.Date;

public class Record implements Serializable{
    private String id;
    private String unit;
    private String title;
    private String logTime;//记录日期
    private String logType;//记录类型（1：血糖；2：血压；3：BMI）
    private String logVal1;//当类型为血糖 填写 血糖数值 当类型为血压 填写 高压数值 当类型为BMI 填写 BMI
    private String logVal2;//当类型为血压 填写 抵押数值 当类型为BMI 填写 身高
    private String logVal3;//当类型为BMI 填写 体重

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public String getLogTime() {
        return logTime;
    }

    public String getLogType() {
        return logType;
    }

    public String getLogVal1() {
        return logVal1;
    }

    public String getLogVal2() {
        return logVal2;
    }

    public String getLogVal3() {
        return logVal3;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public void setLogVal1(String logVal1) {
        this.logVal1 = logVal1;
    }

    public void setLogVal2(String logVal2) {
        this.logVal2 = logVal2;
    }

    public void setLogVal3(String logVal3) {
        this.logVal3 = logVal3;
    }

    public String getMD(){
        Date date=DateUtil.parseDate(logTime,"yyyy-MM-dd HH:mm:ss");
        String str="("+DateUtil.getMonth(date)+"-"+DateUtil.getDay(date)+")";
        return  str;
    }
    public String getDate(){
        Date date=DateUtil.parseDate(logTime,"yyyy-MM-dd HH:mm:ss");
        String str=DateUtil.getDate(date);
        return  str;
    }
    public String getTime(){
        Date date=DateUtil.parseDate(logTime,"yyyy-MM-dd HH:mm:ss");
        String str=DateUtil.getTime(date);
        return  str;
    }
    @Override
    public String toString() {
        return "Record{" +
                "id='" + id + '\'' +
                ", unit='" + unit + '\'' +
                ", title='" + title + '\'' +
                ", logTime='" + logTime + '\'' +
                ", logType='" + logType + '\'' +
                ", logVal1='" + logVal1 + '\'' +
                ", logVal2='" + logVal2 + '\'' +
                ", logVal3='" + logVal3 + '\'' +
                '}';
    }
}
