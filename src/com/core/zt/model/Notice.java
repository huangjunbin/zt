package com.core.zt.model;

import com.core.zt.util.DateUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Notice implements Serializable{
    private int id;
    private String name;
    private List<Day> dateList;
    private List<Week> weekList;
    private boolean isNotice;
    private int type;
    private String remark;
    private List<Drug> drugList;
    private int state;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNotice() {
        return isNotice;
    }

    public void setIsNotice(boolean isNotice) {
        this.isNotice = isNotice;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<Day> getDateList() {
        return dateList;
    }

    public void setDateList(List<Day> dateList) {
        this.dateList = dateList;
    }

    public List<Week> getWeekList() {
        return weekList;
    }

    public void setWeekList(List<Week> weekList) {
        this.weekList = weekList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public List<Drug> getDrugList() {
        return drugList;
    }

    public void setDrugList(List<Drug> drugList) {
        this.drugList = drugList;
    }

    @Override
    public String toString() {
        return "Notice{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateList=" + dateList +
                ", weekList=" + weekList +
                ", isNotice=" + isNotice +
                ", type=" + type +
                ", remark='" + remark + '\'' +
                ", drugList=" + drugList +
                ", state=" + state +
                '}';
    }

    public List<Calendar> getCalendarList() {
        Calendar currentCalendar = Calendar.getInstance();
        List<Calendar> calendarList = new ArrayList<>();
        if (weekList == null) {
            String week= DateUtil.getWeek(currentCalendar);
        }
        return calendarList;
    }
}
