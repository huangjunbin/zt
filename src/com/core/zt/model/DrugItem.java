package com.core.zt.model;


import java.io.Serializable;
import java.util.List;

public class DrugItem implements Serializable {

   private String planId	;//	用药方案Id
    private  String userId		;//	用户id
    private String  createDate		;//	创建日期
    private   String drugChemicalCategoryCode		;//	分类id
    private   String drud_cn_name		;//	中文药名
    private   String  drud_en_name		;//	英文药名
    private    String drugChemicalCategoryCnName		;//	中文分类名称
    private  String drugChemicalCategoryEnName		;//	英文分类名称
    private  String  unit		;//	单位
    private   int childSize		;//	次数
    private  int doseCount		;//	剂量
    private String  remarks		;//	备注
    private int drudId;
    private List<DrugChildItem> child;



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getDrugChemicalCategoryCode() {
        return drugChemicalCategoryCode;
    }

    public void setDrugChemicalCategoryCode(String drugChemicalCategoryCode) {
        this.drugChemicalCategoryCode = drugChemicalCategoryCode;
    }

    public String getDrud_cn_name() {
        return drud_cn_name;
    }

    public void setDrud_cn_name(String drud_cn_name) {
        this.drud_cn_name = drud_cn_name;
    }

    public String getDrud_en_name() {
        return drud_en_name;
    }

    public void setDrud_en_name(String drud_en_name) {
        this.drud_en_name = drud_en_name;
    }

    public String getDrugChemicalCategoryCnName() {
        return drugChemicalCategoryCnName;
    }

    public void setDrugChemicalCategoryCnName(String drugChemicalCategoryCnName) {
        this.drugChemicalCategoryCnName = drugChemicalCategoryCnName;
    }

    public String getDrugChemicalCategoryEnName() {
        return drugChemicalCategoryEnName;
    }

    public void setDrugChemicalCategoryEnName(String drugChemicalCategoryEnName) {
        this.drugChemicalCategoryEnName = drugChemicalCategoryEnName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getChildSize() {
        return childSize;
    }

    public void setChildSize(int childSize) {
        this.childSize = childSize;
    }

    public int getDoseCount() {
        return doseCount;
    }

    public void setDoseCount(int doseCount) {
        this.doseCount = doseCount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }



    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public List<DrugChildItem> getChild() {
        return child;
    }

    public void setChild(List<DrugChildItem> child) {
        this.child = child;
    }

    public int getDrudId() {
        return drudId;
    }

    public void setDrudId(int drudId) {
        this.drudId = drudId;
    }

    @Override
    public String toString() {
        return "DrugItem{" +
                "planId='" + planId + '\'' +
                ", userId='" + userId + '\'' +
                ", createDate='" + createDate + '\'' +
                ", drugChemicalCategoryCode='" + drugChemicalCategoryCode + '\'' +
                ", drud_cn_name='" + drud_cn_name + '\'' +
                ", drud_en_name='" + drud_en_name + '\'' +
                ", drugChemicalCategoryCnName='" + drugChemicalCategoryCnName + '\'' +
                ", drugChemicalCategoryEnName='" + drugChemicalCategoryEnName + '\'' +
                ", unit='" + unit + '\'' +
                ", childSize=" + childSize +
                ", doseCount=" + doseCount +
                ", remarks='" + remarks + '\'' +
                ", drudId=" + drudId +
                ", child=" + child +
                '}';
    }
}
