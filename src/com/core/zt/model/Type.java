package com.core.zt.model;

import java.io.Serializable;
import java.util.List;

public class Type implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private boolean isCheck = false;
	private String name;
	private List<Type> childs;
	private String value;
	private String label;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isCheck() {
		return isCheck;
	}

	public void setCheck(boolean isCheck) {
		this.isCheck = isCheck;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Type> getChilds() {
		return childs;
	}

	public void setChilds(List<Type> childs) {
		this.childs = childs;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "Type{" +
				"id=" + id +
				", isCheck=" + isCheck +
				", name='" + name + '\'' +
				", childs=" + childs +
				", value='" + value + '\'' +
				", label='" + label + '\'' +
				'}';
	}
}
