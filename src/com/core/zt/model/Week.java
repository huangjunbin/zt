package com.core.zt.model;

import java.io.Serializable;

public class Week implements Serializable{
    public int week = 0;//1-7

    public Week(int week) {
        this.week = week;
    }

    public String getWeek() {
        String str = "";
        if (week == 0) {
            str = "每天";
        } else if (week == 7) {
            str = "周日";
        } else if (week == 1) {
            str = "周一";
        } else if (week == 2) {
            str = "周二";
        } else if (week == 3) {
            str = "周三";
        } else if (week == 4) {
            str = "周四";
        } else if (week == 5) {
            str = "周五";
        } else if (week == 6) {
            str = "周六";
        } else {
            str = "未知";
        }
        return str;
    }

}
