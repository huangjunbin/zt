package com.core.zt.model;


public class Case {
    private String	  id			;//病历id
    private String	  diabetesType			;//糖尿病类型
    private String	 diabetesTypeLabel			;//糖尿病类型名称
    private String	 concurrentType			;//并发类型
    private String	 concurrentTypeLabel			;//并发类型名称
    private String	  healthType			;//健康类型
    private String	  healthTypeLabel			;//健康类型名称
    private String	 solutionType			;//治疗方案类型
    private String	 solutionTypeLabel			;//治疗方案类型名称
    private String	  userId			;//用户ID
    private String	 doctorId			;//看病的医生
    private String	 diagnosisTime			;//确诊时间
   private String	diagnosisType		;//确诊类型（0：表示预约；1：表示确诊）

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDiabetesType() {
        return diabetesType;
    }

    public void setDiabetesType(String diabetesType) {
        this.diabetesType = diabetesType;
    }

    public String getDiabetesTypeLabel() {
        return diabetesTypeLabel;
    }

    public void setDiabetesTypeLabel(String diabetesTypeLabel) {
        this.diabetesTypeLabel = diabetesTypeLabel;
    }

    public String getConcurrentType() {
        return concurrentType;
    }

    public void setConcurrentType(String concurrentType) {
        this.concurrentType = concurrentType;
    }

    public String getConcurrentTypeLabel() {
        return concurrentTypeLabel;
    }

    public void setConcurrentTypeLabel(String concurrentTypeLabel) {
        this.concurrentTypeLabel = concurrentTypeLabel;
    }

    public String getHealthType() {
        return healthType;
    }

    public void setHealthType(String healthType) {
        this.healthType = healthType;
    }

    public String getHealthTypeLabel() {
        return healthTypeLabel;
    }

    public void setHealthTypeLabel(String healthTypeLabel) {
        this.healthTypeLabel = healthTypeLabel;
    }

    public String getSolutionType() {
        return solutionType;
    }

    public void setSolutionType(String solutionType) {
        this.solutionType = solutionType;
    }

    public String getSolutionTypeLabel() {
        return solutionTypeLabel;
    }

    public void setSolutionTypeLabel(String solutionTypeLabel) {
        this.solutionTypeLabel = solutionTypeLabel;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDiagnosisTime() {
        return diagnosisTime;
    }

    public void setDiagnosisTime(String diagnosisTime) {
        this.diagnosisTime = diagnosisTime;
    }

    public String getDiagnosisType() {
        return diagnosisType;
    }

    public void setDiagnosisType(String diagnosisType) {
        this.diagnosisType = diagnosisType;
    }

    @Override
    public String toString() {
        return "Case{" +
                "id='" + id + '\'' +
                ", diabetesType='" + diabetesType + '\'' +
                ", diabetesTypeLabel='" + diabetesTypeLabel + '\'' +
                ", concurrentType='" + concurrentType + '\'' +
                ", concurrentTypeLabel='" + concurrentTypeLabel + '\'' +
                ", healthType='" + healthType + '\'' +
                ", healthTypeLabel='" + healthTypeLabel + '\'' +
                ", solutionType='" + solutionType + '\'' +
                ", solutionTypeLabel='" + solutionTypeLabel + '\'' +
                ", userId='" + userId + '\'' +
                ", doctorId='" + doctorId + '\'' +
                ", diagnosisTime='" + diagnosisTime + '\'' +
                ", diagnosisType='" + diagnosisType + '\'' +
                '}';
    }
}
