package com.core.zt.model;


import com.simple.util.db.annotation.SimpleColumn;
import com.simple.util.db.annotation.SimpleId;
import com.simple.util.db.annotation.SimpleTable;

import java.io.Serializable;

@SimpleTable(name = "t_user")
public class User implements Serializable {
    @SimpleId
    @SimpleColumn(name = "userId")
    private String userId;//用户id
    @SimpleColumn(name = "loginDate")
    private String loginDate;//创建时间(或登陆时间)
    @SimpleColumn(name = "nickName")
    private String nickName;//昵称
    @SimpleColumn(name = "userName")
    private String userName;//用户名
    @SimpleColumn(name = "no")
    private String no;//工号
    @SimpleColumn(name = "email")
    private String email;//邮箱地址
    @SimpleColumn(name = "mobile")
    private String mobile;//手机号
    @SimpleColumn(name = "phone")
    private String phone;//电话
    @SimpleColumn(name = "sex")
    private int sex;//1 男 0 女
    @SimpleColumn(name = "photo")
    private String photo;//头像
    @SimpleColumn(name = "stature")
    private int stature;//身高
    @SimpleColumn(name = "age")
    private int age;//年龄
    @SimpleColumn(name = "currentWeight")
    private int currentWeight;//当前体重
    @SimpleColumn(name = "maxWeigth")
    private int maxWeigth;//最大体重
    @SimpleColumn(name = "labourIntensity")
    private String labourIntensity;//劳动强度
    @SimpleColumn(name = "birthday")
    private String birthday;
    @SimpleColumn(name = "area")
    private String area;
    @SimpleColumn(name = "realName")
    private String realName;
    @SimpleColumn(name = "password")
    private String password;
    @SimpleColumn(name = "createDate")
    private String createDate;
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(String loginDate) {
        this.loginDate = loginDate;
    }

    public String getNickName() {
        return nickName==null?"":nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserName() {
        return userName==null?"":userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone==null?"":phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getStature() {
        return stature;
    }

    public void setStature(int stature) {
        this.stature = stature;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCurrentWeight() {
        return currentWeight;
    }

    public void setCurrentWeight(int currentWeight) {
        this.currentWeight = currentWeight;
    }

    public int getMaxWeigth() {
        return maxWeigth;
    }

    public void setMaxWeigth(int maxWeigth) {
        this.maxWeigth = maxWeigth;
    }

    public String getLabourIntensity() {
        return labourIntensity;
    }

    public void setLabourIntensity(String labourIntensity) {
        this.labourIntensity = labourIntensity;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", loginDate='" + loginDate + '\'' +
                ", nickName='" + nickName + '\'' +
                ", userName='" + userName + '\'' +
                ", no='" + no + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", phone='" + phone + '\'' +
                ", sex=" + sex +
                ", photo='" + photo + '\'' +
                ", stature=" + stature +
                ", age=" + age +
                ", currentWeight=" + currentWeight +
                ", maxWeigth=" + maxWeigth +
                ", labourIntensity='" + labourIntensity + '\'' +
                ", birthday='" + birthday + '\'' +
                ", area='" + area + '\'' +
                ", realName='" + realName + '\'' +
                ", password='" + password + '\'' +
                ", createDate='" + createDate + '\'' +
                '}';
    }
}
