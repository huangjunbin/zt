package com.core.zt.model;

public class Series {

    private String id;//	课程id
    private int total;//文章数
    private String seriesName;//课程名称
    private String filePath;//图片路径

    private String fileName;//图片名称

    private String seriesPicId;//图片id

    private String sort;//排序

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSeriesPicId() {
        return seriesPicId;
    }

    public void setSeriesPicId(String seriesPicId) {
        this.seriesPicId = seriesPicId;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "Series{" +
                "id='" + id + '\'' +
                ", total='" + total + '\'' +
                ", seriesName='" + seriesName + '\'' +
                ", filePath='" + filePath + '\'' +
                ", fileName='" + fileName + '\'' +
                ", seriesPicId='" + seriesPicId + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }
}
