package com.core.zt.model;


import java.io.Serializable;

public class Drug implements Serializable {
    private String id;
    private String drugEnName;
    private String drugCnName;
    private String drugChemicalCategoryCode;
    private String drupCategoryEnName;
    private String drupCategoryCnName;
    private String unit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDrugEnName() {
        return drugEnName;
    }

    public void setDrugEnName(String drugEnName) {
        this.drugEnName = drugEnName;
    }

    public String getDrugCnName() {
        return drugCnName;
    }

    public void setDrugCnName(String drugCnName) {
        this.drugCnName = drugCnName;
    }

    public String getDrugChemicalCategoryCode() {
        return drugChemicalCategoryCode;
    }

    public void setDrugChemicalCategoryCode(String drugChemicalCategoryCode) {
        this.drugChemicalCategoryCode = drugChemicalCategoryCode;
    }

    public String getDrupCategoryEnName() {
        return drupCategoryEnName;
    }

    public void setDrupCategoryEnName(String drupCategoryEnName) {
        this.drupCategoryEnName = drupCategoryEnName;
    }

    public String getDrupCategoryCnName() {
        return drupCategoryCnName;
    }

    public void setDrupCategoryCnName(String drupCategoryCnName) {
        this.drupCategoryCnName = drupCategoryCnName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Drug{" +
                "id='" + id + '\'' +
                ", drugEnName='" + drugEnName + '\'' +
                ", drugCnName='" + drugCnName + '\'' +
                ", drugChemicalCategoryCode='" + drugChemicalCategoryCode + '\'' +
                ", drupCategoryEnName='" + drupCategoryEnName + '\'' +
                ", drupCategoryCnName='" + drupCategoryCnName + '\'' +
                ", unit='" + unit + '\'' +
                '}';
    }
}
