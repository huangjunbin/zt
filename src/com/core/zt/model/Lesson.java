package com.core.zt.model;


import java.io.Serializable;

public class Lesson implements Serializable{

    private String id;//课程id
    private String lessonName;//文章名称
    private String lessonContent;//文章内容
    private String author;//作者
    private int browseCnt;//	浏览数
    private int acceptCnt;//	赞同数
    private int usefulCnt;//	有用数
    private String filePath;//图片路径
    private String fileName;//图片名称
    private String picId;//图片id
    private String issueDate;//时间
    private String createUserName;
    private int isUseful;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public String getLessonContent() {
        return lessonContent;
    }

    public void setLessonContent(String lessonContent) {
        this.lessonContent = lessonContent;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getBrowseCnt() {
        return browseCnt;
    }

    public void setBrowseCnt(int browseCnt) {
        this.browseCnt = browseCnt;
    }

    public int getAcceptCnt() {
        return acceptCnt;
    }

    public void setAcceptCnt(int acceptCnt) {
        this.acceptCnt = acceptCnt;
    }

    public int getUsefulCnt() {
        return usefulCnt;
    }

    public void setUsefulCnt(int usefulCnt) {
        this.usefulCnt = usefulCnt;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPicId() {
        return picId;
    }

    public void setPicId(String picId) {
        this.picId = picId;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public int getIsUseful() {
        return isUseful;
    }

    public void setIsUseful(int isUseful) {
        this.isUseful = isUseful;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id='" + id + '\'' +
                ", lessonName='" + lessonName + '\'' +
                ", lessonContent='" + lessonContent + '\'' +
                ", author='" + author + '\'' +
                ", browseCnt=" + browseCnt +
                ", acceptCnt=" + acceptCnt +
                ", usefulCnt=" + usefulCnt +
                ", filePath='" + filePath + '\'' +
                ", fileName='" + fileName + '\'' +
                ", picId='" + picId + '\'' +
                ", issueDate='" + issueDate + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", isUseful=" + isUseful +
                '}';
    }
}
