package com.core.zt.model;

import java.io.Serializable;

public class Advert implements Serializable {
    private String id;//轮播id
    private String picName;//标题
    private  String picDesc;//链接url
    private  String picPath;//图片路径
    private  String sort;//排序

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }

    public String getPicDesc() {
        return picDesc;
    }

    public void setPicDesc(String picDesc) {
        this.picDesc = picDesc;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "Advert{" +
                "id='" + id + '\'' +
                ", picName='" + picName + '\'' +
                ", picDesc='" + picDesc + '\'' +
                ", picPath='" + picPath + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }
}
