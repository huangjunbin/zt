package com.core.zt.model;


import java.io.Serializable;

public class MyRecord implements Serializable{

    private Child bloodSugar0;
    private Child bloodSugar1;
    private Child bloodSugar2;
    private Child bloodSugar3;
    private Child bloodSugar4;
    private Child bloodSugar5;
    private Child bloodSugar6;
    private Child bloodSugar7;
    private Child bloodSugar8;
    private Child BMI;
    private Child bloodPressure;
    private Child hemoglobin;

    public Child getBloodSugar0() {
        return bloodSugar0;
    }

    public void setBloodSugar0(Child bloodSugar0) {
        this.bloodSugar0 = bloodSugar0;
    }

    public Child getBloodSugar1() {
        return bloodSugar1;
    }

    public void setBloodSugar1(Child bloodSugar1) {
        this.bloodSugar1 = bloodSugar1;
    }

    public Child getBloodSugar2() {
        return bloodSugar2;
    }

    public void setBloodSugar2(Child bloodSugar2) {
        this.bloodSugar2 = bloodSugar2;
    }

    public Child getBloodSugar3() {
        return bloodSugar3;
    }

    public void setBloodSugar3(Child bloodSugar3) {
        this.bloodSugar3 = bloodSugar3;
    }

    public Child getBloodSugar4() {
        return bloodSugar4;
    }

    public void setBloodSugar4(Child bloodSugar4) {
        this.bloodSugar4 = bloodSugar4;
    }

    public Child getBloodSugar5() {
        return bloodSugar5;
    }

    public void setBloodSugar5(Child bloodSugar5) {
        this.bloodSugar5 = bloodSugar5;
    }

    public Child getBloodSugar6() {
        return bloodSugar6;
    }

    public void setBloodSugar6(Child bloodSugar6) {
        this.bloodSugar6 = bloodSugar6;
    }

    public Child getBloodSugar7() {
        return bloodSugar7;
    }

    public void setBloodSugar7(Child bloodSugar7) {
        this.bloodSugar7 = bloodSugar7;
    }

    public Child getBloodSugar8() {
        return bloodSugar8;
    }

    public void setBloodSugar8(Child bloodSugar8) {
        this.bloodSugar8 = bloodSugar8;
    }

    public Child getBMI() {
        return BMI;
    }

    public void setBMI(Child BMI) {
        this.BMI = BMI;
    }

    public Child getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(Child bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public Child getHemoglobin() {
        return hemoglobin;
    }

    public void setHemoglobin(Child hemoglobin) {
        this.hemoglobin = hemoglobin;
    }

    @Override
    public String toString() {
        return "MyRecord{" +
                "bloodSugar0=" + bloodSugar0 +
                ", bloodSugar1=" + bloodSugar1 +
                ", bloodSugar2=" + bloodSugar2 +
                ", bloodSugar3=" + bloodSugar3 +
                ", bloodSugar4=" + bloodSugar4 +
                ", bloodSugar5=" + bloodSugar5 +
                ", bloodSugar6=" + bloodSugar6 +
                ", bloodSugar7=" + bloodSugar7 +
                ", bloodSugar8=" + bloodSugar8 +
                ", BMI=" + BMI +
                ", bloodPressure=" + bloodPressure +
                ", hemoglobin=" + hemoglobin +
                '}';
    }

    public class  Child implements  Serializable{
        private float avgNumerical;
        private Record min;
        private Record max;

        public float getAvgNumerical() {
            return avgNumerical;
        }

        public void setAvgNumerical(float avgNumerical) {
            this.avgNumerical = avgNumerical;
        }

        public Record getMin() {
            return min;
        }

        public void setMin(Record min) {
            this.min = min;
        }

        public Record getMax() {
            return max;
        }

        public void setMax(Record max) {
            this.max = max;
        }

        @Override
        public String toString() {
            return "Child{" +
                    "avgNumerical=" + avgNumerical +
                    ", min=" + min +
                    ", max=" + max +
                    '}';
        }
    }


}
