package com.core.zt.model;

public class GroupMember {

	private String id;
	private String groupName;
	private String groupID;
	private String nickName;
	private String traderMemberID;
	private String name;
	private String mobileNumber;
	private String avatarsAddress;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getTraderMemberID() {
		return traderMemberID;
	}

	public void setTraderMemberID(String traderMemberID) {
		this.traderMemberID = traderMemberID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAvatarsAddress() {
		return avatarsAddress;
	}

	public void setAvatarsAddress(String avatarsAddress) {
		this.avatarsAddress = avatarsAddress;
	}

	@Override
	public String toString() {
		return "GroupMember [id=" + id + ", groupName=" + groupName
				+ ", groupID=" + groupID + ", nickName=" + nickName
				+ ", traderMemberID=" + traderMemberID + ", name=" + name
				+ ", mobileNumber=" + mobileNumber + ", avatarsAddress="
				+ avatarsAddress + "]";
	}

}
