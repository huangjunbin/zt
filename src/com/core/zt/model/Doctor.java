package com.core.zt.model;


public class Doctor {

   private String  id;//医生id
    private String  loginDate	;//创建时间(或登陆时间)
    private String userName	;//	用户名
    private String  doctorName		;//	真实姓名
    private String   no	;//工号
    private String email		;//	邮箱地址
    private String  mobile	;//	手机号
    private String phone		;//	电话
    private int sex		;//	1 男 0 女
    private String photo	;//头像
    private String area	;//地区
    private String   hospital	;//医院名称
    private String occupation		;//	职业
    private String occupationLabel;//职业名称
    private String profession		;//	职称
    private String professionLabel	;//	职称名称
    private String practiceCode	;//	执业编号
    private String outTime		;//	出诊时间
    private String notice		;//	公告
    private String specialty		;//	专长

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(String loginDate) {
        this.loginDate = loginDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOccupationLabel() {
        return occupationLabel;
    }

    public void setOccupationLabel(String occupationLabel) {
        this.occupationLabel = occupationLabel;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getProfessionLabel() {
        return professionLabel;
    }

    public void setProfessionLabel(String professionLabel) {
        this.professionLabel = professionLabel;
    }

    public String getPracticeCode() {
        return practiceCode;
    }

    public void setPracticeCode(String practiceCode) {
        this.practiceCode = practiceCode;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "doctorId='" + id + '\'' +
                ", loginDate='" + loginDate + '\'' +
                ", userName='" + userName + '\'' +
                ", doctorName='" + doctorName + '\'' +
                ", no='" + no + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", phone='" + phone + '\'' +
                ", sex=" + sex +
                ", photo='" + photo + '\'' +
                ", area='" + area + '\'' +
                ", hospital='" + hospital + '\'' +
                ", occupation='" + occupation + '\'' +
                ", occupationLabel='" + occupationLabel + '\'' +
                ", profession='" + profession + '\'' +
                ", professionLabel='" + professionLabel + '\'' +
                ", practiceCode='" + practiceCode + '\'' +
                ", outTime='" + outTime + '\'' +
                ", notice='" + notice + '\'' +
                ", specialty='" + specialty + '\'' +
                '}';
    }
}
