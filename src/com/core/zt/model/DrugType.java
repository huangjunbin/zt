package com.core.zt.model;

import java.io.Serializable;
import java.util.List;

public class DrugType implements Serializable {

	private static final long serialVersionUID = 1L;
	private String	id			;//Id
	private 	String		parentCategoryId			;//上级id
	private 	String		drupCategoryEnName		;//英文名称
	private 	String	drupCategoryCnName		;//中文名称
	private List<DrugType> child;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(String parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public String getDrupCategoryEnName() {
		return drupCategoryEnName;
	}

	public void setDrupCategoryEnName(String drupCategoryEnName) {
		this.drupCategoryEnName = drupCategoryEnName;
	}

	public String getDrupCategoryCnName() {
		return drupCategoryCnName;
	}

	public void setDrupCategoryCnName(String drupCategoryCnName) {
		this.drupCategoryCnName = drupCategoryCnName;
	}

	public List<DrugType> getChild() {
		return child;
	}

	public void setChild(List<DrugType> child) {
		this.child = child;
	}

	@Override
	public String toString() {
		return "DrugType{" +
				"id='" + id + '\'' +
				", parentCategoryId='" + parentCategoryId + '\'' +
				", drupCategoryEnName='" + drupCategoryEnName + '\'' +
				", drupCategoryCnName='" + drupCategoryCnName + '\'' +
				", child=" + child +
				'}';
	}
}
