package com.core.zt.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.core.zt.app.AppContext;
import com.core.zt.model.User;
import com.core.zt.service.NoticeServer;
import com.core.zt.service.UserServer;


public class BootBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "BootBroadcastReceiver";
    private static final String ACTION_BOOT = "android.intent.action.BOOT_COMPLETED";
    NoticeServer server;
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Boot this system , BootBroadcastReceiver onReceive()");
        if (intent.getAction().equals(ACTION_BOOT)) {
            Log.i(TAG, "BootBroadcastReceiver onReceive(), Do thing!");
            User cacheUser = UserServer.getInstance(context).getCacheUser();
            server=new NoticeServer(context);
            if (cacheUser != null) {
                AppContext.currentUser = cacheUser;
                server.addNotices();
            }
        }
    }
}
