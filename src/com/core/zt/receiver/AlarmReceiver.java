package com.core.zt.receiver;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.core.zt.R;
import com.core.zt.app.ui.NewNoticeActivity;
import com.core.zt.model.Notice;
import com.core.zt.service.AlarmServer;
import com.core.zt.service.NoticeServer;

public class AlarmReceiver extends BroadcastReceiver {
    AlarmServer alarmServer;
    private NotificationManager nm;
    @Override
    public void onReceive(Context context, Intent intent) {
        int id=intent.getIntExtra("id", 0);
        alarmServer=new AlarmServer(context);
        String service = Context.NOTIFICATION_SERVICE;
        nm = (NotificationManager)context.getSystemService(service); // get system service
        Notice notice=new NoticeServer(context).getNotice(id);
        alarmServer.removeAlarm(id);
        alarmServer.AddAlarm(notice);
        Notification n = new Notification();
        n.icon = R.drawable.ic_launcher;
        n.tickerText = notice.getName()+"-"+notice.getRemark();
        n.when = System.currentTimeMillis();
        n.flags=Notification.FLAG_AUTO_CANCEL ;//|Notification.FLAG_INSISTENT
        n.defaults = Notification.DEFAULT_VIBRATE|Notification.DEFAULT_SOUND;
        Intent i = new Intent(context, NewNoticeActivity.class);
        i.putExtra("tag", 1);
        i.putExtra("data", notice);
        PendingIntent pi = PendingIntent.getActivity(context, 0, i, 0);
        n.setLatestEventInfo(context, notice.getName(), notice.getRemark(), pi);
        nm.notify(1, n);
    }
}
